<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>HHRR | Bonuses </title>
</head>
<body class="hold-transition sidebar-mini">
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Bonuses</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?echo site_url('home');?>">Home</a></li>
              <li class="breadcrumb-item active">Bonuses</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12">
            <div class="card">
              <div class="card-HEADER clearfix">
                <button type="button" class="btn btn-link float-right" onclick="create_bonus()" data-toggle="modal" data-target="#bonus_modal"><i class="fa fa-plus"></i></button>
              </div>
              <div class="card-header d-flex p-0">
                <h3 class="card-title p-3">
                  <i class="fa fa-list mr-1"></i>
                  Bonuses List
                </h3>
                <ul class="nav nav-pills ml-auto p-2">
                  <li class="nav-item">
                    <a class="nav-link active" href="#pending_bonuses_tab" data-toggle="tab">Pending</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#processed_bonuses_tab" data-toggle="tab">Processed</a>
                  </li>
                </ul>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content p-0">
                  <div class="tab-pane active" id="pending_bonuses_tab">
                    <table id="pending_bonuses" class="table datatable" style="width: 100%;">
                      <thead>
                      <tr>
                        <th>#</th>
                        <th>Employee</th>
                        <th>Value</th>
                        <th>Date</th>
                        <th>Status</th>
                        <th>Actions</th>
                      </tr>
                      </thead>
                      <tbody>
                      </tbody>
                    </table>
                  </div>
                  <div class="tab-pane" id="processed_bonuses_tab">
                    <table id="processed_bonuses" class="table datatable" style="width: 100%;">
                      <thead>
                      <tr>
                        <th>#</th>
                        <th>Employee</th>
                        <th>Value</th>
                        <th>Date</th>
                        <th>Status</th>
                        <th>Actions</th>
                      </tr>
                      </thead>
                      <tbody>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col-md-12 -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
    <!-- Modal -->
    <div class="modal fade" id="bonus_modal" tabindex="-1" role="dialog" aria-labelledby="bonus_modal_title" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="bonus_modal_title">Modal title</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form action="javascript:;" method="post" id="bonus_form">
              <div class="card-body">
                <input type="hidden" name="id">
                <div class="form-group">
                  <label>Employee</label>
                  <select class="form-control" placeholder="employee" name="employee" autocomplete="off" required>
                    <option value="0">Choose an option</option>
                    <?foreach ($employees as $employee):?>
                      <option value="<?echo $employee['employee_id'];?>"><?echo $employee['employee_firstname'] . ' ' . $employee['employee_firstlastname'];?></option>
                    <?endforeach;?>
                  </select>
                  <span class="input-feedback"></span>
                </div>
                <div class="form-group">
                  <label>Value</label>
                  <input type="number" class="form-control" placeholder="Value" name="value" autocomplete="off" required>
                  <span class="input-feedback"></span>
                </div>
                <div class="form-group">
                  <label>Date</label>
                  <input type="date" class="form-control" placeholder="Date" name="date" autocomplete="off" required>
                  <span class="input-feedback"></span>
                </div>
              </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary" id="bonus_btn">Btn text</button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- ./wrapper -->
<script src="<?echo base_url();?>scripts/bonuses.js" type="text/javascript"></script>
</body>
</html>
