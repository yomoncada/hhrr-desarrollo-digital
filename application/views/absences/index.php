<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>HHRR | Absences </title>
</head>
<body class="hold-transition sidebar-mini">
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Absences</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?echo site_url('home');?>">Home</a></li>
              <li class="breadcrumb-item active">Absences</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12">
            <div class="card">
              <div class="card-HEADER clearfix">
                <button type="button" class="btn btn-link float-right" onclick="create_absence()" data-toggle="modal" data-target="#absence_modal"><i class="fa fa-plus"></i></button>
              </div>
              <div class="card-header d-flex p-0">
                <h3 class="card-title p-3">
                  <i class="fa fa-list mr-1"></i>
                  Absences List
                </h3>
                <ul class="nav nav-pills ml-auto p-2">
                  <li class="nav-item">
                    <a class="nav-link active" href="#pending_absences_tab" data-toggle="tab">Pending</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#processed_absences_tab" data-toggle="tab">Processed</a>
                  </li>
                </ul>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content p-0">
                  <div class="tab-pane active" id="pending_absences_tab">
                    <table id="pending_absences" class="table datatable" style="width: 100%;">
                      <thead>
                      <tr>
                        <th>#</th>
                        <th>Employee</th>
                        <th>Duration</th>
                        <th>Type</th>
                        <th>Start Date</th>
                        <th>End Date</th>
                        <th>Status</th>
                        <th>Actions</th>
                      </tr>
                      </thead>
                      <tbody>
                      </tbody>
                    </table>
                  </div>
                  <div class="tab-pane" id="processed_absences_tab">
                    <table id="processed_absences" class="table datatable" style="width: 100%;">
                      <thead>
                      <tr>
                        <th>#</th>
                        <th>Employee</th>
                        <th>Duration</th>
                        <th>Type</th>
                        <th>Start Date</th>
                        <th>End Date</th>
                        <th>Status</th>
                        <th>Actions</th>
                      </tr>
                      </thead>
                      <tbody>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col-md-12 -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
    <!-- Modal -->
    <div class="modal fade" id="absence_modal" tabindex="-1" role="dialog" aria-labelledby="absence_modal_title" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="absence_modal_title">Modal title</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form action="javascript:;" method="post" id="absence_form">
              <div class="card-body">
                <input type="hidden" name="id">
                <div class="form-group">
                  <label>Employee</label>
                  <select class="form-control" placeholder="employee" name="employee" autocomplete="off" required>
                    <option value="0">Choose an option</option>
                    <?foreach ($employees as $employee):?>
                      <option value="<?echo $employee['employee_id'];?>"><?echo $employee['employee_firstname'] . ' ' . $employee['employee_firstlastname'];?></option>
                    <?endforeach;?>
                  </select>
                  <span class="input-feedback"></span>
                </div>
                <div class="form-group">
                  <label>Duration (Days)</label>
                  <input type="int" class="form-control" placeholder="Duration (Days)" name="days" autocomplete="off" required>
                  <span class="input-feedback"></span>
                </div>
                <div class="form-group">
                  <label>Start Date</label>
                  <input type="date" class="form-control" placeholder="Start Date" name="startdate" autocomplete="off" required>
                  <span class="input-feedback"></span>
                </div>
                <div class="form-group">
                  <label>End Date</label>
                  <input type="date" class="form-control" placeholder="End Date" name="enddate" autocomplete="off" required>
                  <span class="input-feedback"></span>
                </div>
                <div class="form-group">
                  <label>Reason</label>
                  <textarea class="form-control" placeholder="Reason" name="reason" autocomplete="off" required></textarea>
                  <span class="input-feedback"></span>
                </div>
                <div class="form-group">
                  <label>File</label>
                  <input type="text" class="form-control" placeholder="File" name="file" autocomplete="off" required>
                  <span class="input-feedback"></span>
                </div>
                <div class="form-group">
                  <label>Type</label>
                  <select class="form-control" placeholder="Type" name="type" autocomplete="off" required>
                    <option value="0">Choose an option</option>
                    <option value="Incapacity">Incapacity</option>
                    <option value="Permission">Permission</option>
                    <option value="Lack">Lack</option>
                    <option value="Vacations">Vacations</option>
                  </select>
                  <span class="input-feedback"></span>
                </div>
              </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary" id="absence_btn">Btn text</button>
          </div>
        </div>
      </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="absence_modal" tabindex="-1" role="dialog" aria-labelledby="absence_modal_title" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="absence_modal_title">Modal title</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form action="javascript:;" method="post" id="absence_form">
              <div class="card-body">
                <div class="row">
                  <div class="col-md-12">
                    <div class="row">
                      <div class="form-group">
                        <label>Employee</label>
                        <select class="form-control" placeholder="employee" name="employee" autocomplete="off" required>
                          <option value="0">Choose an option</option>
                          <?foreach ($employees as $employee):?>
                            <option value="<?echo $employee['employee_id'];?>"><?echo $employee['employee_firstname'] . ' ' . $employee['employee_firstlastname'];?></option>
                          <?endforeach;?>
                        </select>
                        <span class="input-feedback"></span>
                      </div>
                    </div>
                  </div>
                </div>
                  <div class="form-group">
                    <label>Days</label>
                    <input type="int" class="form-control" placeholder="Days" name="days" autocomplete="off" required>
                    <span class="input-feedback"></span>
                  </div>
                  <div class="form-group">
                    <label>Start Date</label>
                    <input type="date" class="form-control" placeholder="Start Date" name="startdate" autocomplete="off" required>
                    <span class="input-feedback"></span>
                  </div>
                  <div class="form-group">
                    <label>End Date</label>
                    <input type="date" class="form-control" placeholder="End Date" name="endate" autocomplete="off" required>
                    <span class="input-feedback"></span>
                  </div>
                  <div class="form-group">
                    <label>Reason</label>
                    <textarea class="form-control" placeholder="Reason" name="reason" autocomplete="off" required></textarea>
                    <span class="input-feedback"></span>
                  </div>
                  <div class="form-group">
                    <label>File</label>
                    <input type="text" class="form-control" placeholder="File" name="file" autocomplete="off" required>
                    <span class="input-feedback"></span>
                  </div>
                  <div class="form-group">
                    <label>Type</label>
                    <select class="form-control" placeholder="Type" name="type" autocomplete="off" required>
                      <option value="0">Choose an option</option>
                      <option>Incapacity</option>
                      <option>Permission</option>
                      <option>Lack</option>
                      <option>Vacations</option>
                    </select>
                    <span class="input-feedback"></span>
                  </div>
                </div>
              </form>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary" id="absence_btn">Btn text</button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- ./wrapper -->
<script src="<?echo base_url();?>scripts/absences.js" type="text/javascript"></script>
</body>
</html>
