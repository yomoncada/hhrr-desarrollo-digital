<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>HHRR | Dashboard</title>
</head>
<body class="hold-transition sidebar-mini">
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Dashboard</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?echo site_url('home');?>">Home</a></li>
              <li class="breadcrumb-item active">Dashboard</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- Info boxes -->
          <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box">
              <span class="info-box-icon bg-info elevation-1"><i class="fa fa-circle-o"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Abscenses</span>
                <span class="info-box-number"><?echo $absences;?>
                </span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box mb-3">
              <span class="info-box-icon bg-danger elevation-1"><i class="fa fa-circle-o"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Ascents</span>
                <span class="info-box-number"><?echo $ascents;?></span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->

          <!-- fix for small devices only -->
          <div class="clearfix hidden-md-up"></div>

          <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box mb-3">
              <span class="info-box-icon bg-success elevation-1"><i class="fa fa-circle-o"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Bonuses</span>
                <span class="info-box-number"><?echo $bonuses;?></span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box mb-3">
              <span class="info-box-icon bg-warning elevation-1"><i class="fa fa-circle-o"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Increases</span>
                <span class="info-box-number"><?echo $increases;?></span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <?if($this->session->userdata('employee_rol') == 3){?>
          <div class="col-lg-12">
            <div class="card">
              <div class="card-header">
                <h5 class="m-0">Area Employees List</h5>
              </div>
              <div class="card-body">
                <table id="area_employees" class="table datatable" style="width: 100%;">
                  <thead>
                  <tr>
                    <th width="15%">#</th>
                    <th width="28.3%">Employee</th>
                    <th width="28.3%">Position</th>
                    <th width="28.3%">Actions</th>
                  </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <?}?>
          <?if($this->session->userdata('employee_rol') == 1 || $this->session->userdata('employee_rol') == 2){?>
            <?//for($i=1; $i<=$areas; $i++){
              $i = 1;
              foreach ($areas as $area):
                ?>
          <div class="col-lg-2 col-md-3 col-sm-6 col-xs-12">
             <!-- Settings Box -->
            <div class="card">
              <div class="card-header">
                <h3 class="card-title"><?echo $area['area_name'];?></h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <span class="text-muted"></span>
                <p><span style="font-weight: 800;"><?echo $areas_control[$area['area_name']]['employees'];?></span> Employees</p>
                <hr>
                <p><span style="font-weight: 800;"><?echo $areas_control[$area['area_name']]['absences'];?></span> Absences</p>
                <hr>
                <p><span style="font-weight: 800;"><?echo $areas_control[$area['area_name']]['ascents'];?></span> Ascents</p>
                <hr>
                <p><span style="font-weight: 800;"><?echo $areas_control[$area['area_name']]['increases'];?></span> Increases</p>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
            <?$i++;
            endforeach;//}?>
          <?}?>
          <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
</div>
  <!-- ./wrapper -->
  <script src="<?echo base_url();?>scripts/home.js" type="text/javascript"></script>
</body>
</html>
