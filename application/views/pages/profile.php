
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>HHRR | Profile </title>
</head>
<body class="hold-transition sidebar-mini">
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Profile</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?echo site_url('home');?>">Home</a></li>
              <li class="breadcrumb-item active">User profile</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-3">

            <!-- Profile Image -->
            <div class="card card-primary card-outline">
              <div class="card-body box-profile">
                <div class="text-center">
                  <img class="profile-user-img img-fluid img-circle"
                       src="<?echo base_url();?>dist/img/pic.png"
                       alt="User profile picture">
                </div>

                <h3 class="profile-username text-center" id="profile_fullname">Full Name</h3>

                <p class="text-muted text-center" id="profile_username">Username</p>

                <!--<ul class="list-group list-group-unbordered mb-3">
                  <li class="list-group-item">
                    <b>Followers</b> <a class="float-right">1,322</a>
                  </li>
                  <li class="list-group-item">
                    <b>Following</b> <a class="float-right">543</a>
                  </li>
                  <li class="list-group-item">
                    <b>Friends</b> <a class="float-right">13,287</a>
                  </li>
                </ul>-->

                <a href="javascript:;" class="btn btn-primary btn-block" data-toggle="modal" data-target="#avatar_modal"><b>Change Avatar</b></a>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

            <!-- About Me Box -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">About Me</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <strong><i class="fa fa-birthday-cake mr-1"></i> Birth</strong>

                <p class="text-muted" id="profile_birth">Birth</p>

                <hr>

                <strong><i class="fa fa-suitcase mr-1"></i> Area</strong>

                <p class="text-muted" id="profile_area">Area</p>

                <hr>

                <strong><i class="fa fa-user mr-1"></i> Position</strong>

                <p class="text-muted" id="profile_position">Position</p>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
            <?if(empty($this->session->userdata('employee_profile'))){?>
            <!-- Settings Box -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Settings</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <span class="text-muted"></span>
                <a href="javascript:;" data-toggle="modal" data-target="#settings_modal" onclick="get_settings(<?echo "'"; echo $this->session->userdata('employee_id'); echo "'"?>,'info')"><i class="fa fa-user"></i> Change My Info</a>
                <hr>
                <a href="javascript:;" data-toggle="modal" data-target="#settings_modal" onclick="get_settings(<?echo "'"; echo $this->session->userdata('employee_id'); echo "'"?>,'security')"> <i class="fa fa-lock"></i> Change My Security</a>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
            <?}?>
          </div>
          <!-- /.col -->
          <div class="col-md-9">
            <div class="card">
              <div class="card-header p-2">
                <ul class="nav nav-pills">
                  <li class="nav-item"><a class="nav-link active" href="#absences" data-toggle="tab">Absences</a></li>
                  <li class="nav-item"><a class="nav-link" href="#ascents" data-toggle="tab">Ascents</a></li>
                  <li class="nav-item"><a class="nav-link" href="#bonuses" data-toggle="tab">Bonuses</a></li>
                  <li class="nav-item"><a class="nav-link" href="#increases" data-toggle="tab">Increases</a></li>
                </ul>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content">
                  <div class="active tab-pane" id="absences">
                  	<table id="employee_absences" class="table datatable" style="width: 100%;">
                      <thead>
                      <tr>
                        <th>#</th>
                        <th>Duration</th>
                        <th>Type</th>
                        <th>Start Date</th>
                        <th>End Date</th>
                      </tr>
                      </thead>
                      <tbody>
                      </tbody>
                    </table>
                  </div>
                  <!-- /.tab-pane -->
                  <div class="tab-pane" id="ascents">
                  	<table id="employee_ascents" class="table datatable" style="width: 100%;">
                      <thead>
                      <tr>
                        <th>#</th>
                        <th>Area</th>
                        <th>Position</th>
                        <th>Salary</th>
                        <th>Date</th>
                      </tr>
                      </thead>
                      <tbody>
                      </tbody>
                    </table>
                  </div>
                  <!-- /.tab-pane -->
                  <div class="tab-pane" id="bonuses">
                  	<table id="employee_bonuses" class="table datatable" style="width: 100%;">
                      <thead>
                      <tr>
                        <th>#</th>
                        <th>Value</th>
                        <th>Date</th>
                      </tr>
                      </thead>
                      <tbody>
                      </tbody>
                    </table>
                  </div>
                  <div class="tab-pane" id="increases">
                  	<table id="employee_increases" class="table datatable" style="width: 100%;">
                      <thead>
                      <tr>
                        <th>#</th>
                        <th>Type</th>
                        <th>Value</th>
                        <th>Date</th>
                      </tr>
                      </thead>
                      <tbody>
                      </tbody>
                    </table>
                  </div>
                  <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>
            <!-- /.nav-tabs-custom -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <div class="modal fade" id="settings_modal" tabindex="-1" role="dialog" aria-labelledby="settings_modal_title" aria-hidden="true">
    <div class="modal-dialog modal-lg" id="modal" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="settings_modal_title">Modal title</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form action="javascript:;" method="post" id="settings_form">
            <div class="card-body">
              <div class="row">
                <input type="hidden" name="id">
                <div class="col-md-3 info-input">
                  <div class="form-group">
                    <label>Firstname</label>
                    <input type="text" class="form-control" placeholder="Firstname" name="firstname" autocomplete="off" required>
                    <span class="input-feedback"></span>
                  </div>
                </div>
                <div class="col-md-3 info-input">
                  <div class="form-group">
                    <label>Secondname</label>
                    <input type="text" class="form-control" placeholder="Secondname" name="secondname" autocomplete="off" required>
                    <span class="input-feedback"></span>
                  </div>
                </div>
                <div class="col-md-3 info-input">
                  <div class="form-group">
                    <label>First Lastname</label>
                    <input type="text" class="form-control" placeholder="First Lastname" name="firstlastname" autocomplete="off" required>
                    <span class="input-feedback"></span>
                  </div>
                </div>
                <div class="col-md-3 info-input">
                  <div class="form-group">
                    <label>Second Lastname</label>
                    <input type="text" class="form-control" placeholder="Second Lastname" name="secondlastname" autocomplete="off" required>
                    <span class="input-feedback"></span>
                  </div>
                </div>
                <div class="col-md-3 info-input">
                  <div class="form-group">
                    <label>Birth</label>
                    <input type="date" class="form-control" placeholder="Birth" name="birth" autocomplete="off" required>
                    <span class="input-feedback"></span>
                  </div>
                </div>
                <div class="col-md-3 info-input">
                  <div class="form-group">
                    <label>Country</label>
                    <select class="form-control" placeholder="Country" name="country" onchange="get_regions()" autocomplete="off" required>
                      <option value="0">Choose an option</option>
                      <?foreach ($countries as $country):?>
                        <option value="<?echo $country['country_id'];?>"><?echo $country['country_name'];?></option>
                      <?endforeach;?>
                    </select>
                    <span class="input-feedback"></span>
                  </div>
                </div>
                <div class="col-md-3 info-input">
                  <div class="form-group">
                    <label>Region</label>
                    <select class="form-control" id="regions_select" placeholder="Region" name="region" onchange="get_cities()" autocomplete="off" required>
                      <option value="0">Choose an option</option>
                    </select>
                    <span class="input-feedback"></span>
                  </div>
                </div>
                <div class="col-md-3 info-input">
                  <div class="form-group">
                    <label>City</label>
                    <select class="form-control" id="cities_select" placeholder="City" name="city" autocomplete="off" required>
                      <option value="0">Choose an option</option>
                    </select>
                    <span class="input-feedback"></span>
                  </div>
                </div>
                <div class="col-md-3 info-input">
                  <div class="form-group">
                    <label>Address</label>
                    <textarea class="form-control" placeholder="Address" name="address" autocomplete="off" required></textarea>
                    <span class="input-feedback"></span>
                  </div>
                </div>
                <div class="col-md-3 info-input">
                  <div class="form-group">
                    <label>Neighborhood</label>
                    <input type="text" class="form-control" placeholder="Neighborhood" name="neighborhood" autocomplete="off" required>
                    <span class="input-feedback"></span>
                  </div>
                </div>
                <div class="col-md-3 info-input">
                  <div class="form-group">
                    <label>Cellphone</label>
                    <input type="text" class="form-control" placeholder="Cellphone" name="cellphone" autocomplete="off" required>
                    <span class="input-feedback"></span>
                  </div>
                </div>
                <div class="col-md-3 info-input">
                  <div class="form-group">
                    <label>Phone</label>
                    <input type="text" class="form-control" placeholder="Phone" name="phone" autocomplete="off" required>
                    <span class="input-feedback"></span>
                  </div>
                </div>
                <hr>
                <hr>
                <div class="col-md-3 info-input">
                  <div class="form-group">
                    <label>Whatsapp</label>
                    <input type="text" class="form-control" placeholder="Whatsapp" name="whatsapp" autocomplete="off" required>
                    <span class="input-feedback"></span>
                  </div>
                </div>
                <div class="col-md-3 info-input">
                  <div class="form-group">
                    <label>Facebook</label>
                    <input type="text" class="form-control" placeholder="Facebook" name="facebook" autocomplete="off" required>
                    <span class="input-feedback"></span>
                  </div>
                </div>
                <div class="col-md-3 info-input">
                  <div class="form-group">
                    <label>Google+</label>
                    <input type="text" class="form-control" placeholder="Google+" name="gplus" autocomplete="off" required>
                    <span class="input-feedback"></span>
                  </div>
                </div>
                <div class="col-md-3 info-input">
                  <div class="form-group">
                    <label>LinkedIn</label>
                    <input type="text" class="form-control" placeholder="LinkedIn" name="linkedin" autocomplete="off" required>
                    <span class="input-feedback"></span>
                  </div>
                </div>
                <div class="col-md-12 security-input">
                  <div class="form-group">
                    <label>Actual Password</label>
                    <input type="password" class="form-control" placeholder="Actual Password" name="actual_password" onchange="validate_actual_password()" autocomplete="off" required>
                    <span class="input-feedback"></span>
                  </div>
                </div>
                <div class="col-md-12 security-input">
                  <div class="form-group">
                    <label>New Password</label>
                    <input type="password" class="form-control" placeholder="New Password" name="password" onchange="compare_password()" autocomplete="off" required>
                    <span class="input-feedback"></span>
                  </div>
                </div>
                <div class="col-md-12 security-input">
                  <div class="form-group">
                    <label>Repeat New Password</label>
                    <input type="password" class="form-control" placeholder="Repeat New Password" name="repeat_password" onchange="compare_password()" autocomplete="off" required>
                    <span class="input-feedback"></span>
                  </div>
                </div>
                <div class="col-md-12 security-input">
                  <div class="form-group">
                    <label>Secret Question</label>
                    <select class="form-control" placeholder="Secret Question" name="secques" autocomplete="off" required>
                      <option value="">Choose an option</option>
                      <option value="What is the name of your best childhood friend?">What is the name of your best childhood friend?</option>
                      <option value="What is the name of your first pet?">What is the name of your first pet?</option>
                      <option value="What is the model of your first car?">What is the model of your first car?</option>
                      <option value="What is the profession of your paternal grandfather?">What is the profession of your paternal grandfather?</option>
                      <option value="What is the country you would like to visit?">What is the country you would like to visit?</option>
                    </select>
                    <span class="input-feedback"></span>
                  </div>
                </div>
                <div class="col-md-12 security-input">
                  <div class="form-group">
                    <label>Secret Answer</label>
                    <input type="password" class="form-control" placeholder="Secret Answer" name="secans" autocomplete="off" required>
                    </select>
                    <span class="input-feedback"></span>
                  </div>
                </div>
              </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary" id="settings_btn">Btn text</button>
        </div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="avatar_modal" tabindex="-1" role="dialog" aria-labelledby="avatar_modal_title" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="avatar_modal_title">Modal title</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form action="javascript:;" method="post" id="avatar_form">
            <div class="card-body">
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary" id="avatar_btn">Btn text</button>
        </div>
      </div>
    </div>
  </div>
</div>
  <!-- ./wrapper -->
<script src="<?echo base_url();?>scripts/profile.js" type="text/javascript"></script>
</body>
</html>