<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>HHRR | Log in</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body class="hold-transition login-page">
	<div class="login-box">
		<div class="login-logo">
			<a href=""><b>HHRR</b> Desarrollo Digital</a>
		</div>
		<div class="card" id="login_card">
			<div class="card-body login-card-body">
				<p class="login-box-msg">Sign in to start your session</p>
				<form action="javascript:;" method="post" id="login_form">
					<div class="form-group">
						<input type="text" class="form-control"	 placeholder="Username" name="username" maxlength="16" autocomplete="off" required>
						<span class="input-feedback"></span>
					</div>
					<div class="form-group">
						<input type="password" class="form-control" placeholder="Password" name="password" autocomplete="off" required>
						<span class="input-feedback"></span>
					</div>
					<div class="row">
						<div class="col-4">
							<button type="submit" class="btn btn-primary btn-block btn-flat" id="login_btn" onclick="validate_login()">Sign In</button>
						</div>
					</div>
				</form>
				<p class="mb-1">
			    	<a href="#" class="forgotten-password" onclick="recovery_form()">I forgot my password</a>
			    </p>
			</div>
		</div>
		<div class="card" id="recovery_card">
			<div class="card-body login-card-body">
				<p class="login-box-msg">Fill following form to recover your password</p>
				<form action="javascript:;" method="post" id="recovery_form">
					<div class="form-group">
						<input type="text" class="form-control" id="username" placeholder="Username" name="username" maxlength="16" autocomplete="off" required>
						<span class="input-feedback"></span>
					</div>
					<div class="form-group">
	                    <select class="form-control" placeholder="Secret Question" name="secques" autocomplete="off" required>
	                      <option value="">Choose an option</option>
	                      <option value="What is the name of your best childhood friend?">What is the name of your best childhood friend?</option>
	                      <option value="What is the name of your first pet?">What is the name of your first pet?</option>
	                      <option value="What is the model of your first car?">What is the model of your first car?</option>
	                      <option value="What is the profession of your paternal grandfather?">What is the profession of your paternal grandfather?</option>
	                      <option value="What is the country you would like to visit?">What is the country you would like to visit?</option>
	                    </select>
						<span class="input-feedback"></span>
					</div>
					<div class="form-group">
						<input type="password" class="form-control" placeholder="Secret Answer" name="secans" autocomplete="off" required>
						<span class="input-feedback"></span>
					</div>
					<div class="row">
						<!-- /.col -->
						<div class="col-4">
							<button type="submit" class="btn btn-primary btn-block btn-flat" id="recovery_btn" onclick="validate_recovery()">Recover</button>
						</div>
						<!-- /.col -->
					</div>
				</form>
				<p class="mb-1">
			    	<a href="#" class="forgotten-password" onclick="login_form()">I already remember, take me back</a>
			    </p>
			</div>
		</div>
	</div>
	<script src="<?echo base_url();?>scripts/system.js" type="text/javascript"></script>
</body>
</html>
