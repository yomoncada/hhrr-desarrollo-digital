<!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="<?echo site_url('home');?>" class="brand-link">
      <img src="<?echo base_url();?>dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">HHRR</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="<?echo base_url();?>dist/img/pic.png" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="<?echo site_url('profile');?>" class="d-block"><?echo $this->session->userdata('employee_firstname'); echo ' '; echo $this->session->userdata('employee_firstlastname');?></a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="<?echo site_url('home');?>" class="nav-link active">
              <i class="nav-icon fa fa-dashboard"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>
          <?if($this->session->userdata('employee_rol') == 1 || $this->session->userdata('employee_rol') == 2 || $this->session->userdata('employee_rol') == 3){?>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fa fa-database"></i>
              <p>
                Records
                <i class="right fa fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <?if($this->session->userdata('employee_rol') == 1){?>
              <li class="nav-item">
                <a href="<?echo site_url('allergie');?>" class="nav-link">
                  <i class="nav-icon fa fa-circle-o"></i>
                  <p>
                    Allergies
                    <!--<span class="right badge badge-danger">New</span>-->
                  </p>
                </a>
              </li>
              <?}?>
              <?if($this->session->userdata('employee_rol') == 1 || $this->session->userdata('employee_rol') == 2 || $this->session->userdata('employee_rol') == 3){?>
              <li class="nav-item">
                <a href="<?echo site_url('area');?>" class="nav-link">
                  <i class="nav-icon fa fa-circle-o"></i>
                  <p>
                    Areas
                    <!--<span class="right badge badge-danger">New</span>-->
                  </p>
                </a>
              </li>
              <?}?>
              <?if($this->session->userdata('employee_rol') == 1){?>
              <li class="nav-item">
                <a href="<?echo site_url('boss');?>" class="nav-link">
                  <i class="nav-icon fa fa-circle-o"></i>
                  <p>
                    Bosses
                    <!--<span class="right badge badge-danger">New</span>-->
                  </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?echo site_url('city');?>" class="nav-link">
                  <i class="nav-icon fa fa-circle-o"></i>
                  <p>
                    Cities
                    <!--<span class="right badge badge-danger">New</span>-->
                  </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?echo site_url('country');?>" class="nav-link">
                  <i class="nav-icon fa fa-circle-o"></i>
                  <p>
                    Countries
                    <!--<span class="right badge badge-danger">New</span>-->
                  </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?echo site_url('disease');?>" class="nav-link">
                  <i class="nav-icon fa fa-circle-o"></i>
                  <p>
                    Diseases
                    <!--<span class="right badge badge-danger">New</span>-->
                  </p>
                </a>
              </li>
              <?}?>
              <?if($this->session->userdata('employee_rol') == 1 || $this->session->userdata('employee_rol') == 2 || $this->session->userdata('employee_rol') == 3){?>
              <li class="nav-item">
                <a href="<?echo site_url('employee');?>" class="nav-link">
                  <i class="nav-icon fa fa-circle-o"></i>
                  <p>
                    Employees
                    <!--<span class="right badge badge-danger">New</span>-->
                  </p>
                </a>
              </li>
              <?}?>
              <?if($this->session->userdata('employee_rol') == 1){?>
              <li class="nav-item">
                <a href="<?echo site_url('grade');?>" class="nav-link">
                  <i class="nav-icon fa fa-circle-o"></i>
                  <p>
                    Grades
                    <!--<span class="right badge badge-danger">New</span>-->
                  </p>
                </a>
              </li>
              <?}?>
              <?if($this->session->userdata('employee_rol') == 1 || $this->session->userdata('employee_rol') == 2 || $this->session->userdata('employee_rol') == 3){?>
              <li class="nav-item">
                <a href="<?echo site_url('position');?>" class="nav-link">
                  <i class="nav-icon fa fa-circle-o"></i>
                  <p>
                    Positions
                    <!--<span class="right badge badge-danger">New</span>-->
                  </p>
                </a>
              </li>
              <?}?>
              <?if($this->session->userdata('employee_rol') == 1){?>
              <li class="nav-item">
                <a href="<?echo site_url('region');?>" class="nav-link">
                  <i class="nav-icon fa fa-circle-o"></i>
                  <p>
                    Regions
                    <!--<span class="right badge badge-danger">New</span>-->
                  </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?echo site_url('rol');?>" class="nav-link">
                  <i class="nav-icon fa fa-circle-o"></i>
                  <p>
                    Roles
                    <!--<span class="right badge badge-danger">New</span>-->
                  </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?echo site_url('skill');?>" class="nav-link">
                  <i class="nav-icon fa fa-circle-o"></i>
                  <p>
                    Skilles
                    <!--<span class="right badge badge-danger">New</span>-->
                  </p>
                </a>
              </li>
              <?}?>
            </ul>
          </li>
          <?}?>
          <?if($this->session->userdata('employee_rol') == 1 || $this->session->userdata('employee_rol') == 2){?>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fa fa-sitemap"></i>
              <p>
                Modules
                <i class="right fa fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?echo site_url('absence');?>" class="nav-link">
                  <i class="nav-icon fa fa-circle-o"></i>
                  <p>
                    Absences
                    <!--<span class="right badge badge-danger">New</span>-->
                  </p>
                </a>
              </li>
              <li class=
              <li class="nav-item">
                <a href="<?echo site_url('ascent');?>" class="nav-link">
                  <i class="nav-icon fa fa-circle-o"></i>
                  <p>
                    Ascents
                    <!--<span class="right badge badge-danger">New</span>-->
                  </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?echo site_url('bonus');?>" class="nav-link">
                  <i class="nav-icon fa fa-circle-o"></i>
                  <p>
                    Bonuses
                    <!--<span class="right badge badge-danger">New</span>-->
                  </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?echo site_url('increase');?>" class="nav-link">
                  <i class="nav-icon fa fa-circle-o"></i>
                  <p>
                    Increases
                    <!--<span class="right badge badge-danger">New</span>-->
                  </p>
                </a>
              </li>
            </ul>
          </li>
        </ul>
        <?}?>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>