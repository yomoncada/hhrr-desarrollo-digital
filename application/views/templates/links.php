<!-- Font Awesome Icons -->
<link rel="stylesheet" href="<?echo base_url();?>plugins/font-awesome/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="<?echo base_url();?>dist/css/adminlte.css">
<!-- Bootstrap -->
<!--<link rel="stylesheet" href="<?echo base_url();?>plugins/bootstrap/css/bootstrap.min.css">-->
<!-- Sweet Alert 2 -->
<link rel="stylesheet" href="<?echo base_url();?>plugins/sweetalert/sweetalert2.min.css">
<!-- Data Tables -->
<link rel="stylesheet" href="<?echo base_url();?>plugins/datatables/plugins/bootstrap/datatables.bootstrap4.min.css">
<!-- Google Font: Source Sans Pro -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700">
<!-- jQuery -->
<script src="<?echo base_url();?>plugins/jquery/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<!-- Bootstrap 4 -->
<script src="<?echo base_url();?>plugins/bootstrap/js/bootstrap.bundle.js"></script>
<!-- AdminLTE App -->
<script src="<?echo base_url();?>dist/js/adminlte.min.js"></script>
<!-- Sweet Alert 2 -->
<script src="<?echo base_url();?>plugins/sweetalert/sweetalert2.min.js"></script>
<!-- Data Tables -->
<script src="<?echo base_url();?>plugins/datatables/datatables.min.js"></script>
<script src="<?echo base_url();?>plugins/datatables/plugins/bootstrap/datatables.bootstrap4.min.js"></script>