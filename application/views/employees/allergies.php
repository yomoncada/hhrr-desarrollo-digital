<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>HHRR | Employee Allergies </title>
</head>
<body class="hold-transition sidebar-mini">
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Employee Allergies</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?echo site_url('home');?>">Home</a></li>
              <li class="breadcrumb-item"><a href="<?echo site_url('employee');?>">Employees</a></li>
              <li class="breadcrumb-item active"><?echo $employee->employee_firstname;?>'s  Allergies</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12">
            <div class="card">
              <?if($this->session->userdata('employee_rol') == 1 || $this->session->userdata('employee_rol') == 2){?>
              <div class="card-HEADER clearfix">
                <button type="button" class="btn btn-link float-right" onclick="create_empalle()" data-toggle="modal" data-target="#empalle_modal"><i class="fa fa-plus"></i></button>
              </div>
              <?}?>
              <div class="card-header d-flex p-0">
                <h3 class="card-title p-3">
                  <i class="fa fa-list mr-1"></i>
                  <?echo $employee->employee_firstname;?>'s Allergies List
                </h3>
                <ul class="nav nav-pills ml-auto p-2">
                  <li class="nav-item">
                    <a class="nav-link active" href="#active_allergies_tab" data-toggle="tab">Actives</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#inactive_allergies_tab" data-toggle="tab">Inactives</a>
                  </li>
                </ul>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content p-0">
                  <div class="tab-pane active" id="active_allergies_tab">
                    <table id="active_allergies" class="table datatable" style="width: 100%;">
                      <thead>
                      <tr>
                        <th>#</th>
                        <th>Allergie</th>
                        <th>Status</th>
                        <th>Actions</th>
                      </tr>
                      </thead>
                      <tbody>
                      </tbody>
                    </table>
                  </div>
                  <div class="tab-pane" id="inactive_allergies_tab">
                    <table id="inactive_allergies" class="table datatable" style="width: 100%;">
                      <thead>
                      <tr>
                        <th>#</th>
                        <th>Allergie</th>
                        <th>Status</th>
                        <th>Actions</th>
                      </tr>
                      </thead>
                      <tbody>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col-md-12 -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
    <!-- Modal -->
    <div class="modal fade" id="empalle_modal" tabindex="-1" role="dialog" aria-labelledby="empalle_modal_title" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="empalle_modal_title">Modal title</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form action="javascript:;" method="post" id="empalle_form">
              <div class="card-body">
                <input type="hidden" name="id">
                <div class="form-group">
                  <label>Allergie</label>
                  <select class="form-control" placeholder="Allergie" name="allergie" autocomplete="off" required>
                    <option value="">Choose an option</option>
                    <?foreach ($allergies as $allergie):?>
                      <option value="<?echo $allergie['allergie_id'];?>"><?echo $allergie['allergie_name'];?></option>
                    <?endforeach;?>
                  </select>
                  <span class="input-feedback"></span>
                </div>
              </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary" id="empalle_btn">Btn text</button>
          </div>
        </div>
      </div>
    </div>
  </form>
</div>
  </div>
  <!-- ./wrapper -->
<script src="<?echo base_url();?>scripts/employees.js" type="text/javascript"></script>
</body>
</html>
