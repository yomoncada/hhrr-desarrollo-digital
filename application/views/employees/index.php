<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>HHRR | Employees </title>
</head>
<body class="hold-transition sidebar-mini">
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Employees</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?echo site_url('home');?>">Home</a></li>
              <li class="breadcrumb-item active">Employees</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12">
            <div class="card">
              <?if($this->session->userdata('employee_rol') == 1){?>
              <div class="card-HEADER clearfix">
                <button type="button" class="btn btn-link float-right" onclick="create_employee()" data-toggle="modal" data-target="#employee_modal"><i class="fa fa-plus"></i></button>
              </div>
              <?}?>
              <div class="card-header d-flex p-0">
                <h3 class="card-title p-3">
                  <i class="fa fa-list mr-1"></i>
                  Employees List
                </h3>
                <ul class="nav nav-pills ml-auto p-2">
                  <li class="nav-item">
                    <a class="nav-link active" href="#active_employees_tab" data-toggle="tab">Actives</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#inactive_employees_tab" data-toggle="tab">Inactives</a>
                  </li>
                </ul>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content p-0">
                  <div class="tab-pane active" id="active_employees_tab">
                    <table id="active_employees" class="table datatable" style="width: 100%;">
                      <thead>
                      <tr>
                        <th>#</th>
                        <th>ID</th>
                        <th>Firstname</th>
                        <th>Lastname</th>
                        <th>Rol</th>
                        <th>Status</th>
                        <th>Actions</th>
                      </tr>
                      </thead>
                      <tbody>
                      </tbody>
                    </table>
                  </div>
                  <div class="tab-pane" id="inactive_employees_tab">
                    <table id="inactive_employees" class="table datatable" style="width: 100%;">
                      <thead>
                      <tr>
                        <th>#</th>
                        <th>ID</th>
                        <th>Firstname</th>
                        <th>Lastname</th>
                        <th>Rol</th>
                        <th>Status</th>
                        <th>Actions</th>
                      </tr>
                      </thead>
                      <tbody>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col-md-12 -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
    <!-- Modal -->
    <div class="modal fade" id="employee_modal" tabindex="-1" role="dialog" aria-labelledby="employee_modal_title" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="employee_modal_title">Modal title</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form action="javascript:;" method="post" id="employee_form">
              <div class="card-body">
                <div class="row">
                  <div class="col-md-12">
                    <div class="row">
                      <div class="col-md-3">
                        <div class="form-group">
                          <label>ID</label>
                          <input type="text" class="form-control" id="employee_id" placeholder="ID" name="id" onchange="validate_id()" autocomplete="off" required>
                          <span class="input-feedback"></span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="form-group">
                      <label>Firstname</label>
                      <input type="text" class="form-control" placeholder="Firstname" name="firstname" autocomplete="off" required>
                      <span class="input-feedback"></span>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="form-group">
                      <label>Secondname</label>
                      <input type="text" class="form-control" placeholder="Secondname" name="secondname" autocomplete="off" required>
                      <span class="input-feedback"></span>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="form-group">
                      <label>First Lastname</label>
                      <input type="text" class="form-control" placeholder="First Lastname" name="firstlastname" autocomplete="off" required>
                      <span class="input-feedback"></span>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="form-group">
                      <label>Second Lastname</label>
                      <input type="text" class="form-control" placeholder="Second Lastname" name="secondlastname" autocomplete="off" required>
                      <span class="input-feedback"></span>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="form-group">
                      <label>Birth</label>
                      <input type="date" class="form-control" placeholder="Birth" name="birth" autocomplete="off" required>
                      <span class="input-feedback"></span>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="form-group">
                      <label>Country</label>
                      <select class="form-control" placeholder="Country" name="country" onchange="get_regions()" autocomplete="off" required>
                        <option value="0">Choose an option</option>
                        <?foreach ($countries as $country):?>
                          <option value="<?echo $country['country_id'];?>"><?echo $country['country_name'];?></option>
                        <?endforeach;?>
                      </select>
                      <span class="input-feedback"></span>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="form-group">
                      <label>Region</label>
                      <select class="form-control" id="regions_select" placeholder="Region" name="region" onchange="get_cities()" autocomplete="off" required>
                        <option value="0">Choose an option</option>
                      </select>
                      <span class="input-feedback"></span>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="form-group">
                      <label>City</label>
                      <select class="form-control" id="cities_select" placeholder="City" name="city" autocomplete="off" required>
                        <option value="0">Choose an option</option>
                      </select>
                      <span class="input-feedback"></span>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="form-group">
                      <label>Address</label>
                      <textarea class="form-control" placeholder="Address" name="address" autocomplete="off" required></textarea>
                      <span class="input-feedback"></span>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="form-group">
                      <label>Neighborhood</label>
                      <input type="text" class="form-control" placeholder="Neighborhood" name="neighborhood" autocomplete="off" required>
                      <span class="input-feedback"></span>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="form-group">
                      <label>Cellphone</label>
                      <input type="text" class="form-control" placeholder="Cellphone" name="cellphone" autocomplete="off" required>
                      <span class="input-feedback"></span>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="form-group">
                      <label>Phone</label>
                      <input type="text" class="form-control" placeholder="Phone" name="phone" autocomplete="off" required>
                      <span class="input-feedback"></span>
                    </div>
                  </div>
                  <hr>
                  <hr>
                  <div class="col-md-3">
                    <div class="form-group">
                      <label>Whatsapp</label>
                      <input type="text" class="form-control" placeholder="Whatsapp" name="whatsapp" autocomplete="off" required>
                      <span class="input-feedback"></span>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="form-group">
                      <label>Facebook</label>
                      <input type="text" class="form-control" placeholder="Facebook" name="facebook" autocomplete="off" required>
                      <span class="input-feedback"></span>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="form-group">
                      <label>Google+</label>
                      <input type="text" class="form-control" placeholder="Google+" name="gplus" autocomplete="off" required>
                      <span class="input-feedback"></span>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="form-group">
                      <label>LinkedIn</label>
                      <input type="text" class="form-control" placeholder="LinkedIn" name="linkedin" autocomplete="off" required>
                      <span class="input-feedback"></span>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="form-group">
                      <label>C.V</label>
                      <input type="text" class="form-control" placeholder="C.V" name="file" autocomplete="off" required>
                      <span class="input-feedback"></span>
                    </div>
                  </div>
                  <?if($this->session->userdata('employee_rol') == 1){?>
                  <div class="col-md-3">
                    <div class="form-group">
                      <label>Rol</label>
                      <select class="form-control" placeholder="Rol" name="rol" autocomplete="off" required>
                        <option value="0">Choose an option</option>
                        <?foreach ($roles as $rol):?>
                          <option value="<?echo $rol['rol_id'];?>"><?echo $rol['rol_name'];?></option>
                        <?endforeach;?>
                      </select>
                      <span class="input-feedback"></span>
                    </div>
                  </div>
                  <?}?>
                  <div class="col-md-3">
                    <div class="form-group">
                      <label>Area</label>
                      <select class="form-control" placeholder="Area" name="area" autocomplete="off" required>
                        <option value="0">Choose an option</option>
                        <?foreach ($areas as $area):?>
                          <option value="<?echo $area['area_id'];?>"><?echo $area['area_name'];?></option>
                        <?endforeach;?>
                      </select>
                      <span class="input-feedback"></span>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="form-group">
                      <label>Position</label>
                      <select class="form-control" placeholder="Position" name="position" autocomplete="off" required>
                        <option value="0">Choose an option</option>
                        <?foreach ($positions as $position):?>
                          <option value="<?echo $position['position_id'];?>"><?echo $position['position_name'];?></option>
                        <?endforeach;?>
                      </select>
                      <span class="input-feedback"></span>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="form-group">
                      <label>Boss</label>
                      <select class="form-control" placeholder="Boss" name="boss" autocomplete="off" required>
                        <option value="0">Choose an option</option>
                        <?foreach ($bosses as $boss):?>
                          <option value="<?echo $boss['boss_id'];?>"><?echo $boss['employee_firstname'] . ' ' . $boss['employee_firstlastname'];?></option>
                        <?endforeach;?>
                      </select>
                      <span class="input-feedback"></span>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="form-group">
                      <label>Salary</label>
                      <input type="text" class="form-control" placeholder="Salary" name="salary" autocomplete="off" required>
                      <span class="input-feedback"></span>
                    </div>
                  </div>
                  <?if($this->session->userdata('employee_rol') == 1){?>
                  <div class="col-md-12">
                    <div class="row">
                      <div class="col-md-3">
                        <div class="form-group">
                          <label>Username</label>
                          <input type="text" class="form-control" id="employee_username" placeholder="Username" name="username" onchange="validate_username()" autocomplete="off" required>
                          <span class="input-feedback"></span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="form-group">
                      <label>Password</label>
                      <input type="password" class="form-control" placeholder="Password" name="password" onchange="compare_password()" autocomplete="off" required>
                      <span class="input-feedback"></span>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="form-group">
                      <label>Repeat Password</label>
                      <input type="password" class="form-control" placeholder="Repeat Password" name="repeat_password" onchange="compare_password()" autocomplete="off" required>
                      <span class="input-feedback"></span>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="form-group">
                      <label>Secret Question</label>
                      <select class="form-control" placeholder="Secret Question" name="secques" autocomplete="off" required>
                        <option value="">Choose an option</option>
                        <option value="What is the name of your best childhood friend?">What is the name of your best childhood friend?</option>
                        <option value="What is the name of your first pet?">What is the name of your first pet?</option>
                        <option value="What is the model of your first car?">What is the model of your first car?</option>
                        <option value="What is the profession of your paternal grandfather?">What is the profession of your paternal grandfather?</option>
                        <option value="What is the country you would like to visit?">What is the country you would like to visit?</option>
                      </select>
                    <span class="input-feedback"></span>
                  </div>
                  </div>
                  <div class="col-md-3">
                    <div class="form-group">
                      <label>Secret Answer</label>
                      <input type="password" class="form-control" placeholder="Secret Answer" name="secans" autocomplete="off" required>
                      </select>
                      <span class="input-feedback"></span>
                    </div>
                  </div>
                  <?}?>
                </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary" id="employee_btn">Btn text</button>
          </div>
        </div>
      </div>
    </div>
  </form>
</div>
  </div>
  <!-- ./wrapper -->
<script src="<?echo base_url();?>scripts/employees.js" type="text/javascript"></script>
</body>
</html>
