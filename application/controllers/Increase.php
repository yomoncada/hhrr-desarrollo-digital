<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Increase extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('increase_model','increase');
		$this->load->model('employee_model','employee');
	}

	public function index()
	{
		if($this->session->userdata('is_logued_in') === TRUE && $this->session->userdata('employee_rol') == 1 || $this->session->userdata('employee_rol') == 2)
		{
			$data = array(
	    		'controller' => 'increases',
	    		'employees' => $this->employee->get_all()
	    	);

			$this->load->view('templates/links');
	        $this->load->view('templates/navbar');
	        $this->load->view('templates/sidebar');
			$this->load->view('increases/index',$data);
			$this->load->view('templates/control_sidebar');
	        $this->load->view('templates/footer');
        }
        else
        {
            show_404();
		}
	}

	public function list_pending_increases()
	{
		$list = $this->increase->get_pending_datatables();
		$data = array();
		$no = $_POST['start'];
		$i = 1;
		foreach ($list as $increase_data)
		{
			$no++;
			$row = array();
			$row[] = $i;
			$row[] = '<a href="profile/view/'.$increase_data->increase_employee.'");?>' . $increase_data->employee_firstname . ' ' . $increase_data->employee_firstlastname . '</a>';
			$row[] = $increase_data->increase_type;
			if($increase_data->increase_type == 'Percent')
			{
				$row[] = $increase_data->increase_value  . '%';
			}
			else if($increase_data->increase_type == 'Fixed')
			{
				$row[] = $increase_data->increase_value  . '$';
			}
			$row[] = $increase_data->increase_date;
			$row[] =
				'<a class="btn btn-link" href="javascript:void(0)" title="Process" onclick="get_increase('."'".$increase_data->increase_id."'".')" data-toggle="modal" data-target="#increase_modal">
					<i class="fa fa-edit"></i>
				</a>';
			$data[] = $row;
			$i++;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->increase->count_all(),
			"recordsFiltered" => $this->increase->count_pending_filtered(),
			"data" => $data
		);
		echo json_encode($output);
	}

	public function list_processed_increases()
	{
		$list = $this->increase->get_processed_datatables();
		$data = array();
		$no = $_POST['start'];
		$i = 1;
		foreach ($list as $increase_data)
		{
			$no++;
			$row = array();
			$row[] = $i;
			$row[] = '<a href="profile/view/'.$increase_data->increase_employee.'");?>' . $increase_data->employee_firstname . ' ' . $increase_data->employee_firstlastname . '</a>';
			$row[] = $increase_data->increase_type;
			if($increase_data->increase_type == 'Percent')
			{
				$row[] = $increase_data->increase_value  . '%';
			}
			else if($increase_data->increase_type == 'Fixed')
			{
				$row[] = $increase_data->increase_value  . '$';
			}
			$row[] = $increase_data->increase_date;
			$row[] =
				'<a class="btn btn-link" href="javascript:void(0)" title="Read" onclick="get_increase_unmodifiable('."'".$increase_data->increase_id."'".')" data-toggle="modal" data-target="#increase_modal">
					<i class="fa fa-eye"></i>
				</a>';
			$data[] = $row;
			$i++;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->increase->count_all(),
			"recordsFiltered" => $this->increase->count_processed_filtered(),
			"data" => $data
		);
		echo json_encode($output);
	}

	public function get_increase($id = NULL)
	{
		$data = $this->increase->get($id);
		echo json_encode($data);
	}

	public function create_increase()
	{
		$save_method = "create";
		$this->_validate($save_method);

		$employee = $this->employee->get_by_id($this->input->post('employee'));
		
		$data = array(
			'increase_employee' => $this->input->post('employee'),
			'increase_area' => $employee->employee_area,
			'increase_type' => $this->input->post('type'),
			'increase_value' => $this->input->post('value'),
			'increase_date' => $this->input->post('date'),
			'increase_status' => 'Pending'
		);

		$this->increase->save($data);	
		echo json_encode(array("status" => TRUE));
	}

	public function update_increase()
	{
		$save_method = "update";
		$this->_validate($save_method);

		$employee = $this->employee->get_by_id($this->input->post('employee'));

		$data = array(
			'increase_employee' => $this->input->post('employee'),
			'increase_area' => $employee->employee_area,
			'increase_type' => $this->input->post('type'),
			'increase_value' => $this->input->post('value'),
			'increase_date' => $this->input->post('date'),
			'increase_status' => 'Processed'
		);

		$this->increase->update(array('increase_id' => $this->input->post('id')), $data);

		$employee_data = $this->employee->get_by_id($this->input->post('employee'));

		if($data['increase_type'] == 'Fixed')
		{
			$salary = $employee_data->employee_salary + $this->input->post('value');
		}
		else if($data['increase_type'] == 'Percent')
		{
			$salary = $this->input->post('value') * $employee_data->employee_salary / 100 + $employee_data->employee_salary;
		}
		$this->increase->update_employee_salary($this->input->post('employee'), $salary);
		echo json_encode(array("status" => TRUE));
	}

	private function _validate($save_method)
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('employee') == "0")
		{
			$data['inputerror'][] = 'employee';
			$data['error_string'][] = 'Employee is required.';
			$data['status'] = FALSE;
		}
		if($this->input->post('type') == "0")
		{
			$data['inputerror'][] = 'type';
			$data['error_string'][] = 'Type is required.';
			$data['status'] = FALSE;
		}
		if($this->input->post('value') == '')
		{
			$data['inputerror'][] = 'value';
			$data['error_string'][] = 'Value is required.';
			$data['status'] = FALSE;
		}
		if($this->input->post('date') == '')
		{
			$data['inputerror'][] = 'date';
			$data['error_string'][] = 'Date is required.';
			$data['status'] = FALSE;
		}
		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}
}