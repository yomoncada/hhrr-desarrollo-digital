<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Region extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('region_model','region');
		$this->load->model('country_model','country');
	}

	public function index()
	{
		if($this->session->userdata('is_logued_in') === TRUE && $this->session->userdata('employee_rol') == 1)
		{
			$data = array(
	    		'controller' => 'regions',
	    		'countries' => $this->country->get_all()
	    	);
	    	
			$this->load->view('templates/links');
	        $this->load->view('templates/navbar');
	        $this->load->view('templates/sidebar');
			$this->load->view('regions/index',$data);
			$this->load->view('templates/control_sidebar');
	        $this->load->view('templates/footer');
        }
        else
        {
            show_404();
		}
	}

	public function list_active_regions()
	{
		$list = $this->region->get_datatables_actives();
		$data = array();
		$no = $_POST['start'];
		$i = 1;
		foreach ($list as $region_data)
		{
			$no++;
			$row = array();
			$row[] = $i;
			$row[] = $region_data->region_name;
			$row[] = $region_data->country_name;
			$row[] = $region_data->region_status;
			$row[] =
				'<a class="btn btn-link" href="javascript:void(0)" title="Update" onclick="get_region('."'".$region_data->region_id."'".')" data-toggle="modal" data-target="#region_modal">
					<i class="fa fa-pencil"></i>
				</a>
				<a class="btn btn-link" href="javascript:void(0)" title="Deactivate" onclick="deactivate_region('."'".$region_data->region_id."'".')">
					<i class="fa fa-ban"></i>
				</a>';
			$data[] = $row;
			$i++;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->region->count_all(),
			"recordsFiltered" => $this->region->count_filtered_actives(),
			"data" => $data
		);
		echo json_encode($output);
	}

	public function list_inactive_regions()
	{
		$list = $this->region->get_datatables_inactives();
		$data = array();
		$no = $_POST['start'];
		$i = 1;
		foreach ($list as $region_data)
		{
			$no++;
			$row = array();
			$row[] = $i;
			$row[] = $region_data->region_name;
			$row[] = $region_data->country_name;
			$row[] = $region_data->region_status;
			$row[] =
				'<a class="btn btn-link" href="javascript:void(0)" title="Activate" onclick="activate_region('."'".$region_data->region_id."'".')">
					<i class="fa fa-check"></i>
				</a>';
			$data[] = $row;
			$i++;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->region->count_all(),
			"recordsFiltered" => $this->region->count_filtered_inactives(),
			"data" => $data
		);
		echo json_encode($output);
	}

	public function validate_name()
	{
		$name = $this->input->get('name');

		if ($name!='')
		{
		    $validation = $this->region->validate_by_name($name);
		    if ($validation > 0)
			{
				$data = array(
				'type' => 'Warning',
				'message' => 'Name already exists.',
				'button' => 1
				);
			}
			else if ($validation < 1)
			{
				$data = array(
				'type' => 'Notice',
				'message' => 'Name is available.',
				'button' => 0
				);
			}
	  	}
	  	else if($id=='')
	  	{
		   	$data = array(
				'type' => 'Error',
				'message' => 'Name is required.',
				'button' => 1
			);
	 	}
	   	echo json_encode($data);
	}

	public function get_region($id = NULL)
	{
		$data = $this->region->get($id);
		echo json_encode($data);
	}

	public function create_region()
	{
		$save_method = "create";
		$this->_validate($save_method);

		$data = array(
			'region_name' => $this->input->post('name'),
			'region_country' => $this->input->post('country'),
			'region_status' => 'Active'
		);

		$this->region->create($data);
		echo json_encode(array("status" => TRUE));
	}

	public function update_region()
	{
		$save_method = "update";
		$this->_validate($save_method);
		$data = array(
			'region_name' => $this->input->post('name'),
			'region_country' => $this->input->post('country'),
			'region_status' => 'Active'
		);

		$this->region->update(array('region_id' => $this->input->post('id')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function activate_region($id)
	{
		$this->region->activate($id);
		echo json_encode(array("status" => TRUE));
	}

	public function deactivate_region($id)
	{
		$this->region->deactivate($id);
		echo json_encode(array("status" => TRUE));
	}

	private function _validate($save_method)
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('name') == '')
		{
			$data['inputerror'][] = 'name';
			$data['error_string'][] = 'Name is required.';
			$data['status'] = FALSE;
		}
		if($this->input->post('country') == 0)
		{
			$data['inputerror'][] = 'country';
			$data['error_string'][] = 'Country is required.';
			$data['status'] = FALSE;
		}
		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}
}