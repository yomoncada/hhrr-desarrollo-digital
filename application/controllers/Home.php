<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('home_model','home');
		$this->load->model('employee_model','employee');
	}

	public function index()
	{
		if($this->session->userdata('is_logued_in') === TRUE)
		{
			$data = array(
	    		'controller' => 'home'
	    	);

			$this->load->view('templates/links');
	        $this->load->view('templates/navbar');
	        $this->load->view('templates/sidebar');
			$this->load->view('pages/home',$data);
			$this->load->view('templates/control_sidebar');
	        $this->load->view('templates/footer');
        }
        else
        {
            show_404();
		}
	}

	public function list_area_employees()
	{
		$list = $this->home->get_area_employees_datatables($this->session->userdata('employee_area'));
		$data = array();
		$no = $_POST['start'];
		$i = 1;
		foreach ($list as $area_employees_data)
		{
			$no++;
			$row = array();
			$row[] = $i;
			$row[] = $area_employees_data->employee_firstname . ' ' . $area_employees_data->employee_firstlastname;
			$row[] = $area_employees_data->position_name;
			$row[] =
				'<a class="btn btn-link" href="profile/view/'.$area_employees_data->employee_id.'" title="Read">
					<i class="fa fa-eye"></i>
				</a>
				<a class="btn btn-link" href="employee/skills/'.$area_employees_data->employee_id.'" title="Skills">
					<i class="fa fa-book"></i>
				</a>
				<a class="btn btn-link" href="employee/works/'.$area_employees_data->employee_id.'" title="Works">
					<i class="fa fa-suitcase"></i>
				</a>
				<a class="btn btn-link" href="employee/diseases/'.$area_employees_data->employee_id.'" title="Diseases">
					<i class="fa fa-ambulance"></i>
				</a>
				<a class="btn btn-link" href="employee/allergies/'.$area_employees_data->employee_id.'" title="Allergies">
					<i class="fa fa-heartbeat"></i>
				</a>
				<a class="btn btn-link" href="employee/emails/'.$area_employees_data->employee_id.'" title="Emails">
					<i class="fa fa-inbox"></i>
				</a>';
			$data[] = $row;
			$i++;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->home->count_all_area_employees($this->session->userdata('employee_area')),
			"recordsFiltered" => $this->home->count_area_employees_filtered($this->session->userdata('employee_area')),
			"data" => $data
		);
		echo json_encode($output);
	}
}