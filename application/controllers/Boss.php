<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Boss extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('boss_model','boss');
		$this->load->model('area_model','area');
		$this->load->model('employee_model','employee');
	}

	public function index()
	{
		if($this->session->userdata('is_logued_in') === TRUE && $this->session->userdata('employee_rol') == 1 && $this->session->userdata('employee_rol') == 2)
		{
			$data = array(
	    		'controller' => 'bosses',
	    		'areas' => $this->area->get_all(),
	    		'employees' => $this->employee->get_all()
	    	);
	    	
			$this->load->view('templates/links');
	        $this->load->view('templates/navbar');
	        $this->load->view('templates/sidebar');
			$this->load->view('bosses/index',$data);
			$this->load->view('templates/control_sidebar');
	        $this->load->view('templates/footer');
        }
        else
        {
            show_404();
		}
	}

	public function list_active_bosses()
	{
		$list = $this->boss->get_datatables_actives();
		$data = array();
		$no = $_POST['start'];
		$i = 1;
		foreach ($list as $boss_data)
		{
			$no++;
			$row = array();
			$row[] = $i;
			$row[] = '<a href="profile/view/'.$boss_data->boss_employee.'");?>' . $boss_data->employee_firstname . ' ' . $boss_data->employee_firstlastname . '</a>';
			$row[] = $boss_data->area_name;
			$row[] = $boss_data->boss_status;
			$row[] =
				'<a class="btn btn-link" href="javascript:void(0)" title="Update" onclick="get_boss('."'".$boss_data->boss_id."'".')" data-toggle="modal" data-target="#boss_modal">
					<i class="fa fa-pencil"></i>
				</a>
				<a class="btn btn-link" href="javascript:void(0)" title="Deactivate" onclick="deactivate_boss('."'".$boss_data->boss_id."'".')">
					<i class="fa fa-ban"></i>
				</a>';
			$data[] = $row;
			$i++;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->boss->count_all(),
			"recordsFiltered" => $this->boss->count_filtered_actives(),
			"data" => $data
		);
		echo json_encode($output);
	}

	public function list_inactive_bosses()
	{
		$list = $this->boss->get_datatables_inactives();
		$data = array();
		$no = $_POST['start'];
		$i = 1;
		foreach ($list as $boss_data)
		{
			$no++;
			$row = array();
			$row[] = $i;
			$row[] = $boss_data->employee_firstname . ' ' . $boss_data->employee_firstlastname;
			$row[] = $boss_data->area_name;
			$row[] = $boss_data->boss_status;
			$row[] =
				'<a class="btn btn-link" href="javascript:void(0)" title="Activate" onclick="activate_boss('."'".$boss_data->boss_id."'".')">
					<i class="fa fa-check"></i>
				</a>';
			$data[] = $row;
			$i++;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->boss->count_all(),
			"recordsFiltered" => $this->boss->count_filtered_inactives(),
			"data" => $data
		);
		echo json_encode($output);
	}

	public function validate_employee()
	{
		$employee = $this->input->get('employee');

		if ($employee!='')
		{
		    $validation = $this->boss->validate_by_employee($employee);
		    if ($validation > 0)
			{
				$data = array(
				'type' => 'Warning',
				'message' => 'Employee is already a boss.',
				'button' => 1
				);
			}
			else if ($validation < 1)
			{
				$data = array(
				'type' => 'Notice',
				'message' => 'Employee is available.',
				'button' => 0
				);
			}
	  	}
	  	else if($employee==0)
	  	{
		   	$data = array(
				'type' => 'Error',
				'message' => 'Employee is required.',
				'button' => 1
			);
	 	}
	   	echo json_encode($data);
	}

	public function get_boss($id = NULL)
	{
		$data = $this->boss->get($id);
		echo json_encode($data);
	}

	public function create_boss()
	{
		$save_method = "create";
		$this->_validate($save_method);

		$data = array(
			'boss_employee' => $this->input->post('employee'),
			'boss_area' => $this->input->post('area'),
			'boss_status' => 'Active'
		);

		$this->boss->create($data);
		echo json_encode(array("status" => TRUE));
	}

	public function update_boss()
	{
		$save_method = "update";
		$this->_validate($save_method);
		$data = array(
			'boss_employee' => $this->input->post('employee'),
			'boss_area' => $this->input->post('area'),
			'boss_status' => 'Active'
		);

		$this->boss->update(array('boss_id' => $this->input->post('id')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function activate_boss($id)
	{
		$this->boss->activate($id);
		echo json_encode(array("status" => TRUE));
	}

	public function deactivate_boss($id)
	{
		$this->boss->deactivate($id);
		echo json_encode(array("status" => TRUE));
	}

	private function _validate($save_method)
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('employee') == "")
		{
			$data['inputerror'][] = 'employee';
			$data['error_string'][] = 'Employee is required.';
			$data['status'] = FALSE;
		}
		if($this->input->post('area') == 0)
		{
			$data['inputerror'][] = 'area';
			$data['error_string'][] = 'Area is required.';
			$data['status'] = FALSE;
		}
		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}
}