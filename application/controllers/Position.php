<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Position extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('position_model','position');
		$this->load->model('area_model','area');
	}

	public function index()
	{
		if($this->session->userdata('is_logued_in') === TRUE && $this->session->userdata('employee_rol') != 4)
		{
			$data = array(
	    		'controller' => 'positions',
	    		'areas' => $this->area->get_all()
	    	);
	    	
			$this->load->view('templates/links');
	        $this->load->view('templates/navbar');
	        $this->load->view('templates/sidebar');
			$this->load->view('positions/index',$data);
			$this->load->view('templates/control_sidebar');
	        $this->load->view('templates/footer');
        }
        else
        {
            show_404();
		}
	}

	public function list_active_positions()
	{
		$list = $this->position->get_datatables_actives();
		$data = array();
		$no = $_POST['start'];
		$i = 1;
		foreach ($list as $position_data)
		{
			$no++;
			$row = array();
			$row[] = $i;
			$row[] = $position_data->position_name;
			$row[] = $position_data->area_name;
			$row[] = $position_data->position_description;
			$row[] = $position_data->position_status;
			if($this->session->userdata('employee_rol') == 1 || $this->session->userdata('employee_rol') == 2)
			{
				$row[] =
					'<a class="btn btn-link" href="javascript:void(0)" title="Update" onclick="get_position('."'".$position_data->position_id."'".')" data-toggle="modal" data-target="#position_modal">
						<i class="fa fa-pencil"></i>
					</a>
					<a class="btn btn-link" href="javascript:void(0)" title="Deactivate" onclick="deactivate_position('."'".$position_data->position_id."'".')">
						<i class="fa fa-ban"></i>
					</a>';
			}
			else
			{
				$row[] = 'No actions';	
			}
			$data[] = $row;
			$i++;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->position->count_all(),
			"recordsFiltered" => $this->position->count_filtered_actives(),
			"data" => $data
		);
		echo json_encode($output);
	}

	public function list_inactive_positions()
	{
		$list = $this->position->get_datatables_inactives();
		$data = array();
		$no = $_POST['start'];
		$i = 1;
		foreach ($list as $position_data)
		{
			$no++;
			$row = array();
			$row[] = $i;
			$row[] = $position_data->position_name;
			$row[] = $position_data->area_name;
			$row[] = $position_data->position_description;
			$row[] = $position_data->position_status;
			if($this->session->userdata('employee_rol') == 1 || $this->session->userdata('employee_rol') == 2)
			{
				$row[] =
					'<a class="btn btn-link" href="javascript:void(0)" title="Activate" onclick="activate_position('."'".$position_data->position_id."'".')">
						<i class="fa fa-check"></i>
					</a>';
			}
			else
			{
				$row[] = 'No actions';	
			}
			$data[] = $row;
			$i++;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->position->count_all(),
			"recordsFiltered" => $this->position->count_filtered_inactives(),
			"data" => $data
		);
		echo json_encode($output);
	}

	public function validate_name()
	{
		$name = $this->input->get('name');

		if ($name!='')
		{
		    $validation = $this->position->validate_by_name($name);
		    if ($validation > 0)
			{
				$data = array(
				'type' => 'Warning',
				'message' => 'Name already exists.',
				'button' => 1
				);
			}
			else if ($validation < 1)
			{
				$data = array(
				'type' => 'Notice',
				'message' => 'Name is available.',
				'button' => 0
				);
			}
	  	}
	  	else if($id=='')
	  	{
		   	$data = array(
				'type' => 'Error',
				'message' => 'Name is required.',
				'button' => 1
			);
	 	}
	   	echo json_encode($data);
	}

	public function get_position($id = NULL)
	{
		$data = $this->position->get($id);
		echo json_encode($data);
	}

	public function create_position()
	{
		$save_method = "create";
		$this->_validate($save_method);

		$data = array(
			'position_name' => $this->input->post('name'),
			'position_area' => $this->input->post('area'),
			'position_description' => $this->input->post('description'),
			'position_status' => 'Active'
		);

		$this->position->create($data);
		echo json_encode(array("status" => TRUE));
	}

	public function update_position()
	{
		$save_method = "update";
		$this->_validate($save_method);
		$data = array(
			'position_name' => $this->input->post('name'),
			'position_area' => $this->input->post('area'),
			'position_description' => $this->input->post('description'),
			'position_status' => 'Active'
		);

		$this->position->update(array('position_id' => $this->input->post('id')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function activate_position($id)
	{
		$this->position->activate($id);
		echo json_encode(array("status" => TRUE));
	}

	public function deactivate_position($id)
	{
		$this->position->deactivate($id);
		echo json_encode(array("status" => TRUE));
	}

	private function _validate($save_method)
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('name') == '')
		{
			$data['inputerror'][] = 'name';
			$data['error_string'][] = 'Name is required.';
			$data['status'] = FALSE;
		}
		if($this->input->post('area') == 0)
		{
			$data['inputerror'][] = 'area';
			$data['error_string'][] = 'Area is required.';
			$data['status'] = FALSE;
		}
		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}
}