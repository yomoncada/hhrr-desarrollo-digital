<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Absence extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('absence_model','absence');
		$this->load->model('employee_model','employee');
	}

	public function index()
	{
		if($this->session->userdata('is_logued_in') === TRUE && $this->session->userdata('employee_rol') == 1 || $this->session->userdata('employee_rol') == 2)
		{
			$data = array(
	    		'controller' => 'absences',
	    		'employees' => $this->employee->get_all()
	    	);
	    	
			$this->load->view('templates/links');
	        $this->load->view('templates/navbar');
	        $this->load->view('templates/sidebar');
			$this->load->view('absences/index',$data);
			$this->load->view('templates/control_sidebar');
	        $this->load->view('templates/footer');
        }
        else
        {
            show_404();
		}
	}

	public function list_pending_absences()
	{
		$list = $this->absence->get_pending_datatables();
		$data = array();
		$no = $_POST['start'];
		$i = 1;
		foreach ($list as $absence_data)
		{
			$no++;
			$row = array();
			$row[] = $i;
			$row[] = '<a href="profile/view/'.$absence_data->absence_employee.'");?>' . $absence_data->employee_firstname . ' ' . $absence_data->employee_firstlastname . '</a>';
			$row[] = $absence_data->absence_days . ' days';
			$row[] = $absence_data->absence_type;
			$row[] = $absence_data->absence_startdate;
			$row[] = $absence_data->absence_enddate;
			$row[] = $absence_data->absence_status;
			$row[] =
				$row[] =
				'<a class="btn btn-link" href="javascript:void(0)" title="Process" onclick="get_absence('."'".$absence_data->absence_id."'".')" data-toggle="modal" data-target="#absence_modal">
					<i class="fa fa-edit"></i>
				</a>';
			$data[] = $row;
			$i++;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->absence->count_all(),
			"recordsFiltered" => $this->absence->count_pending_filtered(),
			"data" => $data
		);
		echo json_encode($output);
	}

	public function list_processed_absences()
	{
		$list = $this->absence->get_processed_datatables();
		$data = array();
		$no = $_POST['start'];
		$i = 1;
		foreach ($list as $absence_data)
		{
			$no++;
			$row = array();
			$row[] = $i;
			$row[] = '<a href="profile/view/'.$absence_data->absence_employee.'");?>' . $absence_data->employee_firstname . ' ' . $absence_data->employee_firstlastname . '</a>';
			$row[] = $absence_data->absence_days . ' days';
			$row[] = $absence_data->absence_type;
			$row[] = $absence_data->absence_startdate;
			$row[] = $absence_data->absence_enddate;
			$row[] = $absence_data->absence_status;
			$row[] =
				$row[] =
				'<a class="btn btn-link" href="javascript:void(0)" title="Read" onclick="get_absence_unmodifiable('."'".$absence_data->absence_id."'".')" data-toggle="modal" data-target="#absence_modal">
					<i class="fa fa-eye"></i>
				</a>';
			$data[] = $row;
			$i++;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->absence->count_all(),
			"recordsFiltered" => $this->absence->count_processed_filtered(),
			"data" => $data
		);
		echo json_encode($output);
	}

	public function get_absence($id = NULL)
	{
		$data = $this->absence->get($id);
		echo json_encode($data);
	}

	public function create_absence()
	{
		$save_method = "create";
		$this->_validate($save_method);

		$employee = $this->employee->get_by_id($this->input->post('employee'));

		$data = array(
			'absence_employee' => $this->input->post('employee'),
			'absence_area' => $employee->employee_area,
			'absence_days' => $this->input->post('days'),
			'absence_startdate' => $this->input->post('startdate'),
			'absence_enddate' => $this->input->post('enddate'),
			'absence_reason' => $this->input->post('reason'),
			'absence_file' => $this->input->post('file'),
			'absence_type' => $this->input->post('type'),
			'absence_status' => 'Pending'
		);

		$this->absence->save($data);
		echo json_encode(array("status" => TRUE));
	}

	public function update_absence()
	{
		$save_method = "update";
		$this->_validate($save_method);
		
		$employee = $this->employee->get_by_id($this->input->post('employee'));

		$data = array(
			'absence_employee' => $this->input->post('employee'),
			'absence_area' => $employee->employee_area,
			'absence_days' => $this->input->post('days'),
			'absence_startdate' => $this->input->post('startdate'),
			'absence_enddate' => $this->input->post('enddate'),
			'absence_reason' => $this->input->post('reason'),
			'absence_file' => $this->input->post('file'),
			'absence_type' => $this->input->post('type'),
			'absence_status' => 'Processed'
		);

		$this->absence->update(array('absence_id' => $this->input->post('id')), $data);
		echo json_encode(array("status" => TRUE));
	}

	private function _validate($save_method)
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('employee') == "0")
		{
			$data['inputerror'][] = 'employee';
			$data['error_string'][] = 'Employee is required.';
			$data['status'] = FALSE;
		}
		if($this->input->post('days') == '')
		{
			$data['inputerror'][] = 'days';
			$data['error_string'][] = 'Days is required.';
			$data['status'] = FALSE;
		}
		if($this->input->post('startdate') == "0")
		{
			$data['inputerror'][] = 'startdate';
			$data['error_string'][] = 'Start date is required.';
			$data['status'] = FALSE;
		}
		if($this->input->post('enddate') == "0")
		{
			$data['inputerror'][] = 'enddate';
			$data['error_string'][] = 'End date is required.';
			$data['status'] = FALSE;
		}
		if($this->input->post('type') == "0")
		{
			$data['inputerror'][] = 'type';
			$data['error_string'][] = 'Type is required.';
			$data['status'] = FALSE;
		}
		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}
}