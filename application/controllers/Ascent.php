<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ascent extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('area_model','area');
		$this->load->model('ascent_model','ascent');
		$this->load->model('employee_model','employee');
		$this->load->model('position_model','position');
	}

	public function index()
	{
		if($this->session->userdata('is_logued_in') === TRUE && $this->session->userdata('employee_rol') == 1 || $this->session->userdata('employee_rol') == 2)
		{
			$data = array(
	    		'controller' => 'ascents',
	    		'employees' => $this->employee->get_all(),
	    		'areas' => $this->area->get_all(),
	    		'positions' => $this->position->get_all()
	    	);

			$this->load->view('templates/links');
	        $this->load->view('templates/navbar');
	        $this->load->view('templates/sidebar');
			$this->load->view('ascents/index',$data);
			$this->load->view('templates/control_sidebar');
	        $this->load->view('templates/footer');
        }
        else
        {
            show_404();
		}
	}

	public function list_pending_ascents()
	{
		$list = $this->ascent->get_pending_datatables();
		$data = array();
		$no = $_POST['start'];
		$i = 1;
		foreach ($list as $ascent_data)
		{
			$no++;
			$row = array();
			$row[] = $i;
			$row[] = '<a href="profile/view/'.$ascent_data->ascent_employee.'");?>' . $ascent_data->employee_firstname . ' ' . $ascent_data->employee_firstlastname . '</a>';
			$row[] = $ascent_data->area_name;
			$row[] = $ascent_data->position_name;
			$row[] = $ascent_data->ascent_salary . '$';
			$row[] = $ascent_data->ascent_date;
			$row[] = $ascent_data->ascent_status;
			$row[] =
				'<a class="btn btn-link" href="javascript:void(0)" title="Process" onclick="get_ascent('."'".$ascent_data->ascent_id."'".')" data-toggle="modal" data-target="#ascent_modal">
					<i class="fa fa-edit"></i>
				</a>';
			$data[] = $row;
			$i++;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->ascent->count_all(),
			"recordsFiltered" => $this->ascent->count_pending_filtered(),
			"data" => $data
		);
		echo json_encode($output);
	}

	public function list_processed_ascents()
	{
		$list = $this->ascent->get_processed_datatables();
		$data = array();
		$no = $_POST['start'];
		$i = 1;
		foreach ($list as $ascent_data)
		{
			$no++;
			$row = array();
			$row[] = $i;
			$row[] = '<a href="profile/view/'.$ascent_data->ascent_employee.'");?>' . $ascent_data->employee_firstname . ' ' . $ascent_data->employee_firstlastname . '</a>';
			$row[] = $ascent_data->area_name;
			$row[] = $ascent_data->position_name;
			$row[] = $ascent_data->ascent_salary . '$';
			$row[] = $ascent_data->ascent_date;
			$row[] = $ascent_data->ascent_status;
			$row[] =
				'<a class="btn btn-link" href="javascript:void(0)" title="Read" onclick="get_ascent_unmodifiable('."'".$ascent_data->ascent_id."'".')" data-toggle="modal" data-target="#ascent_modal">
					<i class="fa fa-eye"></i>
				</a>';
			$data[] = $row;
			$i++;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->ascent->count_all(),
			"recordsFiltered" => $this->ascent->count_processed_filtered(),
			"data" => $data
		);
		echo json_encode($output);
	}

	public function get_ascent($id = NULL)
	{
		$data = $this->ascent->get($id);
		echo json_encode($data);
	}

	public function create_ascent()
	{
		$save_method = "create";
		$this->_validate($save_method);

		$data = array(
			'ascent_employee' => $this->input->post('employee'),
			'ascent_area' => $this->input->post('area'),
			'ascent_position' => $this->input->post('position'),
			'ascent_salary' => $this->input->post('salary'),
			'ascent_date' => $this->input->post('date'),
			'ascent_status' => 'Pending'
		);

		$this->ascent->save($data);
		echo json_encode(array("status" => TRUE));
	}

	public function update_ascent()
	{
		$save_method = "update";
		$this->_validate($save_method);

		$data = array(
			'ascent_employee' => $this->input->post('employee'),
			'ascent_area' => $this->input->post('area'),
			'ascent_position' => $this->input->post('position'),
			'ascent_salary' => $this->input->post('salary'),
			'ascent_date' => $this->input->post('date'),
			'ascent_status' => 'Processed'
		);

		$this->ascent->update(array('ascent_id' => $this->input->post('id')), $data);
		$this->ascent->update_employee_area($this->input->post('employee'), $this->input->post('area'));
		$this->ascent->update_employee_position($this->input->post('employee'), $this->input->post('position'));
		$this->ascent->update_employee_salary($this->input->post('employee'), $this->input->post('salary'));
		echo json_encode(array("status" => TRUE));
	}

	private function _validate($save_method)
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('employee') == "0")
		{
			$data['inputerror'][] = 'employee';
			$data['error_string'][] = 'Employee is required.';
			$data['status'] = FALSE;
		}
		if($this->input->post('area') == "0")
		{
			$data['inputerror'][] = 'area';
			$data['error_string'][] = 'Area is required.';
			$data['status'] = FALSE;
		}
		if($this->input->post('position') == "0")
		{
			$data['inputerror'][] = 'position';
			$data['error_string'][] = 'Position is required.';
			$data['status'] = FALSE;
		}
		if($this->input->post('salary') == '')
		{
			$data['inputerror'][] = 'salary';
			$data['error_string'][] = 'Salary is required.';
			$data['status'] = FALSE;
		}
		if($this->input->post('date') == '')
		{
			$data['inputerror'][] = 'date';
			$data['error_string'][] = 'Date is required.';
			$data['status'] = FALSE;
		}
		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}
}