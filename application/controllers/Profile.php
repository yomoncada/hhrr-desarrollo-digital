<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('profile_model','profile');
		$this->load->model('country_model','country');
		$this->load->model('region_model','region');
		$this->load->model('city_model','city');
		$this->load->model('employee_model','employee');
	}

	public function index()
	{
		if($this->session->userdata('is_logued_in') === TRUE)
		{
			$data = array(
	    		'controller' => 'profiles',
	    		'countries' => $this->country->get_all(),
	    		'regions' => $this->region->get_all(),
	    		'cities' => $this->city->get_all()
	    	);

	    	$this->session->unset_userdata('employee_profile');

			$this->load->view('templates/links');
	        $this->load->view('templates/navbar');
	        $this->load->view('templates/sidebar');
			$this->load->view('pages/profile',$data);
			$this->load->view('templates/control_sidebar');
	        $this->load->view('templates/footer');
        }
        else
        {
            show_404();
		}
	}

	public function view($id = NULL)
    {
        if($this->session->userdata('is_logued_in') === TRUE && $this->session->userdata('employee_rol') != 4)
        {
			$employee = $this->profile->get_profile($id);
	    	$this->session->unset_userdata('module');

			if(empty($employee))
			{
				show_404();
			}

			else
			{
				if($this->session->userdata('employee_id') != $id)
				{
		            $this->session->set_userdata('employee_profile',$id);

		            $this->load->view('templates/links');
		        	$this->load->view('templates/navbar');
		        	$this->load->view('templates/sidebar');
					$this->load->view('pages/profile');
		            $this->load->view('templates/control_sidebar');
		        	$this->load->view('templates/footer');
		        }
		        else
		        {
		        	redirect('profile');
				}
	        }
	    }
    }

	public function list_employee_absences()
	{
		if(empty($this->session->userdata('employee_profile')))
		{
			$list = $this->profile->get_absences_datatables($this->session->userdata('employee_id'));
		}
		else
		{
			$list = $this->profile->get_absences_datatables($this->session->userdata('employee_profile'));
		}
		$data = array();
		$no = $_POST['start'];
		$i = 1;
		foreach ($list as $absence_data)
		{
			$no++;
			$row = array();
			$row[] = $i;
			$row[] = $absence_data->absence_days . ' days';
			$row[] = $absence_data->absence_type;
			$row[] = $absence_data->absence_startdate;
			$row[] = $absence_data->absence_enddate;
			$data[] = $row;
			$i++;
		}

		if(empty($this->session->userdata('employee_profile')))
		{
			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->profile->count_all_absences($this->session->userdata('employee_id')),
				"recordsFiltered" => $this->profile->count_absences_filtered($this->session->userdata('employee_id')),
				"data" => $data
			);
		}
		else
		{
			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->profile->count_all_absences($this->session->userdata('employee_profile')),
				"recordsFiltered" => $this->profile->count_absences_filtered($this->session->userdata('employee_profile')),
				"data" => $data
			);
		}
		echo json_encode($output);
	}

	public function list_employee_ascents()
	{
		if(empty($this->session->userdata('employee_profile')))
		{
			$list = $this->profile->get_ascents_datatables($this->session->userdata('employee_id'));
		}
		else
		{
			$list = $this->profile->get_ascents_datatables($this->session->userdata('employee_profile'));
		}
		$data = array();
		$no = $_POST['start'];
		$i = 1;
		foreach ($list as $ascent_data)
		{
			$no++;
			$row = array();
			$row[] = $i;
			$row[] = $ascent_data->area_name;
			$row[] = $ascent_data->position_name;
			$row[] = $ascent_data->ascent_salary . '$';
			$row[] = $ascent_data->ascent_date;
			$data[] = $row;
			$i++;
		}

		if(empty($this->session->userdata('employee_profile')))
		{
			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->profile->count_all_ascents($this->session->userdata('employee_id')),
				"recordsFiltered" => $this->profile->count_ascents_filtered($this->session->userdata('employee_id')),
				"data" => $data
			);
		}
		else
		{
			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->profile->count_all_ascents($this->session->userdata('employee_profile')),
				"recordsFiltered" => $this->profile->count_ascents_filtered($this->session->userdata('employee_profile')),
				"data" => $data
			);
		}
		echo json_encode($output);
	}

	public function list_employee_bonuses()
	{
		if(empty($this->session->userdata('employee_profile')))
		{
			$list = $this->profile->get_bonuses_datatables($this->session->userdata('employee_id'));
		}
		else
		{
			$list = $this->profile->get_bonuses_datatables($this->session->userdata('employee_profile'));
		}
		$data = array();
		$no = $_POST['start'];
		$i = 1;
		foreach ($list as $bonus_data)
		{
			$no++;
			$row = array();
			$row[] = $i;
			$row[] = $bonus_data->bonus_value . '$';
			$row[] = $bonus_data->bonus_date;
			$data[] = $row;
			$i++;
		}

		if(empty($this->session->userdata('employee_profile')))
		{
			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->profile->count_all_bonuses($this->session->userdata('employee_id')),
				"recordsFiltered" => $this->profile->count_bonuses_filtered($this->session->userdata('employee_id')),
				"data" => $data
			);
		}
		else
		{
			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->profile->count_all_bonuses($this->session->userdata('employee_profile')),
				"recordsFiltered" => $this->profile->count_bonuses_filtered($this->session->userdata('employee_profile')),
				"data" => $data
			);
		}
		echo json_encode($output);
	}

	public function list_employee_increases()
	{
		if(empty($this->session->userdata('employee_profile')))
		{
			$list = $this->profile->get_increases_datatables($this->session->userdata('employee_id'));
		}
		else
		{
			$list = $this->profile->get_increases_datatables($this->session->userdata('employee_profile'));
		}
		$data = array();
		$no = $_POST['start'];
		$i = 1;
		foreach ($list as $increase_data)
		{
			$no++;
			$row = array();
			$row[] = $i;
			$row[] = $increase_data->increase_type;
			if($increase_data->increase_type == 'Percent')
			{
				$row[] = $increase_data->increase_value  . '%';
			}
			else if($increase_data->increase_type == 'Fixed')
			{
				$row[] = $increase_data->increase_value  . '$';
			}
			$row[] = $increase_data->increase_date;
			$data[] = $row;
			$i++;
		}

		if(empty($this->session->userdata('employee_profile')))
		{
			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->profile->count_all_increases($this->session->userdata('employee_id')),
				"recordsFiltered" => $this->profile->count_increases_filtered($this->session->userdata('employee_id')),
				"data" => $data
			);
		}
		else
		{
			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->profile->count_all_increases($this->session->userdata('employee_profile')),
				"recordsFiltered" => $this->profile->count_increases_filtered($this->session->userdata('employee_profile')),
				"data" => $data
			);
		}
		echo json_encode($output);
	}

	public function get_profile($id = NULL)
	{
		if(empty($this->session->userdata('employee_profile')))
		{
			$data = $this->profile->get_profile($this->session->userdata('employee_id'));
		}
		else
		{
			$data = $this->profile->get_profile($this->session->userdata('employee_profile'));
		}
		echo json_encode($data);
	}
}