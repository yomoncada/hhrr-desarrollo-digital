<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class City extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('city_model','city');
		$this->load->model('region_model','region');
	}

	public function index()
	{
		if($this->session->userdata('is_logued_in') === TRUE && $this->session->userdata('employee_rol') == 1)
		{
			$data = array(
	    		'controller' => 'cities',
	    		'regions' => $this->region->get_all()
	    	);
	    	
			$this->load->view('templates/links');
	        $this->load->view('templates/navbar');
	        $this->load->view('templates/sidebar');
			$this->load->view('cities/index',$data);
			$this->load->view('templates/control_sidebar');
	        $this->load->view('templates/footer');
        }
        else
        {
            show_404();
		}
	}

	public function list_active_cities()
	{
		$list = $this->city->get_datatables_actives();
		$data = array();
		$no = $_POST['start'];
		$i = 1;
		foreach ($list as $city_data)
		{
			$no++;
			$row = array();
			$row[] = $i;
			$row[] = $city_data->city_name;
			$row[] = $city_data->region_name;
			$row[] = $city_data->city_status;
			$row[] =
				'<a class="btn btn-link" href="javascript:void(0)" title="Update" onclick="get_city('."'".$city_data->city_id."'".')" data-toggle="modal" data-target="#city_modal">
					<i class="fa fa-pencil"></i>
				</a>
				<a class="btn btn-link" href="javascript:void(0)" title="Deactivate" onclick="deactivate_city('."'".$city_data->city_id."'".')">
					<i class="fa fa-ban"></i>
				</a>';
			$data[] = $row;
			$i++;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->city->count_all(),
			"recordsFiltered" => $this->city->count_filtered_actives(),
			"data" => $data
		);
		echo json_encode($output);
	}

	public function list_inactive_cities()
	{
		$list = $this->city->get_datatables_inactives();
		$data = array();
		$no = $_POST['start'];
		$i = 1;
		foreach ($list as $city_data)
		{
			$no++;
			$row = array();
			$row[] = $i;
			$row[] = $city_data->city_name;
			$row[] = $city_data->region_name;
			$row[] = $city_data->city_status;
			$row[] =
				'<a class="btn btn-link" href="javascript:void(0)" title="Activate" onclick="activate_city('."'".$city_data->city_id."'".')">
					<i class="fa fa-check"></i>
				</a>';
			$data[] = $row;
			$i++;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->city->count_all(),
			"recordsFiltered" => $this->city->count_filtered_inactives(),
			"data" => $data
		);
		echo json_encode($output);
	}

	public function validate_name()
	{
		$name = $this->input->get('name');

		if ($name!='')
		{
		    $validation = $this->city->validate_by_name($name);
		    if ($validation > 0)
			{
				$data = array(
				'type' => 'Warning',
				'message' => 'Name already exists.',
				'button' => 1
				);
			}
			else if ($validation < 1)
			{
				$data = array(
				'type' => 'Notice',
				'message' => 'Name is available.',
				'button' => 0
				);
			}
	  	}
	  	else if($id=='')
	  	{
		   	$data = array(
				'type' => 'Error',
				'message' => 'Name is required.',
				'button' => 1
			);
	 	}
	   	echo json_encode($data);
	}

	public function get_city($id = NULL)
	{
		$data = $this->city->get($id);
		echo json_encode($data);
	}

	public function create_city()
	{
		$save_method = "create";
		$this->_validate($save_method);

		$data = array(
			'city_name' => $this->input->post('name'),
			'city_region' => $this->input->post('region'),
			'city_status' => 'Active'
		);

		$this->city->create($data);
		echo json_encode(array("status" => TRUE));
	}

	public function update_city()
	{
		$save_method = "update";
		$this->_validate($save_method);
		$data = array(
			'city_name' => $this->input->post('name'),
			'city_region' => $this->input->post('region'),
			'city_status' => 'Active'
		);

		$this->city->update(array('city_id' => $this->input->post('id')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function activate_city($id)
	{
		$this->city->activate($id);
		echo json_encode(array("status" => TRUE));
	}

	public function deactivate_city($id)
	{
		$this->city->deactivate($id);
		echo json_encode(array("status" => TRUE));
	}

	private function _validate($save_method)
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('name') == '')
		{
			$data['inputerror'][] = 'name';
			$data['error_string'][] = 'Name is required.';
			$data['status'] = FALSE;
		}
		if($this->input->post('region') == 0)
		{
			$data['inputerror'][] = 'region';
			$data['error_string'][] = 'Region is required.';
			$data['status'] = FALSE;
		}
		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}
}