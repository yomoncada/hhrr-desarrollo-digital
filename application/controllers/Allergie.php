<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Allergie extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('allergie_model','allergie');
	}

	public function index()
	{
		if($this->session->userdata('is_logued_in') === TRUE && $this->session->userdata('employee_rol') == 1)
		{
			$data = array(
	    		'controller' => 'allergies'
	    	);
	    	
			$this->load->view('templates/links');
	        $this->load->view('templates/navbar');
	        $this->load->view('templates/sidebar');
			$this->load->view('allergies/index',$data);
			$this->load->view('templates/control_sidebar');
	        $this->load->view('templates/footer');
        }
        else
        {
            show_404();
		}
	}

	public function list_active_allergies()
	{
		$list = $this->allergie->get_datatables_actives();
		$data = array();
		$no = $_POST['start'];
		$i = 1;
		foreach ($list as $allergie_data)
		{
			$no++;
			$row = array();
			$row[] = $i;
			$row[] = $allergie_data->allergie_name;
			$row[] = $allergie_data->allergie_description;
			$row[] = $allergie_data->allergie_status;
			$row[] =
				'<a class="btn btn-link" href="javascript:void(0)" title="Update" onclick="get_allergie('."'".$allergie_data->allergie_id."'".')" data-toggle="modal" data-target="#allergie_modal">
					<i class="fa fa-pencil"></i>
				</a>
				<a class="btn btn-link" href="javascript:void(0)" title="Deactivate" onclick="deactivate_allergie('."'".$allergie_data->allergie_id."'".')">
					<i class="fa fa-ban"></i>
				</a>';
			$data[] = $row;
			$i++;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->allergie->count_all(),
			"recordsFiltered" => $this->allergie->count_filtered_actives(),
			"data" => $data
		);
		echo json_encode($output);
	}

	public function list_inactive_allergies()
	{
		$list = $this->allergie->get_datatables_inactives();
		$data = array();
		$no = $_POST['start'];
		$i = 1;
		foreach ($list as $allergie_data)
		{
			$no++;
			$row = array();
			$row[] = $i;
			$row[] = $allergie_data->allergie_name;
			$row[] = $allergie_data->allergie_description;
			$row[] = $allergie_data->allergie_status;
			$row[] =
				'<a class="btn btn-link" href="javascript:void(0)" title="Activate" onclick="activate_allergie('."'".$allergie_data->allergie_id."'".')">
					<i class="fa fa-check"></i>
				</a>';
			$data[] = $row;
			$i++;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->allergie->count_all(),
			"recordsFiltered" => $this->allergie->count_filtered_inactives(),
			"data" => $data
		);
		echo json_encode($output);
	}

	public function validate_name()
	{
		$name = $this->input->get('name');

		if ($name!='')
		{
		    $validation = $this->allergie->validate_by_name($name);
		    if ($validation > 0)
			{
				$data = array(
				'type' => 'Warning',
				'message' => 'Name already exists.',
				'button' => 1
				);
			}
			else if ($validation < 1)
			{
				$data = array(
				'type' => 'Notice',
				'message' => 'Name is available.',
				'button' => 0
				);
			}
	  	}
	  	else if($id=='')
	  	{
		   	$data = array(
				'type' => 'Error',
				'message' => 'Name is required.',
				'button' => 1
			);
	 	}
	   	echo json_encode($data);
	}

	public function get_allergie($id = NULL)
	{
		$data = $this->allergie->get($id);
		echo json_encode($data);
	}

	public function create_allergie()
	{
		$save_method = "create";
		$this->_validate($save_method);

		$data = array(
			'allergie_name' => $this->input->post('name'),
			'allergie_description' => $this->input->post('description'),
			'allergie_status' => 'Active'
		);

		$this->allergie->create($data);
		echo json_encode(array("status" => TRUE));
	}

	public function update_allergie()
	{
		$save_method = "update";
		$this->_validate($save_method);
		$data = array(
			'allergie_name' => $this->input->post('name'),
			'allergie_description' => $this->input->post('description'),
			'allergie_status' => 'Active'
		);

		$this->allergie->update(array('allergie_id' => $this->input->post('id')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function activate_allergie($id)
	{
		$this->allergie->activate($id);
		echo json_encode(array("status" => TRUE));
	}

	public function deactivate_allergie($id)
	{
		$this->allergie->deactivate($id);
		echo json_encode(array("status" => TRUE));
	}

	private function _validate($save_method)
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('name') == '')
		{
			$data['inputerror'][] = 'name';
			$data['error_string'][] = 'Name is required.';
			$data['status'] = FALSE;
		}
		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}
}