<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Disease extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('disease_model','disease');
		//$this->load->model('position_model','position');
	}

	public function index()
	{
		if($this->session->userdata('is_logued_in') === TRUE && $this->session->userdata('employee_rol') == 1)
		{
			$data = array(
	    		'controller' => 'diseases'
	    	);
	    	
			$this->load->view('templates/links');
	        $this->load->view('templates/navbar');
	        $this->load->view('templates/sidebar');
			$this->load->view('diseases/index',$data);
			$this->load->view('templates/control_sidebar');
	        $this->load->view('templates/footer');
        }
        else
        {
            show_404();
		}
	}

	public function list_active_diseases()
	{
		$list = $this->disease->get_datatables_actives();
		$data = array();
		$no = $_POST['start'];
		$i = 1;
		foreach ($list as $disease_data)
		{
			$no++;
			$row = array();
			$row[] = $i;
			$row[] = $disease_data->disease_name;
			$row[] = $disease_data->disease_description;
			$row[] = $disease_data->disease_status;
			$row[] =
				'<a class="btn btn-link" href="javascript:void(0)" title="Update" onclick="get_disease('."'".$disease_data->disease_id."'".')" data-toggle="modal" data-target="#disease_modal">
					<i class="fa fa-pencil"></i>
				</a>
				<a class="btn btn-link" href="javascript:void(0)" title="Deactivate" onclick="deactivate_disease('."'".$disease_data->disease_id."'".')">
					<i class="fa fa-ban"></i>
				</a>';
			$data[] = $row;
			$i++;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->disease->count_all(),
			"recordsFiltered" => $this->disease->count_filtered_actives(),
			"data" => $data
		);
		echo json_encode($output);
	}

	public function list_inactive_diseases()
	{
		$list = $this->disease->get_datatables_inactives();
		$data = array();
		$no = $_POST['start'];
		$i = 1;
		foreach ($list as $disease_data)
		{
			$no++;
			$row = array();
			$row[] = $i;
			$row[] = $disease_data->disease_name;
			$row[] = $disease_data->disease_description;
			$row[] = $disease_data->disease_status;
			$row[] =
				'<a class="btn btn-link" href="javascript:void(0)" title="Activate" onclick="activate_disease('."'".$disease_data->disease_id."'".')">
					<i class="fa fa-check"></i>
				</a>';
			$data[] = $row;
			$i++;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->disease->count_all(),
			"recordsFiltered" => $this->disease->count_filtered_inactives(),
			"data" => $data
		);
		echo json_encode($output);
	}

	public function validate_name()
	{
		$name = $this->input->get('name');

		if ($name!='')
		{
		    $validation = $this->disease->validate_by_name($name);
		    if ($validation > 0)
			{
				$data = array(
				'type' => 'Warning',
				'message' => 'Name already exists.',
				'button' => 1
				);
			}
			else if ($validation < 1)
			{
				$data = array(
				'type' => 'Notice',
				'message' => 'Name is available.',
				'button' => 0
				);
			}
	  	}
	  	else if($id=='')
	  	{
		   	$data = array(
				'type' => 'Error',
				'message' => 'Name is required.',
				'button' => 1
			);
	 	}
	   	echo json_encode($data);
	}

	public function get_disease($id = NULL)
	{
		$data = $this->disease->get($id);
		echo json_encode($data);
	}

	public function create_disease()
	{
		$save_method = "create";
		$this->_validate($save_method);

		$data = array(
			'disease_name' => $this->input->post('name'),
			'disease_description' => $this->input->post('description'),
			'disease_status' => 'Active'
		);

		$this->disease->create($data);
		echo json_encode(array("status" => TRUE));
	}

	public function update_disease()
	{
		$save_method = "update";
		$this->_validate($save_method);
		$data = array(
			'disease_name' => $this->input->post('name'),
			'disease_description' => $this->input->post('description'),
			'disease_status' => 'Active'
		);

		$this->disease->update(array('disease_id' => $this->input->post('id')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function activate_disease($id)
	{
		$this->disease->activate($id);
		echo json_encode(array("status" => TRUE));
	}

	public function deactivate_disease($id)
	{
		$this->disease->deactivate($id);
		echo json_encode(array("status" => TRUE));
	}

	private function _validate($save_method)
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('name') == '')
		{
			$data['inputerror'][] = 'name';
			$data['error_string'][] = 'Name is required.';
			$data['status'] = FALSE;
		}
		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}
}