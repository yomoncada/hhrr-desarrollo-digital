<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class System extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('employee_model','employee');
		$this->load->model('area_model','area');
		$this->load->model('absence_model','absence');
		$this->load->model('ascent_model','ascent');
		$this->load->model('bonus_model','bonus');
		$this->load->model('increase_model','increase');
	}
		
	public function login($error = null)
	{
		if($this->session->userdata('is_logued_in') == FALSE)
		{
			$this->load->view('templates/links');
			$this->load->view('pages/login');
		}
		else
		{
			redirect('home');
		}
	}

	public function validate_login()
	{
		$this->_validate_login();
		$this->form_validation->set_rules('username', 'username', 'required');
		$this->form_validation->set_rules('password', 'password', 'required');

		if($this->form_validation->run() == TRUE)
		{
			$username = $this->input->post('username');
			$password = $this->input->post('password');
			$validation = $this->employee->validate_login($username,$password);
			if($validation > 0)
			{
				$employee_data = $this->employee->get($username,$password);
				$data = array(
					'is_logued_in' 	=> 		TRUE,
					'employee_id' 	=> 		$employee_data->employee_id,
					'employee_firstname' 	=> 		$employee_data->employee_firstname,
					'employee_firstlastname' 	=> 		$employee_data->employee_firstlastname,
					'employee_rol' 	=> 		$employee_data->employee_rol,
					'employee_area' 	=> 		$employee_data->employee_area
				);
				$this->session->set_userdata($data);

				$data = array(
					'status' => TRUE
				);
				echo json_encode($data);
			}
			else
			{
				$data = array(
					'status' => FALSE,
					'swalx' => 3,
				);
				echo json_encode($data);
			}
		}
	}

	public function validate_recovery()
	{
		$this->_validate_recovery();
		$this->form_validation->set_rules('username', 'username', 'required');
		$this->form_validation->set_rules('secques', 'secques', 'required');
		$this->form_validation->set_rules('secans', 'secans', 'required');

		if($this->form_validation->run() == TRUE)
		{
			$username = $this->input->post('username');
			$secques = $this->input->post('secques');
			$secans = $this->input->post('secans');
			$validation = $this->employee->validate_recovery($username,$secques,$secans);
			if($validation > 0)
			{
				$temporal_password = rand(100000,999999);
				$this->employee->set_temporal_password($username, $temporal_password);

				$data = array(
					'status' => TRUE,
					'temporal_password' => $temporal_password
				);
				echo json_encode($data);
			}
			else
			{
				$data = array(
					'status' => FALSE,
					'swalx' => 3,
				);
				echo json_encode($data);
			}
		}
	}

	public function home($page = 'home')
  	{ 
	    if ( ! file_exists(APPPATH.'views/pages/'.$page.'.php'))
	    {
	      show_404();
	    }
	    else
	    {
		    if($this->session->userdata('is_logued_in') === TRUE)
		    {
		    	if($this->session->userdata('employee_rol') == 3 || $this->session->userdata('employee_rol') == 4)
		    	{
		    		$data = array(
			    		'absences' => $this->absence->count_by_employee($this->session->userdata('employee_id')),
			    		'ascents' => $this->ascent->count_by_employee($this->session->userdata('employee_id')),
			    		'bonuses' => $this->bonus->count_by_employee($this->session->userdata('employee_id')),
			    		'increases' => $this->increase->count_by_employee($this->session->userdata('employee_id'))
			    	);
			    }
			    else if($this->session->userdata('employee_rol') == 1 || $this->session->userdata('employee_rol') == 2)
			    {
			    	$qty_areas = $this->area->count_all();
			    	$areas = $this->area->get_all();

				    	foreach ($areas as $area):
				    		$employees = $this->employee->count_by_area($area['area_id']);
					    	$absences = $this->absence->count_by_area($area['area_id']);
					    	$ascents = $this->ascent->count_by_area($area['area_id']);
					    	$increases = $this->increase->count_by_area($area['area_id']);

				    		$areas_control[$area['area_name']]['employees'] = $employees;
				    		$areas_control[$area['area_name']]['absences'] = $absences;
				    		$areas_control[$area['area_name']]['ascents'] = $ascents;
				    		$areas_control[$area['area_name']]['increases'] = $increases;
				    	endforeach;

			    	$data = array(
			    		'absences' => $this->absence->count_all(),
			    		'ascents' => $this->ascent->count_all(),
			    		'bonuses' => $this->bonus->count_all(),
			    		'increases' => $this->increase->count_all(),
			    		'areas' => $this->area->get_all(),
			    		'areas_control' => $areas_control
			    	);
			    }	

		        $this->load->view('templates/links');
		        $this->load->view('templates/navbar');
		        $this->load->view('templates/sidebar');
		        $this->load->view('pages/'.$page,$data);
		        $this->load->view('templates/control_sidebar');
		        $this->load->view('templates/footer');
		    }
		    else
		    {
		      	redirect('login');
		    }
	    }
	}

	public function logout()
	{
		$this->session->sess_destroy();
		$this->db->truncate('ci_sessions');
		redirect('login');
	}

	private function _validate_login()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;
		
		if($this->input->post('username') == '')
		{
			$data['inputerror'][] = 'username';
			$data['error_string'][] = 'Username is required.';
			$data['status'] = FALSE;
		}
		if($this->input->post('password') == '')
		{
			$data['inputerror'][] = 'password';
			$data['error_string'][] = 'Password is required.';
			$data['status'] = FALSE;
		}
		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

	private function _validate_recovery()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;
		
		if($this->input->post('username') == '')
		{
			$data['inputerror'][] = 'username';
			$data['error_string'][] = 'Username is required.';
			$data['status'] = FALSE;
		}
		if($this->input->post('secques') == '')
		{
			$data['inputerror'][] = 'secques';
			$data['error_string'][] = 'Secret Question is required.';
			$data['status'] = FALSE;
		}
		if($this->input->post('secans') == '')
		{
			$data['inputerror'][] = 'secans';
			$data['error_string'][] = 'Secret Answer is required.';
			$data['status'] = FALSE;
		}
		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

	
}