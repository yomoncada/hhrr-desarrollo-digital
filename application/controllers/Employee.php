<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Employee extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('employee_model','employee');
		$this->load->model('area_model','area');
		$this->load->model('grade_model','grade');
		$this->load->model('country_model','country');
		$this->load->model('region_model','region');
		$this->load->model('allergie_model','allergie');
		$this->load->model('disease_model','disease');
		$this->load->model('city_model','city');
		$this->load->model('rol_model','rol');
		$this->load->model('skill_model','skill');
		$this->load->model('boss_model','boss');
		$this->load->model('position_model','position');
	}

	public function index()
	{
		if($this->session->userdata('is_logued_in') === TRUE && $this->session->userdata('employee_rol') != 4)
		{
			$data = array(
	    		'controller' => 'employees',
	    		'areas' => $this->area->get_all(),
	    		'countries' => $this->country->get_all(),
	    		'regions' => $this->region->get_all(),
	    		'cities' => $this->city->get_all(),
	    		'roles' => $this->rol->get_all(),
	    		'bosses' => $this->boss->get_all(),
	    		'positions' => $this->position->get_all()
	    	);

			$this->load->view('templates/links');
	        $this->load->view('templates/navbar');
	        $this->load->view('templates/sidebar');
			$this->load->view('employees/index',$data);
			$this->load->view('templates/control_sidebar');
	        $this->load->view('templates/footer');
        }
        else
        {
            redirect('login');
		}
	}

	public function skills($id)
	{
		if($this->session->userdata('is_logued_in') === TRUE)
		{
			$employee = $this->employee->get_by_id($id);
			
			if(empty($employee))
			{
				show_404();
			}

			else
			{
				$this->session->set_userdata('employee_skills',$id);

				$data = array(
		    		'controller' => 'employees',
		    		'employee' => $employee,
		    		'skills' => $this->skill->get_all(),
		    		'grades' => $this->grade->get_all()
		    	);

				$this->load->view('templates/links');
		        $this->load->view('templates/navbar');
		        $this->load->view('templates/sidebar');
				$this->load->view('employees/skills',$data);
				$this->load->view('templates/control_sidebar');
		        $this->load->view('templates/footer');
			}
			
        }
        else
        {
            redirect('login');
		}
	}

	public function works($id)
	{
		if($this->session->userdata('is_logued_in') === TRUE && $this->session->userdata('employee_rol') != 4)
		{
			$employee = $this->employee->get_by_id($id);
			
			if(empty($employee))
			{
				show_404();
			}

			else
			{
				$this->session->set_userdata('employee_works',$id);

				$data = array(
		    		'controller' => 'employees',
		    		'employee' => $employee,
		    		'countries' => $this->country->get_all(),
		    		'positions' => $this->position->get_all()
		    	);

				$this->load->view('templates/links');
		        $this->load->view('templates/navbar');
		        $this->load->view('templates/sidebar');
				$this->load->view('employees/works',$data);
				$this->load->view('templates/control_sidebar');
		        $this->load->view('templates/footer');
			}
			
        }
        else
        {
            redirect('login');
		}
	}

	public function allergies($id)
	{
		if($this->session->userdata('is_logued_in') === TRUE && $this->session->userdata('employee_rol') != 4)
		{
			$employee = $this->employee->get_by_id($id);
			
			if(empty($employee))
			{
				show_404();
			}

			else
			{
				$this->session->set_userdata('employee_allergies',$id);

				$data = array(
		    		'controller' => 'employees',
		    		'employee' => $employee,
		    		'allergies' => $this->allergie->get_all()
		    	);

				$this->load->view('templates/links');
		        $this->load->view('templates/navbar');
		        $this->load->view('templates/sidebar');
				$this->load->view('employees/allergies',$data);
				$this->load->view('templates/control_sidebar');
		        $this->load->view('templates/footer');
			}
			
        }
        else
        {
            redirect('login');
		}
	}

	public function diseases($id)
	{
		if($this->session->userdata('is_logued_in') === TRUE && $this->session->userdata('employee_rol') != 4)
		{
			$employee = $this->employee->get_by_id($id);
			
			if(empty($employee))
			{
				show_404();
			}

			else
			{
				$this->session->set_userdata('employee_diseases',$id);

				$data = array(
		    		'controller' => 'employees',
		    		'employee' => $employee,
		    		'diseases' => $this->disease->get_all()
		    	);

				$this->load->view('templates/links');
		        $this->load->view('templates/navbar');
		        $this->load->view('templates/sidebar');
				$this->load->view('employees/diseases',$data);
				$this->load->view('templates/control_sidebar');
		        $this->load->view('templates/footer');
			}
			
        }
        else
        {
            redirect('login');
		}
	}

	public function emails($id)
	{
		if($this->session->userdata('is_logued_in') === TRUE && $this->session->userdata('employee_rol') != 4)
		{
			$employee = $this->employee->get_by_id($id);
			
			if(empty($employee))
			{
				show_404();
			}

			else
			{
				$this->session->set_userdata('employee_emails',$id);

				$data = array(
		    		'controller' => 'employees',	
		    		'employee' => $employee,
		    		'grades' => $this->grade->get_all()
		    	);

				$this->load->view('templates/links');
		        $this->load->view('templates/navbar');
		        $this->load->view('templates/sidebar');
				$this->load->view('employees/emails',$data);
				$this->load->view('templates/control_sidebar');
		        $this->load->view('templates/footer');
			}
			
        }
        else
        {
            redirect('login');
		}
	}

	public function list_active_employees()
	{
		$list = $this->employee->get_employees_datatables_actives();
		$data = array();
		$no = $_POST['start'];
		$i = 1;
		foreach ($list as $employee_data)
		{
			$no++;
			$row = array();
			$row[] = $i;
			$row[] = $employee_data->employee_id;
			$row[] = $employee_data->employee_firstname;
			$row[] = $employee_data->employee_firstlastname;
			$row[] = $employee_data->rol_name;
			$row[] = $employee_data->employee_status;
			if($this->session->userdata('employee_rol') == 1 || $this->session->userdata('employee_rol') == 2)
			{ 
				$row[] =
					'<a class="btn btn-link" href="profile/view/'.$employee_data->employee_id.'" title="Read">
						<i class="fa fa-eye"></i>
					</a>
					<a class="btn btn-link" href="javascript:void(0)" title="Update" onclick="get_employee('."'".$employee_data->employee_id."'".')" data-toggle="modal" data-target="#employee_modal">
						<i class="fa fa-pencil"></i>
					</a>
					<a class="btn btn-link" href="employee/skills/'.$employee_data->employee_id.'" title="Skills">
						<i class="fa fa-book"></i>
					</a>
					<a class="btn btn-link" href="employee/works/'.$employee_data->employee_id.'" title="Works">
						<i class="fa fa-suitcase"></i>
					</a>
					<a class="btn btn-link" href="employee/diseases/'.$employee_data->employee_id.'" title="Diseases">
						<i class="fa fa-ambulance"></i>
					</a>
					<a class="btn btn-link" href="employee/allergies/'.$employee_data->employee_id.'" title="Allergies">
						<i class="fa fa-heartbeat"></i>
					</a>
					<a class="btn btn-link" href="employee/emails/'.$employee_data->employee_id.'" title="Emails">
						<i class="fa fa-inbox"></i>
					</a>
					<a class="btn btn-link" href="javascript:void(0)" title="Deactivate" onclick="deactivate_employee('."'".$employee_data->employee_id."'".')">
						<i class="fa fa-ban"></i>
					</a>';
			}
			else
			{
				$row[] =
					'<a class="btn btn-link" href="profile/view/'.$employee_data->employee_id.'" title="Read">
						<i class="fa fa-eye"></i>
					</a>
					<a class="btn btn-link" href="employee/skills/'.$employee_data->employee_id.'" title="Skills">
						<i class="fa fa-book"></i>
					</a>
					<a class="btn btn-link" href="employee/works/'.$employee_data->employee_id.'" title="Works">
						<i class="fa fa-suitcase"></i>
					</a>
					<a class="btn btn-link" href="employee/diseases/'.$employee_data->employee_id.'" title="Diseases">
						<i class="fa fa-ambulance"></i>
					</a>
					<a class="btn btn-link" href="employee/allergies/'.$employee_data->employee_id.'" title="Allergies">
						<i class="fa fa-heartbeat"></i>
					</a>
					<a class="btn btn-link" href="employee/emails/'.$employee_data->employee_id.'" title="Emails">
						<i class="fa fa-inbox"></i>
					</a>';
			}
			$data[] = $row;
			$i++;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->employee->count_employees_all(),
			"recordsFiltered" => $this->employee->count_employees_filtered_actives(),
			"data" => $data
		);
		echo json_encode($output);
	}

	public function list_inactive_employees()
	{
		$list = $this->employee->get_employees_datatables_inactives();
		$data = array();
		$no = $_POST['start'];
		$i = 1;
		foreach ($list as $employee_data)
		{
			$no++;
			$row = array();
			$row[] = $i;
			$row[] = $employee_data->employee_id;
			$row[] = $employee_data->employee_firstname;
			$row[] = $employee_data->employee_firstlastname;
			$row[] = $employee_data->rol_name;
			$row[] = $employee_data->employee_status;
			if($this->session->userdata('employee_rol') == 1 || $this->session->userdata('employee_rol') == 2)
			{
				$row[] =
					'<a class="btn btn-link" href="javascript:void(0)" title="Activate" onclick="activate_employee('."'".$employee_data->employee_id."'".')">
						<i class="fa fa-check"></i>
					</a>';
			}
			else
			{
				$row[] =
					'No actions';
			}
			$data[] = $row;
			$i++;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->employee->count_employees_all(),
			"recordsFiltered" => $this->employee->count_employees_filtered_inactives(),
			"data" => $data
		);
		echo json_encode($output);
	}

	public function list_active_skills()
	{
		$list = $this->employee->get_skills_datatables_actives($this->session->userdata('employee_skills'));
		$data = array();
		$no = $_POST['start'];
		$i = 1;
		foreach ($list as $employee_skills_data)
		{
			$no++;
			$row = array();
			$row[] = $i;
			$row[] = $employee_skills_data->skill_name;
			$row[] = $employee_skills_data->empski_level . '%';
			$row[] = $employee_skills_data->grade_name;
			$row[] = $employee_skills_data->empski_time . ' months';
			$row[] = $employee_skills_data->empski_status;
			if($this->session->userdata('employee_rol') == 1 || $this->session->userdata('employee_rol') == 2)
			{
				$row[] =
					'
					<a class="btn btn-link" href="javascript:void(0)" title="Update" onclick="get_empski('."'".$employee_skills_data->empski_id."'".')" data-toggle="modal" data-target="#empski_modal">
						<i class="fa fa-pencil"></i>
					</a>
					<a class="btn btn-link" href="javascript:void(0)" title="Deactivate" onclick="deactivate_empski('."'".$employee_skills_data->empski_id."'".')">
						<i class="fa fa-ban"></i>
					</a>';
			}
			else
			{
				$row[] =
					'No actions';
			}
			$data[] = $row;
			$i++;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->employee->count_skills_all($this->session->userdata('employee_skills')),
			"recordsFiltered" => $this->employee->count_skills_filtered_actives($this->session->userdata('employee_skills')),
			"data" => $data
		);
		echo json_encode($output);
	}

	public function list_inactive_skills()
	{
		$list = $this->employee->get_skills_datatables_inactives($this->session->userdata('employee_skills'));
		$data = array();
		$no = $_POST['start'];
		$i = 1;
		foreach ($list as $employee_skills_data)
		{
			$no++;
			$row = array();
			$row[] = $i;
			$row[] = $employee_skills_data->skill_name;
			$row[] = $employee_skills_data->empski_level . '%';
			$row[] = $employee_skills_data->grade_name;
			$row[] = $employee_skills_data->empski_time . ' months';
			$row[] = $employee_skills_data->empski_status;
			if($this->session->userdata('employee_rol') == 1 || $this->session->userdata('employee_rol') == 2)
			{
				$row[] =
					'<a class="btn btn-link" href="javascript:void(0)" title="Activate" onclick="activate_empski('."'".$employee_skills_data->empski_id."'".')">
						<i class="fa fa-check"></i>
					</a>';
			}
			else
			{
				$row[] = 'No actions';
			}
			$data[] = $row;
			$i++;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->employee->count_skills_all($this->session->userdata('employee_skills')),
			"recordsFiltered" => $this->employee->count_skills_filtered_inactives($this->session->userdata('employee_skills')),
			"data" => $data
		);
		echo json_encode($output);
	}

	public function list_active_works()
	{
		$list = $this->employee->get_works_datatables_actives($this->session->userdata('employee_works'));
		$data = array();
		$no = $_POST['start'];
		$i = 1;
		foreach ($list as $employee_works_data)
		{
			$no++;
			$row = array();
			$row[] = $i;
			$row[] = $employee_works_data->empwork_company;
			$row[] = $employee_works_data->country_name;
			$row[] = $employee_works_data->position_name;
			$row[] = $employee_works_data->empwork_time . ' months';
			$row[] = $employee_works_data->empwork_status;
			if($this->session->userdata('employee_rol') == 1 || $this->session->userdata('employee_rol') == 2)
			{
				$row[] =
					'<a class="btn btn-link" href="javascript:void(0)" title="Update" onclick="get_empwork('."'".$employee_works_data->empwork_id."'".')" data-toggle="modal" data-target="#empwork_modal">
						<i class="fa fa-pencil"></i>
					</a>
					<a class="btn btn-link" href="javascript:void(0)" title="Deactivate" onclick="deactivate_empwork('."'".$employee_works_data->empwork_id."'".')">
						<i class="fa fa-ban"></i>
					</a>';
			}
			else
			{
				$row[] = 'No actions';
			}
			$data[] = $row;
			$i++;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->employee->count_works_all($this->session->userdata('employee_works')),
			"recordsFiltered" => $this->employee->count_works_filtered_actives($this->session->userdata('employee_works')),
			"data" => $data
		);
		echo json_encode($output);
	}

	public function list_inactive_works()
	{
		$list = $this->employee->get_works_datatables_inactives($this->session->userdata('employee_works'));
		$data = array();
		$no = $_POST['start'];
		$i = 1;
		foreach ($list as $employee_works_data)
		{
			$no++;
			$row = array();
			$row[] = $i;
			$row[] = $employee_works_data->empwork_company;
			$row[] = $employee_works_data->country_name;
			$row[] = $employee_works_data->position_name;
			$row[] = $employee_works_data->empwork_time . ' months';
			$row[] = $employee_works_data->empwork_status;
			if($this->session->userdata('employee_rol') == 1 || $this->session->userdata('employee_rol') == 2)
			{
				$row[] =
					'<a class="btn btn-link" href="javascript:void(0)" title="Activate" onclick="activate_empwork('."'".$employee_works_data->empwork_id."'".')">
						<i class="fa fa-check"></i>
					</a>';
			}
			else
			{
				$row[] = 'No actions';
			}
			$data[] = $row;
			$i++;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->employee->count_works_all($this->session->userdata('employee_works')),
			"recordsFiltered" => $this->employee->count_works_filtered_inactives($this->session->userdata('employee_works')),
			"data" => $data
		);
		echo json_encode($output);
	}

	public function list_active_allergies()
	{
		$list = $this->employee->get_allergies_datatables_actives($this->session->userdata('employee_allergies'));
		$data = array();
		$no = $_POST['start'];
		$i = 1;
		foreach ($list as $employee_allergies_data)
		{
			$no++;
			$row = array();
			$row[] = $i;
			$row[] = $employee_allergies_data->allergie_name;
			$row[] = $employee_allergies_data->empalle_status;
			if($this->session->userdata('employee_rol') == 1 || $this->session->userdata('employee_rol') == 2)
			{
				$row[] =
					'<a class="btn btn-link" href="javascript:void(0)" title="Update" onclick="get_empalle('."'".$employee_allergies_data->empalle_id."'".')" data-toggle="modal" data-target="#empalle_modal">
						<i class="fa fa-pencil"></i>
					</a>
					<a class="btn btn-link" href="javascript:void(0)" title="Deactivate" onclick="deactivate_empalle('."'".$employee_allergies_data->empalle_id."'".')">
						<i class="fa fa-ban"></i>
					</a>';
			}
			else
			{
				$row[] = 'No actions';
			}
			$data[] = $row;
			$i++;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->employee->count_allergies_all($this->session->userdata('employee_allergies')),
			"recordsFiltered" => $this->employee->count_allergies_filtered_actives($this->session->userdata('employee_allergies')),
			"data" => $data
		);
		echo json_encode($output);
	}

	public function list_inactive_allergies()
	{
		$list = $this->employee->get_allergies_datatables_inactives($this->session->userdata('employee_allergies'));
		$data = array();
		$no = $_POST['start'];
		$i = 1;
		foreach ($list as $employee_allergies_data)
		{
			$no++;
			$row = array();
			$row[] = $i;
			$row[] = $employee_allergies_data->allergie_name;
			$row[] = $employee_allergies_data->empalle_status;
			if($this->session->userdata('employee_rol') == 1 || $this->session->userdata('employee_rol') == 2)
			{
				$row[] =
					'<a class="btn btn-link" href="javascript:void(0)" title="Activate" onclick="activate_empalle('."'".$employee_allergies_data->empalle_id."'".')">
						<i class="fa fa-check"></i>
					</a>';
			}
			else
			{
				$row[] = 'No actions';
			}
			$data[] = $row;
			$i++;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->employee->count_allergies_all($this->session->userdata('employee_allergies')),
			"recordsFiltered" => $this->employee->count_allergies_filtered_inactives($this->session->userdata('employee_allergies')),
			"data" => $data
		);
		echo json_encode($output);
	}

	public function list_active_diseases()
	{
		$list = $this->employee->get_diseases_datatables_actives($this->session->userdata('employee_diseases'));
		$data = array();
		$no = $_POST['start'];
		$i = 1;
		foreach ($list as $employee_diseases_data)
		{
			$no++;
			$row = array();
			$row[] = $i;
			$row[] = $employee_diseases_data->disease_name;
			$row[] = $employee_diseases_data->empdis_date;
			$row[] = $employee_diseases_data->empdis_control;
			$row[] = $employee_diseases_data->empdis_status;
			if($this->session->userdata('employee_rol') == 1 || $this->session->userdata('employee_rol') == 2)
			{
				$row[] =
					'<a class="btn btn-link" href="javascript:void(0)" title="Update" onclick="get_empdis('."'".$employee_diseases_data->empdis_id."'".')" data-toggle="modal" data-target="#empdis_modal">
						<i class="fa fa-pencil"></i>
					</a>
					<a class="btn btn-link" href="javascript:void(0)" title="Deactivate" onclick="deactivate_empdis('."'".$employee_diseases_data->empdis_id."'".')">
						<i class="fa fa-ban"></i>
					</a>';
			}
			else
			{
				$row[] = 'No actions';
			}
			$data[] = $row;
			$i++;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->employee->count_diseases_all($this->session->userdata('employee_diseases')),
			"recordsFiltered" => $this->employee->count_diseases_filtered_actives($this->session->userdata('employee_diseases')),
			"data" => $data
		);
		echo json_encode($output);
	}

	public function list_inactive_diseases()
	{
		$list = $this->employee->get_diseases_datatables_inactives($this->session->userdata('employee_diseases'));
		$data = array();
		$no = $_POST['start'];
		$i = 1;
		foreach ($list as $employee_diseases_data)
		{
			$no++;
			$row = array();
			$row[] = $i;
			$row[] = $employee_diseases_data->disease_name;
			$row[] = $employee_diseases_data->empdis_date;
			$row[] = $employee_diseases_data->empdis_control;
			$row[] = $employee_diseases_data->empdis_status;
			if($this->session->userdata('employee_rol') == 1 || $this->session->userdata('employee_rol') == 2)
			{
				$row[] =
					'<a class="btn btn-link" href="javascript:void(0)" title="Activate" onclick="activate_empdis('."'".$employee_diseases_data->empdis_id."'".')">
						<i class="fa fa-check"></i>
					</a>';
			}
			else
			{
				$row[] = 'No actions';
			}
			$data[] = $row;
			$i++;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->employee->count_diseases_all($this->session->userdata('employee_diseases')),
			"recordsFiltered" => $this->employee->count_diseases_filtered_inactives($this->session->userdata('employee_diseases')),
			"data" => $data
		);
		echo json_encode($output);
	}

	public function list_active_emails()
	{
		$list = $this->employee->get_emails_datatables_actives($this->session->userdata('employee_emails'));
		$data = array();
		$no = $_POST['start'];
		$i = 1;
		foreach ($list as $employee_emails_data)
		{
			$no++;
			$row = array();
			$row[] = $i;
			$row[] = $employee_emails_data->empema_email;
			$row[] = $employee_emails_data->empema_status;
			if($this->session->userdata('employee_rol') == 1 || $this->session->userdata('employee_rol') == 2)
			{
				$row[] =
					'<a class="btn btn-link" href="javascript:void(0)" title="Update" onclick="get_empema('."'".$employee_emails_data->empema_id."'".')" data-toggle="modal" data-target="#empema_modal">
						<i class="fa fa-pencil"></i>
					</a>
					<a class="btn btn-link" href="javascript:void(0)" title="Deactivate" onclick="deactivate_empema('."'".$employee_emails_data->empema_id."'".')">
						<i class="fa fa-ban"></i>
					</a>';
			}
			else
			{
				$row[] = 'No actions';
			}
			$data[] = $row;
			$i++;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->employee->count_emails_all($this->session->userdata('employee_emails')),
			"recordsFiltered" => $this->employee->count_emails_filtered_actives($this->session->userdata('employee_emails')),
			"data" => $data
		);
		echo json_encode($output);
	}

	public function list_inactive_emails()
	{
		$list = $this->employee->get_emails_datatables_inactives($this->session->userdata('employee_emails'));
		$data = array();
		$no = $_POST['start'];
		$i = 1;
		foreach ($list as $employee_emails_data)
		{
			$no++;
			$row = array();
			$row[] = $i;
			$row[] = $employee_emails_data->empema_email;
			$row[] = $employee_emails_data->empema_status;
			if($this->session->userdata('employee_rol') == 1 || $this->session->userdata('employee_rol') == 2)
			{
				$row[] =
					'<a class="btn btn-link" href="javascript:void(0)" title="Activate" onclick="activate_empema('."'".$employee_emails_data->empema_id."'".')">
						<i class="fa fa-check"></i>
					</a>';
			}
			else
			{
				$row[] = 'No actions';
			}
			$data[] = $row;
			$i++;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->employee->count_emails_all($this->session->userdata('employee_emails')),
			"recordsFiltered" => $this->employee->count_emails_filtered_inactives($this->session->userdata('employee_emails')),
			"data" => $data
		);
		echo json_encode($output);
	}

	public function validate_id()
	{
		$id = $this->input->get('id');

		if ($id!='')
		{
		    $validation = $this->employee->validate_by_id($id);
		    if ($validation > 0)
			{
				$data = array(
				'type' => 'Warning',
				'message' => 'ID already exists.',
				'button' => 1
				);
			}
			else if ($validation < 1)
			{
				$data = array(
				'type' => 'Notice',
				'message' => 'ID is available.',
				'button' => 0
				);
			}
	  	}
	  	else if($id=='')
	  	{
		   	$data = array(
				'type' => 'Error',
				'message' => 'ID es required.',
				'button' => 1
			);
	 	}
	   	echo json_encode($data);
	}

	public function validate_actual_password()
	{
		$id = $this->session->userdata('employee_id');
		$actual_password = $this->input->get('actual_password');

		if ($actual_password!='')
		{
		    $validation = $this->employee->validate_actual_password($id,$actual_password);
		    if ($validation > 0)
			{
				$data = array(
				'type' => 'Warning',
				'message' => 'Actual password is correct.',
				'button' => 0
				);
			}
			else if ($validation < 1)
			{
				$data = array(
				'type' => 'Notice',
				'message' => 'Actual password is wrong.',
				'button' => 1
				);
			}
	  	}
	  	else if($actual_password=='')
	  	{
		   	$data = array(
				'type' => 'Error',
				'message' => 'Actual password es required.',
				'button' => 0
			);
	 	}
	   	echo json_encode($data);
	}

	public function compare_password()
	{
		$password = $this->input->get('password');
		$repeat_password = $this->input->get('repeat_password');

		if ($password!='' && $repeat_password!='')
		{
		    if ($password == $repeat_password)
			{
				$data = array(
				'type' => 'Notice',
				'message' => 'The passwords match.',
				'button' => 0
				);
			}
			else
			{
				$data = array(
				'type' => 'Warning',
				'message' => "The passwords doesn't match.",
				'button' => 1
				);
			}
	  	}
	  	else
	  	{
	  		$data = array(
				'button' => 0
			);
	  	}
	   	echo json_encode($data);
	}

	public function validate_username()
	{
		$username = $this->input->get('username');

		if ($username!='')
		{
		    $validation = $this->employee->validate_by_username($username);
		    if ($validation > 0)
			{
				$data = array(
				'type' => 'Warning',
				'message' => 'Username already exists.',
				'button' => 1
				);
			}
			else if ($validation < 1)
			{
				$data = array(
				'type' => 'Notice',
				'message' => 'Username is available.',
				'button' => 0
				);
			}
	  	}
	  	else if($id=='')
	  	{
		   	$data = array(
				'type' => 'Error',
				'message' => 'Username es required.',
				'button' => 1
			);
	 	}
	   	echo json_encode($data);
	}

	public function get_regions($id_country)
	{
		$data = $this->region->get_by_country($id_country);
		echo json_encode($data);
	}

	public function get_cities($id_region)
	{
		$data = $this->city->get_by_region($id_region);
		echo json_encode($data);
	}

	public function get_employee($id)
	{
		$data = $this->employee->get_by_id($id);
		echo json_encode($data);
	}

	public function create_employee($save_method = NULL)
	{
		$this->_validate();
		
		$data = array(
			'employee_id' => $this->input->post('id'),
			'employee_firstname' => $this->input->post('firstname'),
			'employee_secondname' => $this->input->post('secondname'),
			'employee_firstlastname' => $this->input->post('firstlastname'),
			'employee_secondlastname' => $this->input->post('secondlastname'),
			'employee_birth' => $this->input->post('birth'),
			'employee_country' => $this->input->post('country'),
			'employee_region' => $this->input->post('region'),
			'employee_city' => $this->input->post('city'),
			'employee_address' => $this->input->post('address'),
			'employee_neighborhood' => $this->input->post('neighborhood'),
			'employee_cellphone' => $this->input->post('cellphone'),
			'employee_phone' => $this->input->post('phone'),
			'employee_whatsapp' => $this->input->post('whatsapp'),
			'employee_facebook' => $this->input->post('facebook'),
			'employee_gplus' => $this->input->post('gplus'),
			'employee_linkedin' => $this->input->post('linkedin'),
			'employee_file' => $this->input->post('file'),
			'employee_rol' => $this->input->post('rol'),
			'employee_area' => $this->input->post('area'),
			'employee_position' => $this->input->post('position'),
			'employee_boss' => $this->input->post('boss'),
			'employee_salary' => $this->input->post('salary'),
			'employee_username' => $this->input->post('username'),
			'employee_password' => $this->input->post('password'),
			'employee_secques' => $this->input->post('secques'),
			'employee_secans' => $this->input->post('secans'),
			'employee_status' => 'Active'
		);

		$this->employee->save($data);
		echo json_encode(array("status" => TRUE));
	}

	public function update_employee($save_method = NULL)
	{
		$this->_validate($save_method);
		if(empty($save_method))
		{
			if($this->session->userdata('employee_rol') == 1)
			{
				$data = array(
					'employee_id' => $this->input->post('id'),
					'employee_firstname' => $this->input->post('firstname'),
					'employee_secondname' => $this->input->post('secondname'),
					'employee_firstlastname' => $this->input->post('firstlastname'),
					'employee_secondlastname' => $this->input->post('secondlastname'),
					'employee_birth' => $this->input->post('birth'),
					'employee_country' => $this->input->post('country'),
					'employee_region' => $this->input->post('region'),
					'employee_city' => $this->input->post('city'),
					'employee_address' => $this->input->post('address'),
					'employee_neighborhood' => $this->input->post('neighborhood'),
					'employee_cellphone' => $this->input->post('cellphone'),
					'employee_phone' => $this->input->post('phone'),
					'employee_whatsapp' => $this->input->post('whatsapp'),
					'employee_facebook' => $this->input->post('facebook'),
					'employee_gplus' => $this->input->post('gplus'),
					'employee_linkedin' => $this->input->post('linkedin'),
					'employee_file' => $this->input->post('file'),
					'employee_rol' => $this->input->post('rol'),
					'employee_area' => $this->input->post('area'),
					'employee_position' => $this->input->post('position'),
					'employee_boss' => $this->input->post('boss'),
					'employee_salary' => $this->input->post('salary'),
					'employee_username' => $this->input->post('username'),
					'employee_password' => $this->input->post('password'),
					'employee_secques' => $this->input->post('secques'),
					'employee_secans' => $this->input->post('secans'),
					'employee_status' => 'Active'
				);
			}
			else
			{
				$data = array(
					'employee_id' => $this->input->post('id'),
					'employee_firstname' => $this->input->post('firstname'),
					'employee_secondname' => $this->input->post('secondname'),
					'employee_firstlastname' => $this->input->post('firstlastname'),
					'employee_secondlastname' => $this->input->post('secondlastname'),
					'employee_birth' => $this->input->post('birth'),
					'employee_country' => $this->input->post('country'),
					'employee_region' => $this->input->post('region'),
					'employee_city' => $this->input->post('city'),
					'employee_address' => $this->input->post('address'),
					'employee_neighborhood' => $this->input->post('neighborhood'),
					'employee_cellphone' => $this->input->post('cellphone'),
					'employee_phone' => $this->input->post('phone'),
					'employee_whatsapp' => $this->input->post('whatsapp'),
					'employee_facebook' => $this->input->post('facebook'),
					'employee_gplus' => $this->input->post('gplus'),
					'employee_linkedin' => $this->input->post('linkedin'),
					'employee_file' => $this->input->post('file'),
					'employee_area' => $this->input->post('area'),
					'employee_position' => $this->input->post('position'),
					'employee_boss' => $this->input->post('boss'),
					'employee_salary' => $this->input->post('salary'),
					'employee_status' => 'Active'
				);
			}
		}
		else
		{
			if($save_method == 'profile_info')
			{
				$data = array(
					'employee_firstname' => $this->input->post('firstname'),
					'employee_secondname' => $this->input->post('secondname'),
					'employee_firstlastname' => $this->input->post('firstlastname'),
					'employee_secondlastname' => $this->input->post('secondlastname'),
					'employee_birth' => $this->input->post('birth'),
					'employee_country' => $this->input->post('country'),
					'employee_region' => $this->input->post('region'),
					'employee_city' => $this->input->post('city'),
					'employee_address' => $this->input->post('address'),
					'employee_neighborhood' => $this->input->post('neighborhood'),
					'employee_cellphone' => $this->input->post('cellphone'),
					'employee_phone' => $this->input->post('phone'),
					'employee_whatsapp' => $this->input->post('whatsapp'),
					'employee_facebook' => $this->input->post('facebook'),
					'employee_gplus' => $this->input->post('gplus'),
					'employee_linkedin' => $this->input->post('linkedin'),
					'employee_status' => 'Active'
				);
			}
			else if($save_method == 'profile_security')
			{
				$data = array(
					'employee_password' => $this->input->post('password'),
					'employee_secques' => $this->input->post('secques'),
					'employee_secans' => $this->input->post('secans'),
					'employee_status' => 'Active'
				);
			}
		}

		$this->employee->update(array('employee_id' => $this->input->post('id')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function activate_employee($id)
	{
		$this->employee->activate($id);
		echo json_encode(array("status" => TRUE));
	}

	public function deactivate_employee($id)
	{
		$this->employee->deactivate($id);
		echo json_encode(array("status" => TRUE));
	}

	public function get_empski($id = NULL)
	{
		$data = $this->employee->get_empski($id);
		echo json_encode($data);
	}

	public function create_empski($save_method = NULL)
	{
		$this->_validate('empski');

		$data = array(
			'empski_employee' => $this->session->userdata('employee_skills'),
			'empski_skill' => $this->input->post('skill'),
			'empski_level' => $this->input->post('level'),
			'empski_grade' => $this->input->post('grade'),
			'empski_time' => $this->input->post('time'),
			'empski_status' => 'Active'
		);

		$this->employee->save_empski($data);
		echo json_encode(array("status" => TRUE));
	}

	public function update_empski($save_method = NULL)
	{
		$this->_validate('empski');
		$data = array(
			'empski_skill' => $this->input->post('skill'),
			'empski_level' => $this->input->post('level'),
			'empski_grade' => $this->input->post('grade'),
			'empski_time' => $this->input->post('time'),
			'empski_status' => 'Active'
		);

		$this->employee->update_empski(array('empski_id' => $this->input->post('id')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function activate_empski($id)
	{
		$this->employee->activate_empski($id);
		echo json_encode(array("status" => TRUE));
	}

	public function deactivate_empski($id)
	{
		$this->employee->deactivate_empski($id);
		echo json_encode(array("status" => TRUE));
	}

	public function get_empwork($id = NULL)
	{
		$data = $this->employee->get_empwork($id);
		echo json_encode($data);
	}

	public function create_empwork($save_method = NULL)
	{
		$this->_validate('empwork');

		$data = array(
			'empwork_employee' => $this->session->userdata('employee_works'),
			'empwork_company' => $this->input->post('company'),
			'empwork_country' => $this->input->post('country'),
			'empwork_position' => $this->input->post('position'),
			'empwork_time' => $this->input->post('time'),
			'empwork_status' => 'Active'
		);

		$this->employee->save_empwork($data);
		echo json_encode(array("status" => TRUE));
	}

	public function update_empwork($save_method = NULL)
	{
		$this->_validate('empwork');
		$data = array(
			'empwork_company' => $this->input->post('company'),
			'empwork_country' => $this->input->post('country'),
			'empwork_position' => $this->input->post('position'),
			'empwork_time' => $this->input->post('time'),
			'empwork_status' => 'Active'
		);

		$this->employee->update_empwork(array('empwork_id' => $this->input->post('id')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function activate_empwork($id)
	{
		$this->employee->activate_empwork($id);
		echo json_encode(array("status" => TRUE));
	}

	public function deactivate_empwork($id)
	{
		$this->employee->deactivate_empwork($id);
		echo json_encode(array("status" => TRUE));
	}

	public function get_empalle($id = NULL)
	{
		$data = $this->employee->get_empalle($id);
		echo json_encode($data);
	}

	public function create_empalle($save_method = NULL)
	{
		$this->_validate('empalle');

		$data = array(
			'empalle_employee' => $this->session->userdata('employee_allergies'),
			'empalle_allergie' => $this->input->post('allergie'),
			'empalle_status' => 'Active'
		);

		$this->employee->save_empalle($data);
		echo json_encode(array("status" => TRUE));
	}

	public function update_empalle($save_method = NULL)
	{
		$this->_validate('empalle');
		$data = array(
			'empalle_allergie' => $this->input->post('allergie'),
			'empalle_status' => 'Active'
		);

		$this->employee->update_empalle(array('empalle_id' => $this->input->post('id')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function activate_empalle($id)
	{
		$this->employee->activate_empalle($id);
		echo json_encode(array("status" => TRUE));
	}

	public function deactivate_empalle($id)
	{
		$this->employee->deactivate_empalle($id);
		echo json_encode(array("status" => TRUE));
	}

	public function get_empdis($id = NULL)
	{
		$data = $this->employee->get_empdis($id);
		echo json_encode($data);
	}

	public function create_empdis($save_method = NULL)
	{
		$this->_validate('empdis');

		$data = array(
			'empdis_employee' => $this->session->userdata('employee_diseases'),
			'empdis_disease' => $this->input->post('disease'),
			'empdis_diagnostic' => $this->input->post('diagnostic'),
			'empdis_date' => $this->input->post('date'),
			'empdis_control' => $this->input->post('control'),
			'empdis_status' => 'Active'
		);

		$this->employee->save_empdis($data);
		echo json_encode(array("status" => TRUE));
	}

	public function update_empdis($save_method = NULL)
	{
		$this->_validate('empdis');
		$data = array(
			'empdis_employee' => $this->session->userdata('employee_diseases'),
			'empdis_disease' => $this->input->post('disease'),
			'empdis_diagnostic' => $this->input->post('diagnostic'),
			'empdis_date' => $this->input->post('date'),
			'empdis_control' => $this->input->post('control'),
			'empdis_status' => 'Active'
		);

		$this->employee->update_empdis(array('empdis_id' => $this->input->post('id')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function activate_empdis($id)
	{
		$this->employee->activate_empdis($id);
		echo json_encode(array("status" => TRUE));
	}

	public function deactivate_empdis($id)
	{
		$this->employee->deactivate_empdis($id);
		echo json_encode(array("status" => TRUE));
	}

	public function get_empema($id = NULL)
	{
		$data = $this->employee->get_empema($id);
		echo json_encode($data);
	}

	public function create_empema($save_method = NULL)
	{
		$this->_validate('empema');

		$data = array(
			'empema_employee' => $this->session->userdata('employee_emails'),
			'empema_email' => $this->input->post('email'),
			'empema_status' => 'Active'
		);

		$this->employee->save_empema($data);
		echo json_encode(array("status" => TRUE));
	}

	public function update_empema($save_method = NULL)
	{
		$this->_validate('empema');
		$data = array(
			'empema_email' => $this->input->post('email'),
			'empema_status' => 'Active'
		);

		$this->employee->update_empema(array('empema_id' => $this->input->post('id')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function activate_empema($id)
	{
		$this->employee->activate_empema($id);
		echo json_encode(array("status" => TRUE));
	}

	public function deactivate_empema($id)
	{
		$this->employee->deactivate_empema($id);
		echo json_encode(array("status" => TRUE));
	}

	private function _validate($save_method = NULL)
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($save_method == NULL)
		{
			if($this->input->post('id') == '')
			{
				$data['inputerror'][] = 'id';
				$data['error_string'][] = 'Id is required.';
				$data['status'] = FALSE;
			}
			if($this->input->post('firstname') == '')
			{
				$data['inputerror'][] = 'firstname';
				$data['error_string'][] = 'Firstname is required.';
				$data['status'] = FALSE;
			}
			if($this->input->post('firstlastname') == '')
			{
				$data['inputerror'][] = 'firstlastname';
				$data['error_string'][] = 'First lastname is required.';
				$data['status'] = FALSE;
			}
			if($this->session->userdata('employee_rol') == 1)
			{
				if($this->input->post('rol') == '')
				{
					$data['inputerror'][] = 'rol';
					$data['error_string'][] = 'Rol is required.';
					$data['status'] = FALSE;
				}	
			}
			if($this->input->post('area') == '')
			{
				$data['inputerror'][] = 'area';
				$data['error_string'][] = 'Area is required.';
				$data['status'] = FALSE;
			}	
			if($this->input->post('position') == '')
			{
				$data['inputerror'][] = 'position';
				$data['error_string'][] = 'Position is required.';
				$data['status'] = FALSE;
			}
			if($this->session->userdata('employee_rol') == 1)
			{
				if($this->input->post('username') == '')
				{
					$data['inputerror'][] = 'username';
					$data['error_string'][] = 'Username is required.';
					$data['status'] = FALSE;
				}
				if($this->input->post('password') == '')
				{
					$data['inputerror'][] = 'password';
					$data['error_string'][] = 'Password is required.';
					$data['status'] = FALSE;
				}
				if($this->input->post('repeat_password') == '')
				{
					$data['inputerror'][] = 'repeat_password';
					$data['error_string'][] = 'Repeat password is required.';
					$data['status'] = FALSE;
				}
				if($this->input->post('secques') == '0')
				{
					$data['inputerror'][] = 'secques';
					$data['error_string'][] = 'Secret question is required.';
					$data['status'] = FALSE;
				}
				if($this->input->post('secans') == '')
				{
					$data['inputerror'][] = 'secans';
					$data['error_string'][] = 'Secret answer is required.';
					$data['status'] = FALSE;
				}
			}
		}

		if($save_method == 'profile_info')
		{
			if($this->input->post('firstname') == '')
			{
				$data['inputerror'][] = 'firstname';
				$data['error_string'][] = 'Firstname is required.';
				$data['status'] = FALSE;
			}
			if($this->input->post('firstlastname') == '')
			{
				$data['inputerror'][] = 'firstlastname';
				$data['error_string'][] = 'First lastname is required.';
				$data['status'] = FALSE;
			}
		}
		else if($save_method == 'profile_security')
		{
			if($this->input->post('password') == '')
			{
				$data['inputerror'][] = 'password';
				$data['error_string'][] = 'Password is required.';
				$data['status'] = FALSE;
			}
			if($this->input->post('repeat_password') == '')
			{
				$data['inputerror'][] = 'repeat_password';
				$data['error_string'][] = 'Repeat password is required.';
				$data['status'] = FALSE;
			}
			if($this->input->post('secques') == '0')
			{
				$data['inputerror'][] = 'secques';
				$data['error_string'][] = 'Secret question is required.';
				$data['status'] = FALSE;
			}
			if($this->input->post('secans') == '')
			{
				$data['inputerror'][] = 'secans';
				$data['error_string'][] = 'Secret answer is required.';
				$data['status'] = FALSE;
			}
		}
		else if($save_method == 'empski')
		{
			if($this->input->post('skill') == '')
			{
				$data['inputerror'][] = 'skill';
				$data['error_string'][] = 'Skill is required.';
				$data['status'] = FALSE;
			}
			if($this->input->post('level') == '')
			{
				$data['inputerror'][] = 'level';
				$data['error_string'][] = 'Level is required.';
				$data['status'] = FALSE;
			}
			if($this->input->post('grade') == '')
			{
				$data['inputerror'][] = 'grade';
				$data['error_string'][] = 'Grade is required.';
				$data['status'] = FALSE;
			}
			if($this->input->post('time') == '')
			{
				$data['inputerror'][] = 'time';
				$data['error_string'][] = 'Time is required.';
				$data['status'] = FALSE;
			}
		}
		else if($save_method == 'empwork')
		{
			if($this->input->post('company') == '')
			{
				$data['inputerror'][] = 'company';
				$data['error_string'][] = 'Company is required.';
				$data['status'] = FALSE;
			}
			if($this->input->post('country') == '')
			{
				$data['inputerror'][] = 'country';
				$data['error_string'][] = 'Country is required.';
				$data['status'] = FALSE;
			}
			if($this->input->post('position') == '')
			{
				$data['inputerror'][] = 'position';
				$data['error_string'][] = 'Position is required.';
				$data['status'] = FALSE;
			}
			if($this->input->post('time') == '')
			{
				$data['inputerror'][] = 'time';
				$data['error_string'][] = 'Time is required.';
				$data['status'] = FALSE;
			}
		}
		else if($save_method == 'empalle')
		{
			if($this->input->post('allergie') == '')
			{
				$data['inputerror'][] = 'allergie';
				$data['error_string'][] = 'allergie is required.';
				$data['status'] = FALSE;
			}
		}
		else if($save_method == 'empdis')
		{
			if($this->input->post('disease') == '')
			{
				$data['inputerror'][] = 'disease';
				$data['error_string'][] = 'Disease is required.';
				$data['status'] = FALSE;
			}
			if($this->input->post('control') == '')
			{
				$data['inputerror'][] = 'control';
				$data['error_string'][] = 'Control is required.';
				$data['status'] = FALSE;
			}
		}
		else if($save_method == 'empema')
		{
			if($this->input->post('email') == '')
			{
				$data['inputerror'][] = 'email';
				$data['error_string'][] = 'Email is required.';
				$data['status'] = FALSE;
			}
		}
		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}
}