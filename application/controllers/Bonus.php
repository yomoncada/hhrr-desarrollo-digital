<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bonus extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('bonus_model','bonus');
		$this->load->model('employee_model','employee');
	}

	public function index()
	{
		if($this->session->userdata('is_logued_in') === TRUE && $this->session->userdata('employee_rol') == 1 || $this->session->userdata('employee_rol') == 2)
		{
			$data = array(
	    		'controller' => 'bonuses',
	    		'employees' => $this->employee->get_all()
	    	);

			$this->load->view('templates/links');
	        $this->load->view('templates/navbar');
	        $this->load->view('templates/sidebar');
			$this->load->view('bonuses/index',$data);
			$this->load->view('templates/control_sidebar');
	        $this->load->view('templates/footer');
        }
        else
        {
            show_404();
		}
	}

	public function list_pending_bonuses()
	{
		$list = $this->bonus->get_pending_datatables();
		$data = array();
		$no = $_POST['start'];
		$i = 1;
		foreach ($list as $bonus_data)
		{
			$no++;
			$row = array();
			$row[] = $i;
			$row[] = '<a href="profile/view/'.$bonus_data->bonus_employee.'");?>' . $bonus_data->employee_firstname . ' ' . $bonus_data->employee_firstlastname . '</a>';
			$row[] = $bonus_data->bonus_value . '$';
			$row[] = $bonus_data->bonus_date;
			$row[] = $bonus_data->bonus_status;
			$row[] =
				'<a class="btn btn-link" href="javascript:void(0)" title="Process" onclick="get_bonus('."'".$bonus_data->bonus_id."'".')" data-toggle="modal" data-target="#bonus_modal">
					<i class="fa fa-edit"></i>
				</a>';
			$data[] = $row;
			$i++;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->bonus->count_all(),
			"recordsFiltered" => $this->bonus->count_pending_filtered(),
			"data" => $data
		);
		echo json_encode($output);
	}

	public function list_processed_bonuses()
	{
		$list = $this->bonus->get_processed_datatables();
		$data = array();
		$no = $_POST['start'];
		$i = 1;
		foreach ($list as $bonus_data)
		{
			$no++;
			$row = array();
			$row[] = $i;
			$row[] = '<a href="profile/view/'.$bonus_data->bonus_employee.'");?>' . $bonus_data->employee_firstname . ' ' . $bonus_data->employee_firstlastname . '</a>';
			$row[] = $bonus_data->bonus_value . '$';
			$row[] = $bonus_data->bonus_date;
			$row[] = $bonus_data->bonus_status;
			$row[] =
				'<a class="btn btn-link" href="javascript:void(0)" title="Read" onclick="get_bonus_unmodifiable('."'".$bonus_data->bonus_id."'".')" data-toggle="modal" data-target="#bonus_modal">
					<i class="fa fa-eye"></i>
				</a>';
			$data[] = $row;
			$i++;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->bonus->count_all(),
			"recordsFiltered" => $this->bonus->count_processed_filtered(),
			"data" => $data
		);
		echo json_encode($output);
	}

	public function get_bonus($id = NULL)
	{
		$data = $this->bonus->get($id);
		echo json_encode($data);
	}

	public function create_bonus()
	{
		$save_method = "create";
		$this->_validate($save_method);

		$data = array(
			'bonus_employee' => $this->input->post('employee'),
			'bonus_value' => $this->input->post('value'),
			'bonus_date' => $this->input->post('date'),
			'bonus_status' => 'Pending'
		);

		$this->bonus->save($data);	
		echo json_encode(array("status" => TRUE));
	}

	public function update_bonus()
	{
		$save_method = "update";
		$this->_validate($save_method);
		
		$data = array(
			'bonus_employee' => $this->input->post('employee'),
			'bonus_value' => $this->input->post('value'),
			'bonus_date' => $this->input->post('date'),
			'bonus_status' => 'Processed'
		);

		$this->bonus->update(array('bonus_id' => $this->input->post('id')), $data);
		echo json_encode(array("status" => TRUE));
	}

	private function _validate($save_method)
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('employee') == "0")
		{
			$data['inputerror'][] = 'employee';
			$data['error_string'][] = 'Employee is required.';
			$data['status'] = FALSE;
		}
		if($this->input->post('value') == '')
		{
			$data['inputerror'][] = 'value';
			$data['error_string'][] = 'Value is required.';
			$data['status'] = FALSE;
		}
		if($this->input->post('date') == '')
		{
			$data['inputerror'][] = 'date';
			$data['error_string'][] = 'Date is required.';
			$data['status'] = FALSE;
		}
		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}
}