<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Country extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('country_model','country');
		//$this->load->model('position_model','position');
	}

	public function index()
	{
		if($this->session->userdata('is_logued_in') === TRUE && $this->session->userdata('employee_rol') == 1)
		{
			$data = array(
	    		'controller' => 'countries'
	    	);
	    	
			$this->load->view('templates/links');
	        $this->load->view('templates/navbar');
	        $this->load->view('templates/sidebar');
			$this->load->view('countries/index',$data);
			$this->load->view('templates/control_sidebar');
	        $this->load->view('templates/footer');
        }
        else
        {
            show_404();
		}
	}

	public function list_active_countries()
	{
		$list = $this->country->get_datatables_actives();
		$data = array();
		$no = $_POST['start'];
		$i = 1;
		foreach ($list as $country_data)
		{
			$no++;
			$row = array();
			$row[] = $i;
			$row[] = $country_data->country_name;
			$row[] = $country_data->country_status;
			$row[] =
				'<a class="btn btn-link" href="javascript:void(0)" title="Update" onclick="get_country('."'".$country_data->country_id."'".')" data-toggle="modal" data-target="#country_modal">
					<i class="fa fa-pencil"></i>
				</a>
				<a class="btn btn-link" href="javascript:void(0)" title="Deactivate" onclick="deactivate_country('."'".$country_data->country_id."'".')">
					<i class="fa fa-ban"></i>
				</a>';
			$data[] = $row;
			$i++;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->country->count_all(),
			"recordsFiltered" => $this->country->count_filtered_actives(),
			"data" => $data
		);
		echo json_encode($output);
	}

	public function list_inactive_countries()
	{
		$list = $this->country->get_datatables_inactives();
		$data = array();
		$no = $_POST['start'];
		$i = 1;
		foreach ($list as $country_data)
		{
			$no++;
			$row = array();
			$row[] = $i;
			$row[] = $country_data->country_name;
			$row[] = $country_data->country_status;
			$row[] =
				'<a class="btn btn-link" href="javascript:void(0)" title="Activate" onclick="activate_country('."'".$country_data->country_id."'".')">
					<i class="fa fa-check"></i>
				</a>';
			$data[] = $row;
			$i++;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->country->count_all(),
			"recordsFiltered" => $this->country->count_filtered_inactives(),
			"data" => $data
		);
		echo json_encode($output);
	}

	public function validate_name()
	{
		$name = $this->input->get('name');

		if ($name!='')
		{
		    $validation = $this->country->validate_by_name($name);
		    if ($validation > 0)
			{
				$data = array(
				'type' => 'Warning',
				'message' => 'Name already exists.',
				'button' => 1
				);
			}
			else if ($validation < 1)
			{
				$data = array(
				'type' => 'Notice',
				'message' => 'Name is available.',
				'button' => 0
				);
			}
	  	}
	  	else if($id=='')
	  	{
		   	$data = array(
				'type' => 'Error',
				'message' => 'Name is required.',
				'button' => 1
			);
	 	}
	   	echo json_encode($data);
	}

	public function get_country($id = NULL)
	{
		$data = $this->country->get($id);
		echo json_encode($data);
	}

	public function create_country()
	{
		$save_method = "create";
		$this->_validate($save_method);

		$data = array(
			'country_name' => $this->input->post('name'),
			'country_status' => 'Active'
		);

		$this->country->create($data);
		echo json_encode(array("status" => TRUE));
	}

	public function update_country()
	{
		$save_method = "update";
		$this->_validate($save_method);
		$data = array(
			'country_name' => $this->input->post('name'),
			'country_status' => 'Active'
		);

		$this->country->update(array('country_id' => $this->input->post('id')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function activate_country($id)
	{
		$this->country->activate($id);
		echo json_encode(array("status" => TRUE));
	}

	public function deactivate_country($id)
	{
		$this->country->deactivate($id);
		echo json_encode(array("status" => TRUE));
	}

	private function _validate($save_method)
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('name') == '')
		{
			$data['inputerror'][] = 'name';
			$data['error_string'][] = 'Name is required.';
			$data['status'] = FALSE;
		}
		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}
}