<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['absence'] = 'absence';
$route['allergie'] = 'allergie';
$route['area'] = 'area';
$route['ascent'] = 'ascent';
$route['bonus'] = 'bonus';
$route['boss'] = 'boss';
$route['city'] = 'city';
$route['country'] = 'country';
$route['disease'] = 'disease';
$route['increase'] = 'increase';
$route['employee'] = 'employee';
$route['employee/skills/(:any)'] = 'employee/skills/$1';
$route['employee/works/(:any)'] = 'employee/works/$1';
$route['employee/deseases/(:any)'] = 'employee/deseases/$1';
$route['employee/allergies/(:any)'] = 'employee/allergies/$1';
$route['employee/emails/(:any)'] = 'employee/emails/$1';
$route['grade'] = 'grade';
$route['home'] = 'system/home';
$route['login'] = 'system/login';
$route['logout'] = 'system/logout';
$route['position'] = 'position';
$route['profile'] = 'profile';
$route['profile/view/(:any)'] = 'profile/view/$1';
$route['region'] = 'region';
$route['rol'] = 'rol';
$route['skill'] = 'skill';


$route['default_controller'] = 'index';
$route['(:any)'] = 'index/$1';

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
