<?php  
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profile_model extends CI_Model {
	
	var $absences_column_order = array('absence_id','absence_days','absence_type','absence_startdate','absence_enddate',null);
	var $absences_column_search = array('absence_id','absence_days','absence_type','absence_startdate','absence_enddate');
	var $absences_order = array('absence_startdate' => 'desc');
	var $ascents_column_order = array('asc.ascent_id','are.area_name','pos.position_name','asc.ascent_salary','asc.ascent_date',null);
	var $ascents_column_search = array('asc.ascent_id','are.area_name','pos.position_name','asc.ascent_salary','asc.ascent_date');
	var $ascents_order = array('asc.ascent_date' => 'asc');
	var $bonuses_column_order = array('bonus_id','bonus_value','bonus_date',null);
	var $bonuses_column_search = array('bonus_id','bonus_value','bonus_date');
	var $bonuses_order = array('bonus_date' => 'asc');
	var $increases_column_order = array('increase_id','increase_type','increase_value','increase_date',null);
	var $increases_column_search = array('increase_id','increase_type','increase_value','increase_date');
	var $increases_order = array('increase_date' => 'asc');

	public function __construct() {
		parent::__construct();
	}

	private function _get_absences_datatables_query($id)
	{
		$this->db->select('absence_id, absence_days, absence_type, absence_startdate, absence_enddate');
    	$this->db->from('absences');
		$this->db->where('absence_employee',$id);

		$i = 0;

		foreach ($this->absences_column_search as $item)
		{
			if($_POST['search']['value'])
			{
				
				if($i===0)
				{
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->absences_column_search) - 1 == $i)
					$this->db->group_end();
				}
				$i++;
			}

		if(isset($_POST['order']))
		{
			$this->db->order_by($this->absences_column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->absences_order))
		{
			$order = $this->absences_order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	private function _get_ascents_datatables_query($id)
	{
		$this->db->select('asc.ascent_id, are.area_name, pos.position_name, asc.ascent_salary, asc.ascent_date');
    	$this->db->from('ascents asc');
    	$this->db->join('areas are','asc.ascent_area = are.area_id');
	    $this->db->join('positions pos','asc.ascent_position = pos.position_id');
		$this->db->where('asc.ascent_employee',$id);

		$i = 0;

		foreach ($this->ascents_column_search as $item)
		{
			if($_POST['search']['value'])
			{
				
				if($i===0)
				{
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->ascents_column_search) - 1 == $i)
					$this->db->group_end();
				}
				$i++;
			}

		if(isset($_POST['order']))
		{
			$this->db->order_by($this->ascents_column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->ascents_order))
		{
			$order = $this->ascents_order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	private function _get_bonuses_datatables_query($id)
	{
		$this->db->select('bonus_id, bonus_value, bonus_date');
    	$this->db->from('bonuses');
		$this->db->where('bonus_employee',$id);

		$i = 0;

		foreach ($this->bonuses_column_search as $item)
		{
			if($_POST['search']['value'])
			{
				
				if($i===0)
				{
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->bonuses_column_search) - 1 == $i)
					$this->db->group_end();
				}
				$i++;
			}

		if(isset($_POST['order']))
		{
			$this->db->order_by($this->bonuses_column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->bonuses_order))
		{
			$order = $this->bonuses_order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	private function _get_increases_datatables_query($id)
	{
		$this->db->select('increase_id, increase_type, increase_value, increase_date');
    	$this->db->from('increases');
		$this->db->where('increase_employee',$id);

		$i = 0;

		foreach ($this->increases_column_search as $item)
		{
			if($_POST['search']['value'])
			{
				
				if($i===0)
				{
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->increases_column_search) - 1 == $i)
					$this->db->group_end();
				}
				$i++;
			}

		if(isset($_POST['order']))
		{
			$this->db->order_by($this->increases_column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->increases_order))
		{
			$order = $this->increases_order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_absences_datatables($id)
	{
		$this->_get_absences_datatables_query($id);
		if($_POST['length'] != -1)
			$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function get_ascents_datatables($id)
	{
		$this->_get_ascents_datatables_query($id);
		if($_POST['length'] != -1)
			$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function get_bonuses_datatables($id)
	{
		$this->_get_bonuses_datatables_query($id);
		if($_POST['length'] != -1)
			$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function get_increases_datatables($id)
	{
		$this->_get_increases_datatables_query($id);
		if($_POST['length'] != -1)
			$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_absences_filtered($id)
	{
		$this->_get_absences_datatables_query($id);
		$query = $this->db->get();
		return $query->num_rows();
	}

	function count_ascents_filtered($id)
	{
		$this->_get_ascents_datatables_query($id);
		$query = $this->db->get();
		return $query->num_rows();
	}

	function count_bonuses_filtered($id)
	{
		$this->_get_bonuses_datatables_query($id);
		$query = $this->db->get();
		return $query->num_rows();
	}

	function count_increases_filtered($id)
	{
		$this->_get_increases_datatables_query($id);
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all_absences($id)
	{
		$this->db->from('absences');
    	$this->db->where('absence_employee',$id);
		return $this->db->count_all_results();
	}

	public function count_all_ascents($id)
	{
		$this->db->from('ascents');
    	$this->db->where('ascent_employee',$id);
		return $this->db->count_all_results();
	}

	public function count_all_bonuses($id)
	{
		$this->db->from('bonuses');
    	$this->db->where('bonus_employee',$id);
		return $this->db->count_all_results();
	}

	public function count_all_increases($id)
	{
		$this->db->from('increases');
    	$this->db->where('increase_employee',$id);
		return $this->db->count_all_results();
	}

	public function get_profile($id)
	{
		$this->db->select('*');
		$this->db->from('employees emp');
	    $this->db->join('positions pos','emp.employee_position = pos.position_id');
	    $this->db->join('areas are','emp.employee_area = are.area_id');
		$this->db->where('emp.employee_id',$id);
		$query = $this->db->get();
		return $query->row();
	}
}
