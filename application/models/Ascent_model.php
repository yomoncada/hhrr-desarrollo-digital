<?php  
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ascent_model extends CI_Model {
	
	var $table = 'ascents';
	var $column_order = array('asc.ascent_id','emp.employee_firstname','are.area_name','pos.position_name','asc.ascent_salary','asc.ascent_date','asc.ascent_status',null);
	var $column_search = array('asc.ascent_id','emp.employee_firstname','emp.employee_firstlastname','are.area_name','pos.position_name','asc.ascent_salary','asc.ascent_date','asc.ascent_status');
	var $order = array('ascent_id' => 'desc');


	public function __construct() {
		parent::__construct();
	}

	private function _get_pending_datatables_query()
	{
		
		$this->db->select('asc.ascent_id, asc.ascent_employee, emp.employee_firstname, emp.employee_firstlastname, are.area_name, pos.position_name, asc.ascent_salary, asc.ascent_date, asc.ascent_status');
    	$this->db->from('ascents asc');
	    $this->db->join('employees emp','asc.ascent_employee = emp.employee_id');
	    $this->db->join('areas are','asc.ascent_area = are.area_id');
	    $this->db->join('positions pos','asc.ascent_position = pos.position_id');
		$this->db->where('asc.ascent_status','Pending');

		$i = 0;

		foreach ($this->column_search as $item)
		{
			if($_POST['search']['value'])
			{
				
				if($i===0)
				{
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i)
					$this->db->group_end();
				}
				$i++;
			}

		if(isset($_POST['order']))
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	private function _get_processed_datatables_query()
	{
		
		$this->db->select('asc.ascent_id, asc.ascent_employee, emp.employee_firstname, emp.employee_firstlastname, are.area_name, pos.position_name, asc.ascent_salary, asc.ascent_date, asc.ascent_status');
    	$this->db->from('ascents asc');
	    $this->db->join('employees emp','asc.ascent_employee = emp.employee_id');
	    $this->db->join('areas are','asc.ascent_area = are.area_id');
	    $this->db->join('positions pos','asc.ascent_position = pos.position_id');
		$this->db->where('asc.ascent_status','Processed');

		$i = 0;

		foreach ($this->column_search as $item)
		{
			if($_POST['search']['value'])
			{
				
				if($i===0)
				{
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i)
					$this->db->group_end();
				}
				$i++;
			}

		if(isset($_POST['order']))
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_pending_datatables()
	{
		$this->_get_pending_datatables_query();
		if($_POST['length'] != -1)
			$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function get_processed_datatables()
	{
		$this->_get_processed_datatables_query();
		if($_POST['length'] != -1)
			$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_pending_filtered()
	{
		$this->_get_pending_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	function count_processed_filtered()
	{
		$this->_get_processed_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	public function count_by_employee($employee_id)
	{
		$this->db->from($this->table);
		$this->db->where('ascent_employee',$employee_id);
		return $this->db->count_all_results();
	}

	public function count_by_area($area_id)
	{
		$this->db->from($this->table);
		$this->db->where('ascent_area',$area_id);
		return $this->db->count_all_results();
	}

	public function get($id)
	{
		$this->db->where('ascent_id',$id);
		$query = $this->db->get('ascents');
		return $query->row();
	}

	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function update($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function update_employee_area($id, $area)
	{
		$this->db->set('employee_area', $area);
	    $this->db->where('employee_id', $id);
	    $this->db->update('employees');
	}
	public function update_employee_position($id, $position)
	{
		$this->db->set('employee_position', $position);
	    $this->db->where('employee_id', $id);
	    $this->db->update('employees');
	}

	public function update_employee_salary($id, $salary)
	{
		$this->db->set('employee_salary', $salary);
	    $this->db->where('employee_id', $id);
	    $this->db->update('employees');
	}
}