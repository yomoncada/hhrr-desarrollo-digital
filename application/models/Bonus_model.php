<?php  
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bonus_model extends CI_Model {
	
	var $table = 'bonuses';
	var $column_order = array('bon.bonus_id','emp.employee_firstname','bon.bonus_value','bon.bonus_date','bon.bonus_status',null);
	var $column_search = array('bon.bonus_id','emp.employee_firstname','emp.employee_firstlastname','bon.bonus_value','bon.bonus_date','bon.bonus_status');
	var $order = array('bonus_id' => 'desc');


	public function __construct() {
		parent::__construct();
	}

	private function _get_pending_datatables_query()
	{
		
		$this->db->select('bon.bonus_id, bon.bonus_employee, emp.employee_firstname, emp.employee_firstlastname, bon.bonus_value, bon.bonus_date, bon.bonus_status');
    	$this->db->from('bonuses bon');
	    $this->db->join('employees emp','bon.bonus_employee = emp.employee_id');
		$this->db->where('bon.bonus_status','Pending');

		$i = 0;

		foreach ($this->column_search as $item)
		{
			if($_POST['search']['value'])
			{
				
				if($i===0)
				{
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i)
					$this->db->group_end();
				}
				$i++;
			}

		if(isset($_POST['order']))
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	private function _get_processed_datatables_query()
	{
		
		$this->db->select('bon.bonus_id, bon.bonus_employee, emp.employee_firstname, emp.employee_firstlastname, bon.bonus_value, bon.bonus_date, bon.bonus_status');
    	$this->db->from('bonuses bon');
	    $this->db->join('employees emp','bon.bonus_employee = emp.employee_id');
		$this->db->where('bon.bonus_status','Processed');

		$i = 0;

		foreach ($this->column_search as $item)
		{
			if($_POST['search']['value'])
			{
				
				if($i===0)
				{
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i)
					$this->db->group_end();
				}
				$i++;
			}

		if(isset($_POST['order']))
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_pending_datatables()
	{
		$this->_get_pending_datatables_query();
		if($_POST['length'] != -1)
			$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function get_processed_datatables()
	{
		$this->_get_processed_datatables_query();
		if($_POST['length'] != -1)
			$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_pending_filtered()
	{
		$this->_get_pending_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	function count_processed_filtered()
	{
		$this->_get_processed_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	public function count_by_employee($id)
	{
		$this->db->from($this->table);
		$this->db->where('bonus_employee',$id);
		return $this->db->count_all_results();
	}

	public function get($id)
	{
		$this->db->where('bonus_id',$id);
		$query = $this->db->get('bonuses');
		return $query->row();
	}

	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function update($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}
}