<?php  
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class position_model extends CI_Model {
	
	var $table = 'positions';
	var $column_order = array('pos.position_id','pos.position_name','are.area_name','pos.position_status','pos.position_description',null);
	var $column_search = array('pos.position_id','pos.position_name','are.area_name','pos.position_status','pos.position_description');
	var $order = array('pos.position_id' => 'desc');


	public function __construct() {
		parent::__construct();
	}

	private function _get_datatables_query_actives()
	{
		
		$this->db->select('pos.position_id, pos.position_name, are.area_name, pos.position_description, pos.position_status');
    	$this->db->from('positions pos');
	    $this->db->join('areas are','pos.position_area = are.area_id');
		$this->db->where('pos.position_status','Active');

		$i = 0;

		foreach ($this->column_search as $item)
		{
			if($_POST['search']['value'])
			{
				
				if($i===0)
				{
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i)
					$this->db->group_end();
				}
				$i++;
			}

		if(isset($_POST['order']))
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	private function _get_datatables_query_inactives()
	{
		
		$this->db->select('pos.position_id, pos.position_name, are.area_name, pos.position_description, pos.position_status');
    	$this->db->from('positions pos');
	    $this->db->join('areas are','pos.position_area = are.area_id');
		$this->db->where('pos.position_status','Inactive');

		$i = 0;

		foreach ($this->column_search as $item)
		{
			if($_POST['search']['value'])
			{
				
				if($i===0)
				{
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i)
					$this->db->group_end();
				}
				$i++;
			}

		if(isset($_POST['order']))
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables_actives()
	{
		$this->_get_datatables_query_actives();
		if($_POST['length'] != -1)
			$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function get_datatables_inactives()
	{
		$this->_get_datatables_query_inactives();
		if($_POST['length'] != -1)
			$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered_actives()
	{
		$this->_get_datatables_query_actives();
		$query = $this->db->get();
		return $query->num_rows();
	}

	function count_filtered_inactives()
	{
		$this->_get_datatables_query_inactives();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	public function get($id)
	{
		$this->db->where('position_id',$id);
		$query = $this->db->get('positions');
		return $query->row();
	}

	function get_all()
	{
		$this->db->where('position_status','Active'); 
		$query = $this->db->get('positions');
      	return $query->result_array();
	}

	public function get_by_name($name)
	{
		$this->db->from('positions');
		$this->db->where('position_name',$name);
		$query = $this->db->get();
		return $query->row();
	}

	public function validate_by_name($name)
	{
		$this->db->where('position_name',$name);
		$query = $this->db->get('positions');
		return $query->num_rows();
	}

	public function create($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function update($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function activate($id)
	{
		$this->db->set('position_status','Active');
	    $this->db->where('position_id', $id);
	    $this->db->update($this->table);
	}

	public function deactivate($id)
	{
		$this->db->set('position_status','Inactive');
	    $this->db->where('position_id', $id);
	    $this->db->update($this->table);
	}
}