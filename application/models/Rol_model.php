<?php  
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rol_model extends CI_Model {
	
	var $table = 'roles';
	var $column_order = array('rol_id','rol_name','rol_description','rol_status',null);
	var $column_search = array('rol_id','rol_name','rol_description','rol_status');
	var $order = array('rol_id' => 'desc');


	public function __construct() {
		parent::__construct();
	}

	private function _get_datatables_query_actives()
	{
		$this->db->select('rol_id, rol_name, rol_description, rol_status');
    	$this->db->from('roles');
		$this->db->where('rol_status','Active');

		$i = 0;

		foreach ($this->column_search as $item)
		{
			if($_POST['search']['value'])
			{
				
				if($i===0)
				{
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i)
					$this->db->group_end();
				}
				$i++;
			}

		if(isset($_POST['order']))
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	private function _get_datatables_query_inactives()
	{
		
		$this->db->select('rol_id, rol_name, rol_description, rol_status');
    	$this->db->from('roles');
		$this->db->where('rol_status','Inactive');

		$i = 0;

		foreach ($this->column_search as $item)
		{
			if($_POST['search']['value'])
			{
				
				if($i===0)
				{
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i)
					$this->db->group_end();
				}
				$i++;
			}

		if(isset($_POST['order']))
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables_actives()
	{
		$this->_get_datatables_query_actives();
		if($_POST['length'] != -1)
			$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function get_datatables_inactives()
	{
		$this->_get_datatables_query_inactives();
		if($_POST['length'] != -1)
			$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered_actives()
	{
		$this->_get_datatables_query_actives();
		$query = $this->db->get();
		return $query->num_rows();
	}

	function count_filtered_inactives()
	{
		$this->_get_datatables_query_inactives();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	public function get($id)
	{
		$this->db->where('rol_id',$id);
		$query = $this->db->get('roles');
		return $query->row();
	}

	function get_all()
	{
		$this->db->where('rol_status','Active'); 
		$query = $this->db->get('roles');
      	return $query->result_array();
	}

	public function get_by_name($name)
	{
		$this->db->from('roles');
		$this->db->where('rol_name',$name);
		$query = $this->db->get();
		return $query->row();
	}

	public function validate_by_name($name)
	{
		$this->db->where('rol_name',$name);
		$query = $this->db->get('roles');
		return $query->num_rows();
	}

	public function create($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function update($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function activate($id)
	{
		$this->db->set('rol_status','Active');
	    $this->db->where('rol_id', $id);
	    $this->db->update($this->table);
	}

	public function deactivate($id)
	{
		$this->db->set('rol_status','Inactive');
	    $this->db->where('rol_id', $id);
	    $this->db->update($this->table);
	}
}