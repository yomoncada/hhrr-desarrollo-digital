<?php  
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Region_model extends CI_Model {
	
	var $table = 'regions';
	var $column_order = array('reg.region_id','reg.region_name','con.country_name','reg.region_status',null);
	var $column_search = array('reg.region_id','reg.region_name','con.country_name','reg.region_status');
	var $order = array('region_id' => 'desc');


	public function __construct() {
		parent::__construct();
	}

	private function _get_datatables_query_actives()
	{
		
		$this->db->select('reg.region_id, reg.region_name, con.country_name, reg.region_status');
    	$this->db->from('regions reg');
	    $this->db->join('countries con','reg.region_country = con.country_id');
		$this->db->where('reg.region_status','Active');

		$i = 0;

		foreach ($this->column_search as $item)
		{
			if($_POST['search']['value'])
			{
				
				if($i===0)
				{
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i)
					$this->db->group_end();
				}
				$i++;
			}

		if(isset($_POST['order']))
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	private function _get_datatables_query_inactives()
	{
		
		$this->db->select('reg.region_id, reg.region_name, con.country_name, reg.region_status');
    	$this->db->from('regions reg');
	    $this->db->join('countries con','reg.region_country = con.country_id');
		$this->db->where('reg.region_status','Inactive');

		$i = 0;

		foreach ($this->column_search as $item)
		{
			if($_POST['search']['value'])
			{
				
				if($i===0)
				{
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i)
					$this->db->group_end();
				}
				$i++;
			}

		if(isset($_POST['order']))
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables_actives()
	{
		$this->_get_datatables_query_actives();
		if($_POST['length'] != -1)
			$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function get_datatables_inactives()
	{
		$this->_get_datatables_query_inactives();
		if($_POST['length'] != -1)
			$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered_actives()
	{
		$this->_get_datatables_query_actives();
		$query = $this->db->get();
		return $query->num_rows();
	}

	function count_filtered_inactives()
	{
		$this->_get_datatables_query_inactives();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	public function get($id)
	{
		$this->db->where('region_id',$id);
		$query = $this->db->get('regions');
		return $query->row();
	}

	function get_all()
	{
		$this->db->where('region_status','Active'); 
		$query = $this->db->get('regions');
      	return $query->result_array();
	}

	public function get_by_name($name)
	{
		$this->db->from('regions');
		$this->db->where('region_name',$name);
		$query = $this->db->get();
		return $query->row();
	}

	public function get_by_country($id_country)
	{
		$this->db->select('region_id, region_name');
		$this->db->from('regions');
		$this->db->where('region_country',$id_country);
		$query = $this->db->get();
		return $query->result_array();
	}

	public function validate_by_name($name)
	{
		$this->db->where('region_name',$name);
		$query = $this->db->get('regions');
		return $query->num_rows();
	}

	public function create($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function update($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function activate($id)
	{
		$this->db->set('region_status','Active');
	    $this->db->where('region_id', $id);
	    $this->db->update($this->table);
	}

	public function deactivate($id)
	{
		$this->db->set('region_status','Inactive');
	    $this->db->where('region_id', $id);
	    $this->db->update($this->table);
	}
}