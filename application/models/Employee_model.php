<?php  
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Employee_model extends CI_Model {
	
	var $employees_table = 'employees';
	var $employee_column_order = array('emp.employee_id','emp.employee_firstname','emp.employee_firstlastname','rol.rol_name','emp.employee_status',null);
	var $employee_column_search = array('emp.employee_id','emp.employee_firstname','emp.employee_firstlastname','rol.rol_name','emp.employee_status');
	var $employee_order = array('employee_id' => 'desc');
	var $empski_table = 'employees_skills';
	var $empski_column_order = array('empski.empski_id','ski.skill_name','empski.empski_level','gra.grade_name','empski.empski_time','empski.empski_status',null);
	var $empski_column_search = array('empski.empski_id','ski.skill_name','empski.empski_level','gra.grade_name','empski.empski_time','empski.empski_status');
	var $empski_order = array('empski_id' => 'desc');
	var $empwork_table = 'employees_works';
	var $empwork_column_order = array('empwork.empwork_id','empwork.empwork_company','con.country_name','pos.position_name','empwork.empwork_time','empwork.empwork_status',null);
	var $empwork_column_search = array('empwork.empwork_id','empwork.empwork_company','con.country_name','pos.position_name','empwork.empwork_time','empwork.empwork_status');
	var $empwork_order = array('empwork_id' => 'desc');
	var $empalle_table = 'employees_allergies';
	var $empalle_column_order = array('empalle.empalle_id','alle.allergie_name','empalle.empalle_status',null);
	var $empalle_column_search = array('empalle.empalle_id','alle.allergie_name','empalle.empalle_status');
	var $empalle_order = array('empalle_id' => 'desc');
	var $empdis_table = 'employees_diseases';
	var $empdis_column_order = array('empdis.empdis_id','dis.disease_name','empdis.empdis_diagnostic','empdis.empdis_date','empdis.empdis_control','empdis.empdis_status',null);
	var $empdis_column_search = array('empdis.empdis_id','dis.disease_name','empdis.empdis_diagnostic','empdis.empdis_date','empdis.empdis_control','empdis.empdis_status');
	var $empdis_order = array('empdis_id' => 'desc');
	var $empema_table = 'employees_emails';
	var $empema_column_order = array('empema.empema_id','empema.empema_email','empema.empema_status',null);
	var $empema_column_search = array('empema.empema_id','empema.empema_email','empema.empema_status');
	var $empema_order = array('empema_id' => 'desc');

	public function __construct() {
		parent::__construct();
	}

	private function _get_employees_datatables_query_actives()
	{
		
		$this->db->select('emp.employee_id, emp.employee_firstname, emp.employee_firstlastname, rol.rol_name, emp.employee_status');
    	$this->db->from('employees emp');
	    $this->db->join('roles rol','emp.employee_rol = rol.rol_id');
		$this->db->where('emp.employee_status','Active');

		$i = 0;

		foreach ($this->employee_column_search as $item)
		{
			if($_POST['search']['value'])
			{
				
				if($i===0)
				{
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->employee_column_search) - 1 == $i)
					$this->db->group_end();
				}
				$i++;
			}

		if(isset($_POST['order']))
		{
			$this->db->order_by($this->employee_column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->employee_order))
		{
			$order = $this->employee_order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	private function _get_employees_datatables_query_inactives()
	{
		
		$this->db->select('emp.employee_id, emp.employee_firstname, emp.employee_firstlastname, rol.rol_name, emp.employee_status');
    	$this->db->from('employees emp');
	    $this->db->join('roles rol','emp.employee_rol = rol.rol_id');
		$this->db->where('emp.employee_status','Inactive');

		$i = 0;

		foreach ($this->employee_column_search as $item)
		{
			if($_POST['search']['value'])
			{
				
				if($i===0)
				{
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->employee_column_search) - 1 == $i)
					$this->db->group_end();
				}
				$i++;
			}

		if(isset($_POST['order']))
		{
			$this->db->order_by($this->employee_column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->employee_order))
		{
			$order = $this->employee_order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_employees_datatables_actives()
	{
		$this->_get_employees_datatables_query_actives();
		if($_POST['length'] != -1)
			$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function get_employees_datatables_inactives()
	{
		$this->_get_employees_datatables_query_inactives();
		if($_POST['length'] != -1)
			$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_employees_filtered_actives()
	{
		$this->_get_employees_datatables_query_actives();
		$query = $this->db->get();
		return $query->num_rows();
	}

	function count_employees_filtered_inactives()
	{
		$this->_get_employees_datatables_query_inactives();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_employees_all()
	{
		$this->db->from($this->employees_table);
		return $this->db->count_all_results();
	}

	public function count_by_area($area_id)
	{
		$this->db->from($this->employees_table);
		$this->db->where('employee_area',$area_id);
		return $this->db->count_all_results();
	}

	private function _get_skills_datatables_query_actives($id)
	{
		
		$this->db->select('empski.empski_id, ski.skill_name, empski.empski_level, gra.grade_name, empski.empski_time, empski.empski_status');
    	$this->db->from('employees_skills empski');
	    $this->db->join('skills ski','empski.empski_skill = ski.skill_id');
	    $this->db->join('grades gra','empski.empski_grade = gra.grade_id');
		$this->db->where('empski.empski_employee',$id);
		$this->db->where('empski.empski_status','Active');

		$i = 0;

		foreach ($this->empski_column_search as $item)
		{
			if($_POST['search']['value'])
			{
				
				if($i===0)
				{
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->empski_column_search) - 1 == $i)
					$this->db->group_end();
				}
				$i++;
			}

		if(isset($_POST['order']))
		{
			$this->db->order_by($this->empski_column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->empski_order))
		{
			$order = $this->empski_order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	private function _get_skills_datatables_query_inactives($id)
	{
		
		$this->db->select('empski.empski_id, ski.skill_name, empski.empski_level, gra.grade_name, empski.empski_time, empski.empski_status');
    	$this->db->from('employees_skills empski');
	    $this->db->join('skills ski','empski.empski_skill = ski.skill_id');
	    $this->db->join('grades gra','empski.empski_grade = gra.grade_id');
		$this->db->where('empski.empski_employee',$id);
		$this->db->where('empski.empski_status','Inactive');

		$i = 0;

		foreach ($this->empski_column_search as $item)
		{
			if($_POST['search']['value'])
			{
				
				if($i===0)
				{
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->empski_column_search) - 1 == $i)
					$this->db->group_end();
				}
				$i++;
			}

		if(isset($_POST['order']))
		{
			$this->db->order_by($this->empski_column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->empski_order))
		{
			$order = $this->empski_order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_skills_datatables_actives($id)
	{
		$this->_get_skills_datatables_query_actives($id);
		if($_POST['length'] != -1)
			$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function get_skills_datatables_inactives($id)
	{
		$this->_get_skills_datatables_query_inactives($id);
		if($_POST['length'] != -1)
			$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_skills_filtered_actives($id)
	{
		$this->_get_skills_datatables_query_actives($id);
		$query = $this->db->get();
		return $query->num_rows();
	}

	function count_skills_filtered_inactives($id)
	{
		$this->_get_skills_datatables_query_inactives($id);
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_skills_all($id)
	{
		$this->db->from($this->empski_table);
		$this->db->where('empski_employee',$id);
		return $this->db->count_all_results();
	}

	private function _get_works_datatables_query_actives($id)
	{
		
		$this->db->select('empwork.empwork_id, empwork.empwork_company, con.country_name, pos.position_name, empwork.empwork_time, empwork.empwork_status');
    	$this->db->from('employees_works empwork');
	    $this->db->join('countries con','empwork.empwork_country = con.country_id');
	    $this->db->join('positions pos','empwork.empwork_position = pos.position_id');
		$this->db->where('empwork.empwork_employee',$id);
		$this->db->where('empwork.empwork_status','Active');

		$i = 0;

		foreach ($this->empwork_column_search as $item)
		{
			if($_POST['search']['value'])
			{
				
				if($i===0)
				{
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->empwork_column_search) - 1 == $i)
					$this->db->group_end();
				}
				$i++;
			}

		if(isset($_POST['order']))
		{
			$this->db->order_by($this->empwork_column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->empwork_order))
		{
			$order = $this->empwork_order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	private function _get_works_datatables_query_inactives($id)
	{
		
		$this->db->select('empwork.empwork_id, empwork.empwork_company, con.country_name, pos.position_name, empwork.empwork_time, empwork.empwork_status');
    	$this->db->from('employees_works empwork');
	    $this->db->join('countries con','empwork.empwork_country = con.country_id');
	    $this->db->join('positions pos','empwork.empwork_position = pos.position_id');
		$this->db->where('empwork.empwork_employee',$id);
		$this->db->where('empwork.empwork_status','Inactive');

		$i = 0;

		foreach ($this->empwork_column_search as $item)
		{
			if($_POST['search']['value'])
			{
				
				if($i===0)
				{
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->empwork_column_search) - 1 == $i)
					$this->db->group_end();
				}
				$i++;
			}

		if(isset($_POST['order']))
		{
			$this->db->order_by($this->empwork_column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->empwork_order))
		{
			$order = $this->empwork_order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_works_datatables_actives($id)
	{
		$this->_get_works_datatables_query_actives($id);
		if($_POST['length'] != -1)
			$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function get_works_datatables_inactives($id)
	{
		$this->_get_works_datatables_query_inactives($id);
		if($_POST['length'] != -1)
			$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_works_filtered_actives($id)
	{
		$this->_get_works_datatables_query_actives($id);
		$query = $this->db->get();
		return $query->num_rows();
	}

	function count_works_filtered_inactives($id)
	{
		$this->_get_works_datatables_query_inactives($id);
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_works_all($id)
	{
		$this->db->from($this->empwork_table);
		$this->db->where('empwork_employee',$id);
		return $this->db->count_all_results();
	}

	private function _get_allergies_datatables_query_actives($id)
	{
		
		$this->db->select('empalle.empalle_id, alle.allergie_name, empalle.empalle_status');
    	$this->db->from('employees_allergies empalle');
	    $this->db->join('allergies alle','empalle.empalle_allergie = alle.allergie_id');
		$this->db->where('empalle.empalle_employee',$id);
		$this->db->where('empalle.empalle_status','Active');

		$i = 0;

		foreach ($this->empalle_column_search as $item)
		{
			if($_POST['search']['value'])
			{
				
				if($i===0)
				{
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->empalle_column_search) - 1 == $i)
					$this->db->group_end();
				}
				$i++;
			}

		if(isset($_POST['order']))
		{
			$this->db->order_by($this->empalle_column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->empalle_order))
		{
			$order = $this->empalle_order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	private function _get_allergies_datatables_query_inactives($id)
	{
		
		$this->db->select('empalle.empalle_id, alle.allergie_name, empalle.empalle_status');
    	$this->db->from('employees_allergies empalle');
	    $this->db->join('allergies alle','empalle.empalle_allergie = alle.allergie_id');
		$this->db->where('empalle.empalle_employee',$id);
		$this->db->where('empalle.empalle_status','Inactive');

		$i = 0;

		foreach ($this->empalle_column_search as $item)
		{
			if($_POST['search']['value'])
			{
				
				if($i===0)
				{
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->empalle_column_search) - 1 == $i)
					$this->db->group_end();
				}
				$i++;
			}

		if(isset($_POST['order']))
		{
			$this->db->order_by($this->empalle_column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->empalle_order))
		{
			$order = $this->empalle_order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_allergies_datatables_actives($id)
	{
		$this->_get_allergies_datatables_query_actives($id);
		if($_POST['length'] != -1)
			$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function get_allergies_datatables_inactives($id)
	{
		$this->_get_allergies_datatables_query_inactives($id);
		if($_POST['length'] != -1)
			$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_allergies_filtered_actives($id)
	{
		$this->_get_allergies_datatables_query_actives($id);
		$query = $this->db->get();
		return $query->num_rows();
	}

	function count_allergies_filtered_inactives($id)
	{
		$this->_get_allergies_datatables_query_inactives($id);
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_allergies_all($id)
	{
		$this->db->from($this->empalle_table);
		$this->db->where('empalle_employee',$id);
		return $this->db->count_all_results();
	}

	private function _get_diseases_datatables_query_actives($id)
	{
		
		$this->db->select('empdis.empdis_id, dis.disease_name, empdis.empdis_diagnostic, empdis.empdis_date, empdis.empdis_control, empdis.empdis_status');
    	$this->db->from('employees_diseases empdis');
	    $this->db->join('diseases dis','empdis.empdis_disease = dis.disease_id');
		$this->db->where('empdis.empdis_employee',$id);
		$this->db->where('empdis.empdis_status','Active');

		$i = 0;

		foreach ($this->empdis_column_search as $item)
		{
			if($_POST['search']['value'])
			{
				
				if($i===0)
				{
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->empdis_column_search) - 1 == $i)
					$this->db->group_end();
				}
				$i++;
			}

		if(isset($_POST['order']))
		{
			$this->db->order_by($this->empdis_column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->empdis_order))
		{
			$order = $this->empdis_order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	private function _get_diseases_datatables_query_inactives($id)
	{
		
		$this->db->select('empdis.empdis_id, dis.disease_name, empdis.empdis_diagnostic, empdis.empdis_date, empdis.empdis_control, empdis.empdis_status');
    	$this->db->from('employees_diseases empdis');
	    $this->db->join('diseases dis','empdis.empdis_disease = dis.disease_id');
		$this->db->where('empdis.empdis_employee',$id);
		$this->db->where('empdis.empdis_status','Inactive');

		$i = 0;

		foreach ($this->empdis_column_search as $item)
		{
			if($_POST['search']['value'])
			{
				
				if($i===0)
				{
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->empdis_column_search) - 1 == $i)
					$this->db->group_end();
				}
				$i++;
			}

		if(isset($_POST['order']))
		{
			$this->db->order_by($this->empdis_column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->empdis_order))
		{
			$order = $this->empdis_order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_diseases_datatables_actives($id)
	{
		$this->_get_diseases_datatables_query_actives($id);
		if($_POST['length'] != -1)
			$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function get_diseases_datatables_inactives($id)
	{
		$this->_get_diseases_datatables_query_inactives($id);
		if($_POST['length'] != -1)
			$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_diseases_filtered_actives($id)
	{
		$this->_get_diseases_datatables_query_actives($id);
		$query = $this->db->get();
		return $query->num_rows();
	}

	function count_diseases_filtered_inactives($id)
	{
		$this->_get_diseases_datatables_query_inactives($id);
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_diseases_all($id)
	{
		$this->db->from($this->empdis_table);
		$this->db->where('empdis_employee',$id);
		return $this->db->count_all_results();
	}

	private function _get_emails_datatables_query_actives($id)
	{
		
		$this->db->select('empema.empema_id, empema.empema_email, empema.empema_status');
    	$this->db->from('employees_emails empema');
		$this->db->where('empema.empema_employee',$id);
		$this->db->where('empema.empema_status','Active');

		$i = 0;

		foreach ($this->empema_column_search as $item)
		{
			if($_POST['search']['value'])
			{
				
				if($i===0)
				{
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->empema_column_search) - 1 == $i)
					$this->db->group_end();
				}
				$i++;
			}

		if(isset($_POST['order']))
		{
			$this->db->order_by($this->empema_column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->empema_order))
		{
			$order = $this->empema_order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	private function _get_emails_datatables_query_inactives($id)
	{
		
		$this->db->select('empema.empema_id, empema.empema_email, empema.empema_status');
    	$this->db->from('employees_emails empema');
		$this->db->where('empema.empema_employee',$id);
		$this->db->where('empema.empema_status','Inactive');

		$i = 0;

		foreach ($this->empema_column_search as $item)
		{
			if($_POST['search']['value'])
			{
				
				if($i===0)
				{
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->empema_column_search) - 1 == $i)
					$this->db->group_end();
				}
				$i++;
			}

		if(isset($_POST['order']))
		{
			$this->db->order_by($this->empema_column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->empema_order))
		{
			$order = $this->empema_order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_emails_datatables_actives($id)
	{
		$this->_get_emails_datatables_query_actives($id);
		if($_POST['length'] != -1)
			$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function get_emails_datatables_inactives($id)
	{
		$this->_get_emails_datatables_query_inactives($id);
		if($_POST['length'] != -1)
			$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_emails_filtered_actives($id)
	{
		$this->_get_emails_datatables_query_actives($id);
		$query = $this->db->get();
		return $query->num_rows();
	}

	function count_emails_filtered_inactives($id)
	{
		$this->_get_emails_datatables_query_inactives($id);
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_emails_all($id)
	{
		$this->db->from($this->empema_table);
		$this->db->where('empema_employee',$id);
		return $this->db->count_all_results();
	}

	public function get($username,$password)
	{
		$this->db->where('employee_username',$username);
		$this->db->where('employee_password',$password);
		$query = $this->db->get('employees');
		return $query->row();
	}

	function get_all()
	{
		$this->db->where('employee_status','Active'); 
		$query = $this->db->get('employees');
      	return $query->result_array();
	}

	public function get_by_id($id)
	{
		$this->db->from('employees');
		$this->db->where('employee_id',$id);
		$query = $this->db->get();
		return $query->row();
	}

	public function get_empski($id)
	{
		$this->db->from('employees_skills');
		$this->db->where('empski_id',$id);
		$query = $this->db->get();
		return $query->row();
	}

	public function get_empwork($id)
	{
		$this->db->from('employees_works');
		$this->db->where('empwork_id',$id);
		$query = $this->db->get();
		return $query->row();
	}

	public function get_empalle($id)
	{
		$this->db->from('employees_allergies');
		$this->db->where('empalle_id',$id);
		$query = $this->db->get();
		return $query->row();
	}

	public function get_empdis($id)
	{
		$this->db->from('employees_diseases');
		$this->db->where('empdis_id',$id);
		$query = $this->db->get();
		return $query->row();
	}

	public function get_empema($id)
	{
		$this->db->from('employees_emails');
		$this->db->where('empema_id',$id);
		$query = $this->db->get();
		return $query->row();
	}

	public function get_actual_password($id)
	{
		$this->db->select('employee_password');
		$this->db->from('employees');
		$this->db->where('employee_id',$id);
		$query = $this->db->get();
		return $query->row_array();
	}

	public function validate_login($username,$password)
	{
		$this->db->where('employee_username',$username);
		$this->db->where('employee_password',$password);
		$query = $this->db->get('employees');
		return $query->num_rows();
	}

	public function validate_recovery($username, $secques, $secans)
	{
		$this->db->from($this->employees_table);
		$this->db->where('employee_username',$username);
		$this->db->where('employee_secques',$secques);
		$this->db->where('employee_secans',$secans);
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function validate_by_id($id)
	{
		$this->db->where('employee_id',$id);
		$query = $this->db->get('employees');
		return $query->num_rows();
	}

	public function validate_by_username($username)
	{
		$this->db->where('employee_username',$username);
		$query = $this->db->get('employees');
		return $query->num_rows();
	}

	public function validate_actual_password($id,$actual_password)
	{
		$this->db->where('employee_id',$id);
		$this->db->where('employee_password',$actual_password);
		$query = $this->db->get('employees');
		return $query->num_rows();
	}

	public function set_temporal_password($username,$password)
	{
		$this->db->set('employee_password',$password);
		$this->db->where('employee_username',$username);
		$this->db->update($this->employees_table);
	}

	public function save($data)
	{
		$this->db->insert($this->employees_table, $data);
		return $this->db->insert_id();
	}

	public function update($where, $data)
	{
		$this->db->update($this->employees_table, $data, $where);
		return $this->db->affected_rows();
	}

	public function activate($id)
	{
		$this->db->set('employee_status','Active');
	    $this->db->where('employee_id', $id);
	    $this->db->update($this->employees_table);
	}

	public function deactivate($id)
	{
		$this->db->set('employee_status','Inactive');
	    $this->db->where('employee_id', $id);
	    $this->db->update($this->employees_table);
	}

	public function save_empski($data)
	{
		$this->db->insert($this->empski_table, $data);
		return $this->db->insert_id();
	}

	public function update_empski($where, $data)
	{
		$this->db->update($this->empski_table, $data, $where);
		return $this->db->affected_rows();
	}

	public function activate_empski($id)
	{
		$this->db->set('empski_status','Active');
	    $this->db->where('empski_id', $id);
	    $this->db->update($this->empski_table);
	}

	public function deactivate_empski($id)
	{
		$this->db->set('empski_status','Inactive');
	    $this->db->where('empski_id', $id);
	    $this->db->update($this->empski_table);
	}

	public function save_empwork($data)
	{
		$this->db->insert($this->empwork_table, $data);
		return $this->db->insert_id();
	}

	public function update_empwork($where, $data)
	{
		$this->db->update($this->empwork_table, $data, $where);
		return $this->db->affected_rows();
	}

	public function activate_empwork($id)
	{
		$this->db->set('empwork_status','Active');
	    $this->db->where('empwork_id', $id);
	    $this->db->update($this->empwork_table);
	}

	public function deactivate_empwork($id)
	{
		$this->db->set('empwork_status','Inactive');
	    $this->db->where('empwork_id', $id);
	    $this->db->update($this->empwork_table);
	}

	public function save_empalle($data)
	{
		$this->db->insert($this->empalle_table, $data);
		return $this->db->insert_id();
	}

	public function update_empalle($where, $data)
	{
		$this->db->update($this->empalle_table, $data, $where);
		return $this->db->affected_rows();
	}

	public function activate_empalle($id)
	{
		$this->db->set('empalle_status','Active');
	    $this->db->where('empalle_id', $id);
	    $this->db->update($this->empalle_table);
	}

	public function deactivate_empalle($id)
	{
		$this->db->set('empalle_status','Inactive');
	    $this->db->where('empalle_id', $id);
	    $this->db->update($this->empalle_table);
	}

	public function save_empdis($data)
	{
		$this->db->insert($this->empdis_table, $data);
		return $this->db->insert_id();
	}

	public function update_empdis($where, $data)
	{
		$this->db->update($this->empdis_table, $data, $where);
		return $this->db->affected_rows();
	}

	public function activate_empdis($id)
	{
		$this->db->set('empdis_status','Active');
	    $this->db->where('empdis_id', $id);
	    $this->db->update($this->empdis_table);
	}

	public function deactivate_empdis($id)
	{
		$this->db->set('empdis_status','Inactive');
	    $this->db->where('empdis_id', $id);
	    $this->db->update($this->empdis_table);
	}

	public function save_empema($data)
	{
		$this->db->insert($this->empema_table, $data);
		return $this->db->insert_id();
	}

	public function update_empema($where, $data)
	{
		$this->db->update($this->empema_table, $data, $where);
		return $this->db->affected_rows();
	}

	public function activate_empema($id)
	{
		$this->db->set('empema_status','Active');
	    $this->db->where('empema_id', $id);
	    $this->db->update($this->empema_table);
	}

	public function deactivate_empema($id)
	{
		$this->db->set('empema_status','Inactive');
	    $this->db->where('empema_id', $id);
	    $this->db->update($this->empema_table);
	}
}