-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: May 29, 2018 at 03:17 AM
-- Server version: 5.7.17-log
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hhrr`
--

-- --------------------------------------------------------

--
-- Table structure for table `absences`
--

CREATE TABLE `absences` (
  `absence_id` int(4) NOT NULL,
  `absence_employee` varchar(11) NOT NULL,
  `absence_area` int(4) NOT NULL,
  `absence_days` int(4) NOT NULL,
  `absence_startdate` date NOT NULL,
  `absence_enddate` date NOT NULL,
  `absence_reason` varchar(180) NOT NULL,
  `absence_file` varchar(45) NOT NULL,
  `absence_type` enum('Incapacity','Permission','Lack','Vacations') NOT NULL,
  `absence_status` enum('Pending','Processed') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `absences`
--

INSERT INTO `absences` (`absence_id`, `absence_employee`, `absence_area`, `absence_days`, `absence_startdate`, `absence_enddate`, `absence_reason`, `absence_file`, `absence_type`, `absence_status`) VALUES
(1, 'V-27402258', 1, 15, '2018-05-28', '0000-00-00', '', '', 'Incapacity', 'Processed'),
(2, 'V-27402258', 1, 15, '2018-05-10', '2018-05-25', '', '', 'Incapacity', 'Processed'),
(3, 'V-8589947', 1, 2, '2018-05-04', '2018-05-06', '', '', 'Lack', 'Pending'),
(4, 'V-27402258', 1, 15, '2018-05-28', '2018-06-06', '', '', 'Incapacity', 'Pending');

-- --------------------------------------------------------

--
-- Table structure for table `allergies`
--

CREATE TABLE `allergies` (
  `allergie_id` int(4) NOT NULL,
  `allergie_name` varchar(45) NOT NULL,
  `allergie_description` varchar(180) NOT NULL,
  `allergie_status` enum('Active','Inactive') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `allergies`
--

INSERT INTO `allergies` (`allergie_id`, `allergie_name`, `allergie_description`, `allergie_status`) VALUES
(1, 'Food allergie', 'Dairys', 'Active'),
(2, 'Air allergies', '', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `areas`
--

CREATE TABLE `areas` (
  `area_id` int(4) NOT NULL,
  `area_name` varchar(45) NOT NULL,
  `area_description` varchar(180) NOT NULL,
  `area_status` enum('Active','Inactive') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `areas`
--

INSERT INTO `areas` (`area_id`, `area_name`, `area_description`, `area_status`) VALUES
(1, ' IT Department', '', 'Active'),
(2, 'Dinning Room', '', 'Active'),
(3, 'I&D', '', 'Active'),
(4, 'Marketing', '', 'Active'),
(5, 'Accounting', '', 'Active'),
(6, 'Bathroom', '', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `ascents`
--

CREATE TABLE `ascents` (
  `ascent_id` int(4) NOT NULL,
  `ascent_employee` varchar(11) NOT NULL,
  `ascent_area` int(4) NOT NULL,
  `ascent_position` int(4) NOT NULL,
  `ascent_salary` int(11) NOT NULL,
  `ascent_date` date NOT NULL,
  `ascent_status` enum('Processed','Pending') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ascents`
--

INSERT INTO `ascents` (`ascent_id`, `ascent_employee`, `ascent_area`, `ascent_position`, `ascent_salary`, `ascent_date`, `ascent_status`) VALUES
(1, 'V-27402258', 1, 1, 123, '2018-05-28', 'Processed'),
(2, 'V-8071662', 1, 1, 123123, '2018-05-03', 'Processed'),
(3, 'V-8589947', 2, 1, 123, '2018-05-12', 'Pending');

-- --------------------------------------------------------

--
-- Table structure for table `bonuses`
--

CREATE TABLE `bonuses` (
  `bonus_id` int(4) NOT NULL,
  `bonus_employee` varchar(11) NOT NULL,
  `bonus_area` int(4) NOT NULL,
  `bonus_value` int(11) NOT NULL,
  `bonus_date` date NOT NULL,
  `bonus_status` enum('Processed','Pending') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bonuses`
--

INSERT INTO `bonuses` (`bonus_id`, `bonus_employee`, `bonus_area`, `bonus_value`, `bonus_date`, `bonus_status`) VALUES
(1, 'V-27402258', 0, 123, '2018-05-23', 'Processed'),
(2, 'V-8071662', 0, 125, '2018-05-02', 'Pending');

-- --------------------------------------------------------

--
-- Table structure for table `bosses`
--

CREATE TABLE `bosses` (
  `boss_id` int(4) NOT NULL,
  `boss_employee` varchar(45) NOT NULL,
  `boss_area` int(4) NOT NULL,
  `boss_status` enum('Active','Inactive') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bosses`
--

INSERT INTO `bosses` (`boss_id`, `boss_employee`, `boss_area`, `boss_status`) VALUES
(1, 'V-8589947', 1, 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `city_id` int(4) NOT NULL,
  `city_name` varchar(45) NOT NULL,
  `city_region` int(4) NOT NULL,
  `city_status` enum('Active','Inactive') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`city_id`, `city_name`, `city_region`, `city_status`) VALUES
(1, 'Miami', 1, 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `country_id` int(4) NOT NULL,
  `country_name` varchar(45) NOT NULL,
  `country_status` enum('Active','Inactive') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`country_id`, `country_name`, `country_status`) VALUES
(1, 'United States', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `diseases`
--

CREATE TABLE `diseases` (
  `disease_id` int(4) NOT NULL,
  `disease_name` varchar(45) NOT NULL,
  `disease_description` varchar(180) NOT NULL,
  `disease_status` enum('Active','Inactive') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `diseases`
--

INSERT INTO `diseases` (`disease_id`, `disease_name`, `disease_description`, `disease_status`) VALUES
(1, 'Asthma', '', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `employee_id` varchar(45) NOT NULL,
  `employee_firstname` varchar(45) NOT NULL,
  `employee_secondname` varchar(45) NOT NULL,
  `employee_firstlastname` varchar(45) NOT NULL,
  `employee_secondlastname` varchar(45) NOT NULL,
  `employee_birth` date NOT NULL,
  `employee_country` int(4) NOT NULL,
  `employee_region` int(4) NOT NULL,
  `employee_city` int(4) NOT NULL,
  `employee_address` varchar(180) NOT NULL,
  `employee_neighborhood` varchar(45) NOT NULL,
  `employee_cellphone` varchar(45) NOT NULL,
  `employee_phone` varchar(45) NOT NULL,
  `employee_whatsapp` varchar(45) NOT NULL,
  `employee_facebook` varchar(45) NOT NULL,
  `employee_gplus` varchar(45) NOT NULL,
  `employee_linkedin` varchar(45) NOT NULL,
  `employee_file` varchar(45) NOT NULL,
  `employee_rol` int(4) NOT NULL,
  `employee_area` int(4) NOT NULL,
  `employee_position` int(4) NOT NULL,
  `employee_boss` int(4) NOT NULL,
  `employee_salary` varchar(11) NOT NULL,
  `employee_username` varchar(45) NOT NULL,
  `employee_password` varchar(45) NOT NULL,
  `employee_secques` enum('What is the name of your best childhood friend?','What is the name of your first pet?','What is the model of your first car?','What is the profession of your paternal grandfather?','What is the country you would like to visit?') NOT NULL,
  `employee_secans` varchar(45) NOT NULL,
  `employee_status` enum('Active','Inactive') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`employee_id`, `employee_firstname`, `employee_secondname`, `employee_firstlastname`, `employee_secondlastname`, `employee_birth`, `employee_country`, `employee_region`, `employee_city`, `employee_address`, `employee_neighborhood`, `employee_cellphone`, `employee_phone`, `employee_whatsapp`, `employee_facebook`, `employee_gplus`, `employee_linkedin`, `employee_file`, `employee_rol`, `employee_area`, `employee_position`, `employee_boss`, `employee_salary`, `employee_username`, `employee_password`, `employee_secques`, `employee_secans`, `employee_status`) VALUES
('V-27402258', 'Yonathan', 'Alejandro', 'Moncada', 'Navarro', '1997-05-15', 1, 1, 1, 'Urb. San\'t Omero II, Calle N, Casa 2, El Consejo 2118 Estado Aragua', '', '+584243591604', '+582443234011', '+584243591604', 'Yonathan Moncada', 'Yonathan Moncada', 'Yonathan Moncada', '', 1, 1, 1, 1, '246', 'yomoncada', '123123123', 'What is the name of your first pet?', 'Firulais', 'Active'),
('V-8071662', 'Luciano', '', 'Moncada', '', '0000-00-00', 0, 0, 0, '', '', '', '', '', '', '', '', '', 3, 2, 2, 1, '123123', 'lucianoamz', '123123', 'What is the name of your best childhood friend?', 'asd', 'Active'),
('V-8589947', 'Esmeralda', '', 'Navarro', '', '0000-00-00', 1, 0, 0, '', '', '', '', '', '', '', '', '', 2, 6, 3, 1, '3.36', 'esmenav63', '123123123', 'What is the model of your first car?', 'asd', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `employees_allergies`
--

CREATE TABLE `employees_allergies` (
  `empalle_id` int(4) NOT NULL,
  `empalle_employee` varchar(11) NOT NULL,
  `empalle_allergie` int(4) NOT NULL,
  `empalle_status` enum('Active','Inactive') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `employees_allergies`
--

INSERT INTO `employees_allergies` (`empalle_id`, `empalle_employee`, `empalle_allergie`, `empalle_status`) VALUES
(1, 'V-8071662', 2, 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `employees_diseases`
--

CREATE TABLE `employees_diseases` (
  `empdis_id` int(4) NOT NULL,
  `empdis_employee` varchar(11) NOT NULL,
  `empdis_disease` int(4) NOT NULL,
  `empdis_diagnostic` varchar(180) NOT NULL,
  `empdis_date` date NOT NULL,
  `empdis_control` enum('Yes','No') NOT NULL,
  `empdis_status` enum('Active','Inactive') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `employees_diseases`
--

INSERT INTO `employees_diseases` (`empdis_id`, `empdis_employee`, `empdis_disease`, `empdis_diagnostic`, `empdis_date`, `empdis_control`, `empdis_status`) VALUES
(1, 'V-8589947', 1, '', '2005-05-25', 'Yes', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `employees_emails`
--

CREATE TABLE `employees_emails` (
  `empema_id` int(4) NOT NULL,
  `empema_employee` varchar(11) NOT NULL,
  `empema_email` varchar(45) NOT NULL,
  `empema_status` enum('Active','Inactive') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `employees_emails`
--

INSERT INTO `employees_emails` (`empema_id`, `empema_employee`, `empema_email`, `empema_status`) VALUES
(1, 'V-8589947', 'esmenav63@gmail.com', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `employees_skills`
--

CREATE TABLE `employees_skills` (
  `empski_id` int(4) NOT NULL,
  `empski_employee` varchar(11) NOT NULL,
  `empski_skill` int(4) NOT NULL,
  `empski_level` int(4) NOT NULL,
  `empski_grade` int(4) NOT NULL,
  `empski_time` int(4) NOT NULL,
  `empski_status` enum('Active','Inactive') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `employees_skills`
--

INSERT INTO `employees_skills` (`empski_id`, `empski_employee`, `empski_skill`, `empski_level`, `empski_grade`, `empski_time`, `empski_status`) VALUES
(1, 'V-27402258', 1, 100, 1, 6, 'Active'),
(2, 'V-8589947', 1, 100, 2, 12, 'Active'),
(3, 'V-8589947', 2, 50, 4, 10, 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `employees_works`
--

CREATE TABLE `employees_works` (
  `empwork_id` int(4) NOT NULL,
  `empwork_employee` varchar(11) NOT NULL,
  `empwork_company` varchar(45) NOT NULL,
  `empwork_country` int(4) NOT NULL,
  `empwork_position` int(4) NOT NULL,
  `empwork_time` int(4) NOT NULL,
  `empwork_status` enum('Active','Inactive') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `employees_works`
--

INSERT INTO `employees_works` (`empwork_id`, `empwork_employee`, `empwork_company`, `empwork_country`, `empwork_position`, `empwork_time`, `empwork_status`) VALUES
(1, 'V-8589947', 'Google', 1, 2, 15, 'Active'),
(2, 'V-8589947', 'Fiverr', 1, 3, 12, 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `grades`
--

CREATE TABLE `grades` (
  `grade_id` int(4) NOT NULL,
  `grade_name` varchar(45) NOT NULL,
  `grade_description` varchar(180) NOT NULL,
  `grade_status` enum('Active','Inactive') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `grades`
--

INSERT INTO `grades` (`grade_id`, `grade_name`, `grade_description`, `grade_status`) VALUES
(1, 'Engineer', '', 'Active'),
(2, 'Amateur', '', 'Active'),
(3, 'Junior', '', 'Active'),
(4, 'Senior', '', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `increases`
--

CREATE TABLE `increases` (
  `increase_id` int(4) NOT NULL,
  `increase_employee` varchar(11) NOT NULL,
  `increase_area` int(4) NOT NULL,
  `increase_type` enum('Fixed','Percent') NOT NULL,
  `increase_value` int(11) NOT NULL,
  `increase_date` date NOT NULL,
  `increase_status` enum('Processed','Pending') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `increases`
--

INSERT INTO `increases` (`increase_id`, `increase_employee`, `increase_area`, `increase_type`, `increase_value`, `increase_date`, `increase_status`) VALUES
(1, 'V-27402258', 1, 'Fixed', 123, '2018-05-01', 'Processed'),
(2, 'V-8589947', 1, 'Percent', 12, '2018-05-12', 'Processed');

-- --------------------------------------------------------

--
-- Table structure for table `positions`
--

CREATE TABLE `positions` (
  `position_id` int(4) NOT NULL,
  `position_name` varchar(45) NOT NULL,
  `position_area` int(4) NOT NULL,
  `position_status` enum('Active','Inactive') NOT NULL,
  `position_description` varchar(180) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `positions`
--

INSERT INTO `positions` (`position_id`, `position_name`, `position_area`, `position_status`, `position_description`) VALUES
(1, 'Community Manager', 1, 'Active', ''),
(2, 'Web Developer', 3, 'Active', ''),
(3, 'Graphic Designer', 3, 'Active', ''),
(4, 'Seller', 4, 'Active', '');

-- --------------------------------------------------------

--
-- Table structure for table `regions`
--

CREATE TABLE `regions` (
  `region_id` int(4) NOT NULL,
  `region_name` varchar(45) NOT NULL,
  `region_country` int(4) NOT NULL,
  `region_status` enum('Active','Inactive') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `regions`
--

INSERT INTO `regions` (`region_id`, `region_name`, `region_country`, `region_status`) VALUES
(1, 'Northeast', 1, 'Active'),
(2, 'Midwestern', 1, 'Active'),
(3, 'Southern', 1, 'Active'),
(4, 'Western', 1, 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `rol_id` int(4) NOT NULL,
  `rol_name` varchar(45) NOT NULL,
  `rol_description` varchar(180) NOT NULL,
  `rol_status` enum('Active','Inactive') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`rol_id`, `rol_name`, `rol_description`, `rol_status`) VALUES
(1, 'Admin', '', 'Active'),
(2, 'Manager', '', 'Active'),
(3, ' Supervisor', '', 'Active'),
(4, 'Employee', '', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `skills`
--

CREATE TABLE `skills` (
  `skill_id` int(4) NOT NULL,
  `skill_name` varchar(45) NOT NULL,
  `skill_description` varchar(180) NOT NULL,
  `skill_status` enum('Active','Inactive') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `skills`
--

INSERT INTO `skills` (`skill_id`, `skill_name`, `skill_description`, `skill_status`) VALUES
(1, 'PHP', '', 'Active'),
(2, 'Ruby', '', 'Active');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `absences`
--
ALTER TABLE `absences`
  ADD PRIMARY KEY (`absence_id`),
  ADD KEY `absences_employee` (`absence_employee`),
  ADD KEY `absence_area` (`absence_area`);

--
-- Indexes for table `allergies`
--
ALTER TABLE `allergies`
  ADD PRIMARY KEY (`allergie_id`);

--
-- Indexes for table `areas`
--
ALTER TABLE `areas`
  ADD PRIMARY KEY (`area_id`);

--
-- Indexes for table `ascents`
--
ALTER TABLE `ascents`
  ADD PRIMARY KEY (`ascent_id`),
  ADD KEY `ascents_employee` (`ascent_employee`),
  ADD KEY `ascents_area` (`ascent_area`),
  ADD KEY `ascents_position` (`ascent_position`);

--
-- Indexes for table `bonuses`
--
ALTER TABLE `bonuses`
  ADD PRIMARY KEY (`bonus_id`),
  ADD KEY `bonus_employee` (`bonus_employee`);

--
-- Indexes for table `bosses`
--
ALTER TABLE `bosses`
  ADD PRIMARY KEY (`boss_id`),
  ADD KEY `bosses_area` (`boss_area`),
  ADD KEY `boss_employee` (`boss_employee`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`city_id`),
  ADD KEY `cities_region` (`city_region`);

--
-- Indexes for table `ci_sessions`
--
ALTER TABLE `ci_sessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ci_sessions_timestamp` (`timestamp`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`country_id`);

--
-- Indexes for table `diseases`
--
ALTER TABLE `diseases`
  ADD PRIMARY KEY (`disease_id`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`employee_id`),
  ADD KEY `employee_boss` (`employee_boss`),
  ADD KEY `employee_country` (`employee_country`,`employee_region`,`employee_city`,`employee_rol`,`employee_area`,`employee_position`);

--
-- Indexes for table `employees_allergies`
--
ALTER TABLE `employees_allergies`
  ADD PRIMARY KEY (`empalle_id`),
  ADD KEY `empalle_employee` (`empalle_employee`),
  ADD KEY `empalle_allergie` (`empalle_allergie`);

--
-- Indexes for table `employees_diseases`
--
ALTER TABLE `employees_diseases`
  ADD PRIMARY KEY (`empdis_id`),
  ADD KEY `empwork_employee` (`empdis_employee`),
  ADD KEY `empwork_disease` (`empdis_disease`);

--
-- Indexes for table `employees_emails`
--
ALTER TABLE `employees_emails`
  ADD PRIMARY KEY (`empema_id`),
  ADD KEY `empema_employee` (`empema_employee`);

--
-- Indexes for table `employees_skills`
--
ALTER TABLE `employees_skills`
  ADD PRIMARY KEY (`empski_id`),
  ADD KEY `empwork_employee` (`empski_employee`),
  ADD KEY `empwork_skill` (`empski_skill`),
  ADD KEY `empwork_grade` (`empski_grade`);

--
-- Indexes for table `employees_works`
--
ALTER TABLE `employees_works`
  ADD PRIMARY KEY (`empwork_id`),
  ADD KEY `empwork_employee` (`empwork_employee`),
  ADD KEY `empwork_country` (`empwork_country`),
  ADD KEY `empwork_position` (`empwork_position`);

--
-- Indexes for table `grades`
--
ALTER TABLE `grades`
  ADD PRIMARY KEY (`grade_id`);

--
-- Indexes for table `increases`
--
ALTER TABLE `increases`
  ADD PRIMARY KEY (`increase_id`),
  ADD KEY `increases_employee` (`increase_employee`);

--
-- Indexes for table `positions`
--
ALTER TABLE `positions`
  ADD PRIMARY KEY (`position_id`),
  ADD KEY `position_area` (`position_area`);

--
-- Indexes for table `regions`
--
ALTER TABLE `regions`
  ADD PRIMARY KEY (`region_id`),
  ADD KEY `regions_country` (`region_country`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`rol_id`);

--
-- Indexes for table `skills`
--
ALTER TABLE `skills`
  ADD PRIMARY KEY (`skill_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `absences`
--
ALTER TABLE `absences`
  MODIFY `absence_id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `allergies`
--
ALTER TABLE `allergies`
  MODIFY `allergie_id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `areas`
--
ALTER TABLE `areas`
  MODIFY `area_id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `ascents`
--
ALTER TABLE `ascents`
  MODIFY `ascent_id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `bonuses`
--
ALTER TABLE `bonuses`
  MODIFY `bonus_id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `bosses`
--
ALTER TABLE `bosses`
  MODIFY `boss_id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `city_id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `country_id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `diseases`
--
ALTER TABLE `diseases`
  MODIFY `disease_id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `employees_allergies`
--
ALTER TABLE `employees_allergies`
  MODIFY `empalle_id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `employees_diseases`
--
ALTER TABLE `employees_diseases`
  MODIFY `empdis_id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `employees_emails`
--
ALTER TABLE `employees_emails`
  MODIFY `empema_id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `employees_skills`
--
ALTER TABLE `employees_skills`
  MODIFY `empski_id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `employees_works`
--
ALTER TABLE `employees_works`
  MODIFY `empwork_id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `grades`
--
ALTER TABLE `grades`
  MODIFY `grade_id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `increases`
--
ALTER TABLE `increases`
  MODIFY `increase_id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `positions`
--
ALTER TABLE `positions`
  MODIFY `position_id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `regions`
--
ALTER TABLE `regions`
  MODIFY `region_id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `rol_id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `skills`
--
ALTER TABLE `skills`
  MODIFY `skill_id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `absences`
--
ALTER TABLE `absences`
  ADD CONSTRAINT `absences_ibfk_1` FOREIGN KEY (`absence_employee`) REFERENCES `employees` (`employee_id`);

--
-- Constraints for table `ascents`
--
ALTER TABLE `ascents`
  ADD CONSTRAINT `ascents_ibfk_1` FOREIGN KEY (`ascent_employee`) REFERENCES `employees` (`employee_id`),
  ADD CONSTRAINT `ascents_ibfk_2` FOREIGN KEY (`ascent_position`) REFERENCES `positions` (`position_id`),
  ADD CONSTRAINT `ascents_ibfk_3` FOREIGN KEY (`ascent_area`) REFERENCES `areas` (`area_id`);

--
-- Constraints for table `bonuses`
--
ALTER TABLE `bonuses`
  ADD CONSTRAINT `bonuses_ibfk_1` FOREIGN KEY (`bonus_employee`) REFERENCES `employees` (`employee_id`);

--
-- Constraints for table `bosses`
--
ALTER TABLE `bosses`
  ADD CONSTRAINT `bosses_ibfk_1` FOREIGN KEY (`boss_area`) REFERENCES `areas` (`area_id`);

--
-- Constraints for table `cities`
--
ALTER TABLE `cities`
  ADD CONSTRAINT `cities_ibfk_1` FOREIGN KEY (`city_region`) REFERENCES `regions` (`region_id`);

--
-- Constraints for table `employees_allergies`
--
ALTER TABLE `employees_allergies`
  ADD CONSTRAINT `employees_allergies_ibfk_1` FOREIGN KEY (`empalle_allergie`) REFERENCES `allergies` (`allergie_id`),
  ADD CONSTRAINT `employees_allergies_ibfk_2` FOREIGN KEY (`empalle_employee`) REFERENCES `employees` (`employee_id`);

--
-- Constraints for table `employees_diseases`
--
ALTER TABLE `employees_diseases`
  ADD CONSTRAINT `employees_diseases_ibfk_1` FOREIGN KEY (`empdis_employee`) REFERENCES `employees` (`employee_id`),
  ADD CONSTRAINT `employees_diseases_ibfk_2` FOREIGN KEY (`empdis_disease`) REFERENCES `diseases` (`disease_id`);

--
-- Constraints for table `employees_emails`
--
ALTER TABLE `employees_emails`
  ADD CONSTRAINT `employees_emails_ibfk_1` FOREIGN KEY (`empema_employee`) REFERENCES `employees` (`employee_id`);

--
-- Constraints for table `employees_skills`
--
ALTER TABLE `employees_skills`
  ADD CONSTRAINT `employees_skills_ibfk_1` FOREIGN KEY (`empski_skill`) REFERENCES `skills` (`skill_id`),
  ADD CONSTRAINT `employees_skills_ibfk_2` FOREIGN KEY (`empski_employee`) REFERENCES `employees` (`employee_id`),
  ADD CONSTRAINT `employees_skills_ibfk_3` FOREIGN KEY (`empski_grade`) REFERENCES `grades` (`grade_id`);

--
-- Constraints for table `employees_works`
--
ALTER TABLE `employees_works`
  ADD CONSTRAINT `employees_works_ibfk_1` FOREIGN KEY (`empwork_employee`) REFERENCES `employees` (`employee_id`),
  ADD CONSTRAINT `employees_works_ibfk_2` FOREIGN KEY (`empwork_position`) REFERENCES `positions` (`position_id`),
  ADD CONSTRAINT `employees_works_ibfk_3` FOREIGN KEY (`empwork_country`) REFERENCES `countries` (`country_id`);

--
-- Constraints for table `increases`
--
ALTER TABLE `increases`
  ADD CONSTRAINT `increases_ibfk_1` FOREIGN KEY (`increase_employee`) REFERENCES `employees` (`employee_id`);

--
-- Constraints for table `positions`
--
ALTER TABLE `positions`
  ADD CONSTRAINT `positions_ibfk_1` FOREIGN KEY (`position_area`) REFERENCES `areas` (`area_id`);

--
-- Constraints for table `regions`
--
ALTER TABLE `regions`
  ADD CONSTRAINT `regions_ibfk_1` FOREIGN KEY (`region_country`) REFERENCES `countries` (`country_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
