var save_method;
var pending_increases;
var processed_increases;

$(document).ready(function (){
	pending_increases = $('#pending_increases').DataTable({ 
		"processing": true,
		"serverSide": true,
		"order": [],

		"ajax":{
			"url": "http://localhost/hhrr-master/index.php/increase/list_pending_increases",
			"type": "POST"
		},

		"columnDefs": [
		{ 
			"targets": [ -1 ],
			"orderable": false,
		},
		],
	});

	processed_increases = $('#processed_increases').DataTable({ 
		"processing": true,
		"serverSide": true,
		"order": [],

		"ajax":{
			"url": "http://localhost/hhrr-master/index.php/increase/list_processed_increases",
			"type": "POST"
		},

		"columnDefs": [
		{ 
			"targets": [ -1 ],
			"orderable": false,
		},
		],
	});
});

function create_increase(){
	$('#increase_form')[0].reset();
	$('.form-group').removeClass('has-error');
    $('.form-group').removeClass('has-warning');
    $('.form-group').removeClass('has-success');
	$('.input-feedback').empty();
	$('#increase_modal_title').text('Create an increase');
	$('#increase_btn').css('display','block');
	$('#increase_btn').attr('onclick','save_increase("create")');
	$('#increase_btn').text('Create');
	$('[name="id"]').removeAttr('disabled',true);
	$('[name="employee"]').removeAttr('disabled',true);
	$('[name="type"]').removeAttr('disabled',true);
	$('[name="value"]').removeAttr('disabled',true);
}

function get_increase(id){
	$('#increase_form')[0].reset();
	$('.form-group').removeClass('has-error');
    $('.form-group').removeClass('has-warning');
    $('.form-group').removeClass('has-success');
	$('.input-feedback').empty();
	$('#increase_modal_title').text('Process an increase');
	$('#increase_btn').css('display','block');
	$('#increase_btn').attr('onclick','save_increase("update")');
	$('#increase_btn').text('Process');
	$('[name="id"]').removeAttr('disabled',true);
	$('[name="employee"]').removeAttr('disabled',true);
	$('[name="type"]').removeAttr('disabled',true);
	$('[name="value"]').removeAttr('disabled',true);

	$.ajax({
		url : "http://localhost/hhrr-master/index.php/increase/get_increase/" + id,
		type: "GET",
		dataType: "JSON",
		success: function (data){
			$('[name="id"]').val(data.increase_id);
			$('[name="employee"]').val(data.increase_employee);
			$('[name="type"]').val(data.increase_type);
			$('[name="value"]').val(data.increase_value);
			$('[name="date"]').val(data.increase_date);
		},
		error: function (jqXHR, textStatus, errorThrown){
			swal({
				title: "Error",
				text: "An error has occurred!",
				type: "error"
			});
		}
	});
}

function get_increase_unmodifiable(id){
	get_increase(id);
	$('#increase_modal_title').text('Read an increase');
	$('#increase_btn').css('display','none');

	$.ajax({
		url : "http://localhost/hhrr-master/index.php/increase/get_increase/" + id,
		type: "GET",
		dataType: "JSON",
		success: function (data){
			$('[name="date"]').val(data.increase_date);
			$('[name="id"]').attr('disabled',true);
			$('[name="employee"]').attr('disabled',true);
			$('[name="type"]').attr('disabled',true);
			$('[name="value"]').attr('disabled',true);
			$('[name="date"]').attr('disabled',true);
		},
		error: function (jqXHR, textStatus, errorThrown){
			swal({
				title: "Error",
				text: "An error has occurred!",
				type: "error"
			});
		}
	});
}

function save_increase(save_method){
	$('.form-group').removeClass('has-error');
    $('.form-group').removeClass('has-warning');
    $('.form-group').removeClass('has-success');
    $('.input-feedback').empty(); 

	var url;

	if(save_method == 'create'){
		url = "http://localhost/hhrr-master/index.php/increase/create_increase";
	}
	else{
		url = "http://localhost/hhrr-master/index.php/increase/update_increase";
	}

	$.ajax({
		url : url,
		type: "POST",
		data: $('#increase_form').serialize(),
		dataType: "JSON",
		success: function (data){
			if(data.status){
				if(save_method == 'create'){
					message = "Increase created!";
					$('#increase_form')[0].reset();
				}
				else{
					message = "Increase processed!";
				}

				swal({
					title: "Success",
					text: message,
					type: "success"
				}); 
				reload_increases();
			}
			else{
				if(data.inputerror)
                {
                    for(var i = 0; i < data.inputerror.length; i++){
                        $('[name="'+data.inputerror[i]+'"]').parent().addClass('has-error');
                        $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]);
                    }
                }
			}
		},
		error: function (jqXHR, textStatus, errorThrown){
			swal({
				title: "Error",
				text: "An error has occurred!",
				type: "error"
			});
		}
	});
}

function reload_increases(){
	pending_increases.ajax.reload(null,false);
	processed_increases.ajax.reload(null,false);
}