var save_method;
var active_diseases;
var inactive_diseases;

$(document).ready(function (){
	active_diseases = $('#active_diseases').DataTable({ 
		"processing": true,
		"serverSide": true,
		"order": [],

		"ajax":{
			"url": "http://localhost/hhrr-master/index.php/disease/list_active_diseases",
			"type": "POST"
		},

		"columnDefs": [
		{ 
			"targets": [ -1 ],
			"orderable": false,
		},
		],
	});

	inactive_diseases = $('#inactive_diseases').DataTable({ 
		"processing": true,
		"serverSide": true,
		"order": [],

		"ajax":{
			"url": "http://localhost/hhrr-master/index.php/disease/list_inactive_diseases",
			"type": "POST"
		},

		"columnDefs": [
		{ 
			"targets": [ -1 ],
			"orderable": false,
		},
		],
	});
});

function create_disease(){
	$('#disease_form')[0].reset();
	$('.form-group').removeClass('has-error');
    $('.form-group').removeClass('has-warning');
    $('.form-group').removeClass('has-success');
	$('.input-feedback').empty();
	$('#disease_modal_title').text('Create an disease');
	$('#disease_btn').attr('onclick','save_disease("create")');
	$('#disease_btn').text('Create');
}

function get_disease(id){
	$('#disease_form')[0].reset();
	$('.form-group').removeClass('has-error');
    $('.form-group').removeClass('has-warning');
    $('.form-group').removeClass('has-success');
	$('.input-feedback').empty();
	$('#disease_modal_title').text('Update an disease');
	$('#disease_btn').attr('onclick','save_disease("update")');
	$('#disease_btn').text('Save changes');

	$.ajax({
		url : "http://localhost/hhrr-master/index.php/disease/get_disease/" + id,
		type: "GET",
		dataType: "JSON",
		success: function (data){
			$('[name="id"]').val(data.disease_id);
			$('[name="name"]').val(data.disease_name);
			$('[name="description"]').val(data.disease_description);
		},
		error: function (jqXHR, textStatus, errorThrown){
			swal({
				title: "Error",
				text: "An error has occurred!",
				type: "error"
			});
		}
	});
}

function validate_name(){
	$('[name="name"]').parent().removeClass('has-error');
    $('[name="name"]').parent().removeClass('has-warning');
    $('[name="name"]').parent().removeClass('has-success');
    $('[name="name"]').next().empty();

    var name = $("#disease_name").val();

	$.ajax({
		url : "http://localhost/hhrr-master/index.php/disease/validate_name",
		type: 'GET',
		data: {'name':name},
		dataType: 'JSON',
		success: function (data){
			switch(data.type){
                case 'Warning':
                    $('[name="name"]').parent().addClass('has-warning');
                    $('[name="name"]').next().text(data.message);  
                break;

                case 'Notice':
                    $('[name="name"]').parent().addClass('has-success');
                    $('[name="name"]').next().text(data.message);  
                break;

                case 'Error':
                    $('[name="name"]').parent().addClass('has-error');
                    $('[name="name"]').next().text(data.message);  
                break;
            }

			if(data.button == 1){
				$('#disease_btn').attr('disabled',true);
			}
			else{
				$('#disease_btn').attr('disabled',false);
			} 
		},
		error: function (jqXHR, textStatus, errorThrown){
			swal({
				title: "Error",
				text: "An error has occurred!",
				type: "error"
			});
		}
	});
}

function save_disease(save_method){
	$('.form-group').removeClass('has-error');
    $('.form-group').removeClass('has-warning');
    $('.form-group').removeClass('has-success');
    $('.input-feedback').empty(); 

	var url;

	if(save_method == 'create'){
		url = "http://localhost/hhrr-master/index.php/disease/create_disease";
	}
	else{
		url = "http://localhost/hhrr-master/index.php/disease/update_disease";
	}

	$.ajax({
		url : url,
		type: "POST",
		data: $('#disease_form').serialize(),
		dataType: "JSON",
		success: function (data){
			if(data.status){
				if(save_method == 'create'){
					message = "Disease created!";
					$('#disease_form')[0].reset();
				}
				else{
					message = "Disease updated!";
				}

				swal({
					title: "Success",
					text: message,
					type: "success"
				}); 
				reload_diseases();
			}
			else{
				if(data.inputerror)
                {
                    for(var i = 0; i < data.inputerror.length; i++){
                        $('[name="'+data.inputerror[i]+'"]').parent().addClass('has-error');
                        $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]);
                    }
                }
			}
		},
		error: function (jqXHR, textStatus, errorThrown){
			swal({
				title: "Error",
				text: "An error has occurred!",
				type: "error"
			});
		}
	});
}

function activate_disease(id){  
	swal({
		title: "Warning",
		text: "Do you want to activate this disease?",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#f8ac59",
		confirmButtonText: "Sí",
		cancelButtonText: "No"
	}).then(function (){
		$.ajax({
			url : "http://localhost/hhrr-master/index.php/disease/activate_disease/" + id,
			type: "POST",
			dataType: "JSON",
			success: function (data){
				reload_diseases();
			},
			error: function (jqXHR, textStatus, errorThrown){
				swal({
					title: "Error",
					text: "An error has occurred!",
					type: "error"
				});
			}
		});
		swal("Success", "Disease activated!", "success");
	}, function (dismiss){
        // dismiss can be 'cancel', 'overlay',
        // 'close', and 'timer'
        if(dismiss === 'cancel'){
          	swal("Notice", "Activation canceled!", "info");
        }
    })
}

function deactivate_disease(id){  
	swal({
		title: "Warning",
		text: "Do you want to deactivate this disease?",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#f8ac59",
		confirmButtonText: "Yes",
		cancelButtonText: "No"
	}).then(function (){
		$.ajax({
			url : "http://localhost/hhrr-master/index.php/disease/deactivate_disease/" + id,
			type: "POST",
			dataType: "JSON",
			success: function (data){
				reload_diseases();
			},
			error: function (jqXHR, textStatus, errorThrown){
				swal({
					title: "Error",
					text: "An error has occurred!",
					type: "error"
				});
			}
		});
		swal("Success", "Disease deactivated!", "success");
	}, function (dismiss){
        // dismiss can be 'cancel', 'overlay',
        // 'close', and 'timer'
        if(dismiss === 'cancel'){
          	swal("Notice", "Deactivation canceled!", "info");
        }
    })
}

function reload_diseases(){
	active_diseases.ajax.reload(null,false);
	inactive_diseases.ajax.reload(null,false);
}