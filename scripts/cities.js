var save_method;
var active_cities;
var inactive_cities;

$(document).ready(function (){
	active_cities = $('#active_cities').DataTable({ 
		"processing": true,
		"serverSide": true,
		"order": [],

		"ajax":{
			"url": "http://localhost/hhrr-master/index.php/city/list_active_cities",
			"type": "POST"
		},

		"columnDefs": [
		{ 
			"targets": [ -1 ],
			"orderable": false,
		},
		],
	});

	inactive_cities = $('#inactive_cities').DataTable({ 
		"processing": true,
		"serverSide": true,
		"order": [],

		"ajax":{
			"url": "http://localhost/hhrr-master/index.php/city/list_inactive_cities",
			"type": "POST"
		},

		"columnDefs": [
		{ 
			"targets": [ -1 ],
			"orderable": false,
		},
		],
	});
});

function create_city(){
	$('#city_form')[0].reset();
	$('.form-group').removeClass('has-error');
    $('.form-group').removeClass('has-warning');
    $('.form-group').removeClass('has-success');
	$('.input-feedback').empty();
	$('#city_modal_title').text('Create an city');
	$('#city_btn').attr('onclick','save_city("create")');
	$('#city_btn').text('Create');
}

function get_city(id){
	$('#city_form')[0].reset();
	$('.form-group').removeClass('has-error');
    $('.form-group').removeClass('has-warning');
    $('.form-group').removeClass('has-success');
	$('.input-feedback').empty();
	$('#city_modal_title').text('Update an city');
	$('#city_btn').attr('onclick','save_city("update")');
	$('#city_btn').text('Save changes');

	$.ajax({
		url : "http://localhost/hhrr-master/index.php/city/get_city/" + id,
		type: "GET",
		dataType: "JSON",
		success: function (data){
			$('[name="id"]').val(data.city_id);
			$('[name="name"]').val(data.city_name);
			$('[name="region"]').val(data.city_region);
		},
		error: function (jqXHR, textStatus, errorThrown){
			swal({
				title: "Error",
				text: "An error has occurred!",
				type: "error"
			});
		}
	});
}

function validate_name(){
	$('[name="name"]').parent().removeClass('has-error');
    $('[name="name"]').parent().removeClass('has-warning');
    $('[name="name"]').parent().removeClass('has-success');
    $('[name="name"]').next().empty();

    var name = $("#city_name").val();

	$.ajax({
		url : "http://localhost/hhrr-master/index.php/city/validate_name",
		type: 'GET',
		data: {'name':name},
		dataType: 'JSON',
		success: function (data){
			switch(data.type){
                case 'Warning':
                    $('[name="name"]').parent().addClass('has-warning');
                    $('[name="name"]').next().text(data.message);  
                break;

                case 'Notice':
                    $('[name="name"]').parent().addClass('has-success');
                    $('[name="name"]').next().text(data.message);  
                break;

                case 'Error':
                    $('[name="name"]').parent().addClass('has-error');
                    $('[name="name"]').next().text(data.message);  
                break;
            }

			if(data.button == 1){
				$('#city_btn').attr('disabled',true);
			}
			else{
				$('#city_btn').attr('disabled',false);
			} 
		},
		error: function (jqXHR, textStatus, errorThrown){
			swal({
				title: "Error",
				text: "An error has occurred!",
				type: "error"
			});
		}
	});
}

function save_city(save_method){
	$('.form-group').removeClass('has-error');
    $('.form-group').removeClass('has-warning');
    $('.form-group').removeClass('has-success');
    $('.input-feedback').empty(); 

	var url;

	if(save_method == 'create'){
		url = "http://localhost/hhrr-master/index.php/city/create_city";
	}
	else{
		url = "http://localhost/hhrr-master/index.php/city/update_city";
	}

	$.ajax({
		url : url,
		type: "POST",
		data: $('#city_form').serialize(),
		dataType: "JSON",
		success: function (data){
			if(data.status){
				if(save_method == 'create'){
					message = "City created!";
					$('#city_form')[0].reset();
				}
				else{
					message = "City updated!";
				}

				swal({
					title: "Success",
					text: message,
					type: "success"
				}); 
				reload_cities();
			}
			else{
				if(data.inputerror)
                {
                    for(var i = 0; i < data.inputerror.length; i++){
                        $('[name="'+data.inputerror[i]+'"]').parent().addClass('has-error');
                        $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]);
                    }
                }
			}
		},
		error: function (jqXHR, textStatus, errorThrown){
			swal({
				title: "Error",
				text: "An error has occurred!",
				type: "error"
			});
		}
	});
}

function activate_city(id){  
	swal({
		title: "Warning",
		text: "Do you want to activate this city?",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#f8ac59",
		confirmButtonText: "Sí",
		cancelButtonText: "No"
	}).then(function (){
		$.ajax({
			url : "http://localhost/hhrr-master/index.php/city/activate_city/" + id,
			type: "POST",
			dataType: "JSON",
			success: function (data){
				reload_cities();
			},
			error: function (jqXHR, textStatus, errorThrown){
				swal({
					title: "Error",
					text: "An error has occurred!",
					type: "error"
				});
			}
		});
		swal("Success", "City activated!", "success");
	}, function (dismiss){
        // dismiss can be 'cancel', 'overlay',
        // 'close', and 'timer'
        if(dismiss === 'cancel'){
          	swal("Notice", "Activation canceled!", "info");
        }
    })
}

function deactivate_city(id){  
	swal({
		title: "Warning",
		text: "Do you want to deactivate this city?",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#f8ac59",
		confirmButtonText: "Yes",
		cancelButtonText: "No"
	}).then(function (){
		$.ajax({
			url : "http://localhost/hhrr-master/index.php/city/deactivate_city/" + id,
			type: "POST",
			dataType: "JSON",
			success: function (data){
				reload_cities();
			},
			error: function (jqXHR, textStatus, errorThrown){
				swal({
					title: "Error",
					text: "An error has occurred!",
					type: "error"
				});
			}
		});
		swal("Success", "City deactivated!", "success");
	}, function (dismiss){
        // dismiss can be 'cancel', 'overlay',
        // 'close', and 'timer'
        if(dismiss === 'cancel'){
          	swal("Notice", "Deactivation canceled!", "info");
        }
    })
}

function reload_cities(){
	active_cities.ajax.reload(null,false);
	inactive_cities.ajax.reload(null,false);
}