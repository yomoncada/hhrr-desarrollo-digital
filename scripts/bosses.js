var save_method;
var active_bosses;
var inactive_bosses;

$(document).ready(function (){
	active_bosses = $('#active_bosses').DataTable({ 
		"processing": true,
		"serverSide": true,
		"order": [],

		"ajax":{
			"url": "http://localhost/hhrr-master/index.php/boss/list_active_bosses",
			"type": "POST"
		},

		"columnDefs": [
		{ 
			"targets": [ -1 ],
			"orderable": false,
		},
		],
	});

	inactive_bosses = $('#inactive_bosses').DataTable({ 
		"processing": true,
		"serverSide": true,
		"order": [],

		"ajax":{
			"url": "http://localhost/hhrr-master/index.php/boss/list_inactive_bosses",
			"type": "POST"
		},

		"columnDefs": [
		{ 
			"targets": [ -1 ],
			"orderable": false,
		},
		],
	});
});

function create_boss(){
	$('#boss_form')[0].reset();
	$('.form-group').removeClass('has-error');
    $('.form-group').removeClass('has-warning');
    $('.form-group').removeClass('has-success');
	$('.input-feedback').empty();
	$('#boss_modal_title').text('Create an boss');
	$('#boss_btn').attr('onclick','save_boss("create")');
	$('#boss_btn').text('Create');
}

function get_boss(id){
	$('#boss_form')[0].reset();
	$('.form-group').removeClass('has-error');
    $('.form-group').removeClass('has-warning');
    $('.form-group').removeClass('has-success');
	$('.input-feedback').empty();
	$('#boss_modal_title').text('Update an boss');
	$('#boss_btn').attr('onclick','save_boss("update")');
	$('#boss_btn').text('Save changes');

	$.ajax({
		url : "http://localhost/hhrr-master/index.php/boss/get_boss/" + id,
		type: "GET",
		dataType: "JSON",
		success: function (data){
			$('[name="id"]').val(data.boss_id);
			$('[name="employee"]').val(data.boss_employee);
			$('[name="area"]').val(data.boss_area);
		},
		error: function (jqXHR, textStatus, errorThrown){
			swal({
				title: "Error",
				text: "An error has occurred!",
				type: "error"
			});
		}
	});
}

function validate_employee(){
	$('[name="employee"]').parent().removeClass('has-error');
    $('[name="employee"]').parent().removeClass('has-warning');
    $('[name="employee"]').parent().removeClass('has-success');
    $('[name="employee"]').next().empty();

    var employee = $('[name="employee"]').val();

	$.ajax({
		url : "http://localhost/hhrr-master/index.php/boss/validate_employee",
		type: 'GET',
		data: {'employee':employee},
		dataType: 'JSON',
		success: function (data){
			switch(data.type){
                case 'Warning':
                    $('[name="employee"]').parent().addClass('has-warning');
                    $('[name="employee"]').next().text(data.message);  
                break;

                case 'Notice':
                    $('[name="employee"]').parent().addClass('has-success');
                    $('[name="employee"]').next().text(data.message);  
                break;

                case 'Error':
                    $('[name="employee"]').parent().addClass('has-error');
                    $('[name="employee"]').next().text(data.message);  
                break;
            }

			if(data.button == 1){
				$('#boss_btn').attr('disabled',true);
			}
			else{
				$('#boss_btn').attr('disabled',false);
			} 
		},
		error: function (jqXHR, textStatus, errorThrown){
			swal({
				title: "Error",
				text: "An error has occurred!",
				type: "error"
			});
		}
	});
}

function save_boss(save_method){
	$('.form-group').removeClass('has-error');
    $('.form-group').removeClass('has-warning');
    $('.form-group').removeClass('has-success');
    $('.input-feedback').empty(); 

	var url;

	if(save_method == 'create'){
		url = "http://localhost/hhrr-master/index.php/boss/create_boss";
	}
	else{
		url = "http://localhost/hhrr-master/index.php/boss/update_boss";
	}

	$.ajax({
		url : url,
		type: "POST",
		data: $('#boss_form').serialize(),
		dataType: "JSON",
		success: function (data){
			if(data.status){
				if(save_method == 'create'){
					message = "Boss created!";
					$('#boss_form')[0].reset();
				}
				else{
					message = "Boss updated!";
				}

				swal({
					title: "Success",
					text: message,
					type: "success"
				}); 
				reload_bosses();
			}
			else{
				if(data.inputerror)
                {
                    for(var i = 0; i < data.inputerror.length; i++){
                        $('[name="'+data.inputerror[i]+'"]').parent().addClass('has-error');
                        $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]);
                    }
                }
			}
		},
		error: function (jqXHR, textStatus, errorThrown){
			swal({
				title: "Error",
				text: "An error has occurred!",
				type: "error"
			});
		}
	});
}

function activate_boss(id){  
	swal({
		title: "Warning",
		text: "Do you want to activate this boss?",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#f8ac59",
		confirmButtonText: "Sí",
		cancelButtonText: "No"
	}).then(function (){
		$.ajax({
			url : "http://localhost/hhrr-master/index.php/boss/activate_boss/" + id,
			type: "POST",
			dataType: "JSON",
			success: function (data){
				reload_bosses();
			},
			error: function (jqXHR, textStatus, errorThrown){
				swal({
					title: "Error",
					text: "An error has occurred!",
					type: "error"
				});
			}
		});
		swal("Success", "Boss activated!", "success");
	}, function (dismiss){
        // dismiss can be 'cancel', 'overlay',
        // 'close', and 'timer'
        if(dismiss === 'cancel'){
          	swal("Notice", "Activation canceled!", "info");
        }
    })
}

function deactivate_boss(id){  
	swal({
		title: "Warning",
		text: "Do you want to deactivate this boss?",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#f8ac59",
		confirmButtonText: "Yes",
		cancelButtonText: "No"
	}).then(function (){
		$.ajax({
			url : "http://localhost/hhrr-master/index.php/boss/deactivate_boss/" + id,
			type: "POST",
			dataType: "JSON",
			success: function (data){
				reload_bosses();
			},
			error: function (jqXHR, textStatus, errorThrown){
				swal({
					title: "Error",
					text: "An error has occurred!",
					type: "error"
				});
			}
		});
		swal("Success", "Boss deactivated!", "success");
	}, function (dismiss){
        // dismiss can be 'cancel', 'overlay',
        // 'close', and 'timer'
        if(dismiss === 'cancel'){
          	swal("Notice", "Deactivation canceled!", "info");
        }
    })
}

function reload_bosses(){
	active_bosses.ajax.reload(null,false);
	inactive_bosses.ajax.reload(null,false);
}