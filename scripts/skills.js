var save_method;
var active_skills;
var inactive_skills;

$(document).ready(function (){
	active_skills = $('#active_skills').DataTable({ 
		"processing": true,
		"serverSide": true,
		"order": [],

		"ajax":{
			"url": "http://localhost/hhrr-master/index.php/skill/list_active_skills",
			"type": "POST"
		},

		"columnDefs": [
		{ 
			"targets": [ -1 ],
			"orderable": false,
		},
		],
	});

	inactive_skills = $('#inactive_skills').DataTable({ 
		"processing": true,
		"serverSide": true,
		"order": [],

		"ajax":{
			"url": "http://localhost/hhrr-master/index.php/skill/list_inactive_skills",
			"type": "POST"
		},

		"columnDefs": [
		{ 
			"targets": [ -1 ],
			"orderable": false,
		},
		],
	});
});

function create_skill(){
	$('#skill_form')[0].reset();
	$('.form-group').removeClass('has-error');
    $('.form-group').removeClass('has-warning');
    $('.form-group').removeClass('has-success');
	$('.input-feedback').empty();
	$('#skill_modal_title').text('Create an skill');
	$('#skill_btn').attr('onclick','save_skill("create")');
	$('#skill_btn').text('Create');
}

function get_skill(id){
	$('#skill_form')[0].reset();
	$('.form-group').removeClass('has-error');
    $('.form-group').removeClass('has-warning');
    $('.form-group').removeClass('has-success');
	$('.input-feedback').empty();
	$('#skill_modal_title').text('Update an skill');
	$('#skill_btn').attr('onclick','save_skill("update")');
	$('#skill_btn').text('Save changes');

	$.ajax({
		url : "http://localhost/hhrr-master/index.php/skill/get_skill/" + id,
		type: "GET",
		dataType: "JSON",
		success: function (data){
			$('[name="id"]').val(data.skill_id);
			$('[name="name"]').val(data.skill_name);
			$('[name="description"]').val(data.skill_description);
		},
		error: function (jqXHR, textStatus, errorThrown){
			swal({
				title: "Error",
				text: "An error has occurred!",
				type: "error"
			});
		}
	});
}

function validate_name(){
	$('[name="name"]').parent().removeClass('has-error');
    $('[name="name"]').parent().removeClass('has-warning');
    $('[name="name"]').parent().removeClass('has-success');
    $('[name="name"]').next().empty();

    var name = $("#skill_name").val();

	$.ajax({
		url : "http://localhost/hhrr-master/index.php/skill/validate_name",
		type: 'GET',
		data: {'name':name},
		dataType: 'JSON',
		success: function (data){
			switch(data.type){
                case 'Warning':
                    $('[name="name"]').parent().addClass('has-warning');
                    $('[name="name"]').next().text(data.message);  
                break;

                case 'Notice':
                    $('[name="name"]').parent().addClass('has-success');
                    $('[name="name"]').next().text(data.message);  
                break;

                case 'Error':
                    $('[name="name"]').parent().addClass('has-error');
                    $('[name="name"]').next().text(data.message);  
                break;
            }

			if(data.button == 1){
				$('#skill_btn').attr('disabled',true);
			}
			else{
				$('#skill_btn').attr('disabled',false);
			} 
		},
		error: function (jqXHR, textStatus, errorThrown){
			swal({
				title: "Error",
				text: "An error has occurred!",
				type: "error"
			});
		}
	});
}

function save_skill(save_method){
	$('.form-group').removeClass('has-error');
    $('.form-group').removeClass('has-warning');
    $('.form-group').removeClass('has-success');
    $('.input-feedback').empty(); 

	var url;

	if(save_method == 'create'){
		url = "http://localhost/hhrr-master/index.php/skill/create_skill";
	}
	else{
		url = "http://localhost/hhrr-master/index.php/skill/update_skill";
	}

	$.ajax({
		url : url,
		type: "POST",
		data: $('#skill_form').serialize(),
		dataType: "JSON",
		success: function (data){
			if(data.status){
				if(save_method == 'create'){
					message = "Skill created!";
					$('#skill_form')[0].reset();
				}
				else{
					message = "Skill updated!";
				}

				swal({
					title: "Success",
					text: message,
					type: "success"
				}); 
				reload_skills();
			}
			else{
				if(data.inputerror)
                {
                    for(var i = 0; i < data.inputerror.length; i++){
                        $('[name="'+data.inputerror[i]+'"]').parent().addClass('has-error');
                        $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]);
                    }
                }
			}
		},
		error: function (jqXHR, textStatus, errorThrown){
			swal({
				title: "Error",
				text: "An error has occurred!",
				type: "error"
			});
		}
	});
}

function activate_skill(id){  
	swal({
		title: "Warning",
		text: "Do you want to activate this skill?",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#f8ac59",
		confirmButtonText: "Sí",
		cancelButtonText: "No"
	}).then(function (){
		$.ajax({
			url : "http://localhost/hhrr-master/index.php/skill/activate_skill/" + id,
			type: "POST",
			dataType: "JSON",
			success: function (data){
				reload_skills();
			},
			error: function (jqXHR, textStatus, errorThrown){
				swal({
					title: "Error",
					text: "An error has occurred!",
					type: "error"
				});
			}
		});
		swal("Success", "Skill activated!", "success");
	}, function (dismiss){
        // dismiss can be 'cancel', 'overlay',
        // 'close', and 'timer'
        if(dismiss === 'cancel'){
          	swal("Notice", "Activation canceled!", "info");
        }
    })
}

function deactivate_skill(id){  
	swal({
		title: "Warning",
		text: "Do you want to deactivate this skill?",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#f8ac59",
		confirmButtonText: "Yes",
		cancelButtonText: "No"
	}).then(function (){
		$.ajax({
			url : "http://localhost/hhrr-master/index.php/skill/deactivate_skill/" + id,
			type: "POST",
			dataType: "JSON",
			success: function (data){
				reload_skills();
			},
			error: function (jqXHR, textStatus, errorThrown){
				swal({
					title: "Error",
					text: "An error has occurred!",
					type: "error"
				});
			}
		});
		swal("Success", "Skill deactivated!", "success");
	}, function (dismiss){
        // dismiss can be 'cancel', 'overlay',
        // 'close', and 'timer'
        if(dismiss === 'cancel'){
          	swal("Notice", "Deactivation canceled!", "info");
        }
    })
}

function reload_skills(){
	active_skills.ajax.reload(null,false);
	inactive_skills.ajax.reload(null,false);
}