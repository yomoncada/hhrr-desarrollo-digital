var save_method;
var active_employees;
var inactive_employees;
var active_skills;
var inactive_skills;
var active_works;
var inactive_works;
var active_allergies;
var inactive_allergies;
var active_diseases;
var inactive_diseases;
var active_emails;
var inactive_emails;

$(document).ready(function (){
	active_employees = $('#active_employees').DataTable({ 
		"processing": true,
		"serverSide": true,
		"order": [],

		"ajax":{
			"url": "http://localhost/hhrr-master/index.php/employee/list_active_employees",
			"type": "POST"
		},

		"columnDefs": [
		{ 
			"targets": [ -1 ],
			"orderable": false,
		},
		],
	});

	inactive_employees = $('#inactive_employees').DataTable({ 
		"processing": true,
		"serverSide": true,
		"order": [],

		"ajax":{
			"url": "http://localhost/hhrr-master/index.php/employee/list_inactive_employees",
			"type": "POST"
		},

		"columnDefs": [
		{ 
			"targets": [ -1 ],
			"orderable": false,
		},
		],
	});

	active_skills = $('#active_skills').DataTable({ 
		"processing": true,
		"serverSide": true,
		"order": [],

		"ajax":{
			"url": "http://localhost/hhrr-master/index.php/employee/list_active_skills",
			"type": "POST"
		},

		"columnDefs": [
		{ 
			"targets": [ -1 ],
			"orderable": false,
		},
		],
	});

	inactive_skills = $('#inactive_skills').DataTable({ 
		"processing": true,
		"serverSide": true,
		"order": [],

		"ajax":{
			"url": "http://localhost/hhrr-master/index.php/employee/list_inactive_skills",
			"type": "POST"
		},

		"columnDefs": [
		{ 
			"targets": [ -1 ],
			"orderable": false,
		},
		],
	});

	active_works = $('#active_works').DataTable({ 
		"processing": true,
		"serverSide": true,
		"order": [],

		"ajax":{
			"url": "http://localhost/hhrr-master/index.php/employee/list_active_works",
			"type": "POST"
		},

		"columnDefs": [
		{ 
			"targets": [ -1 ],
			"orderable": false,
		},
		],
	});

	inactive_works = $('#inactive_works').DataTable({ 
		"processing": true,
		"serverSide": true,
		"order": [],

		"ajax":{
			"url": "http://localhost/hhrr-master/index.php/employee/list_inactive_works",
			"type": "POST"
		},

		"columnDefs": [
		{ 
			"targets": [ -1 ],
			"orderable": false,
		},
		],
	});

	active_allergies = $('#active_allergies').DataTable({ 
		"processing": true,
		"serverSide": true,
		"order": [],

		"ajax":{
			"url": "http://localhost/hhrr-master/index.php/employee/list_active_allergies",
			"type": "POST"
		},

		"columnDefs": [
		{ 
			"targets": [ -1 ],
			"orderable": false,
		},
		],
	});

	inactive_allergies = $('#inactive_allergies').DataTable({ 
		"processing": true,
		"serverSide": true,
		"order": [],

		"ajax":{
			"url": "http://localhost/hhrr-master/index.php/employee/list_inactive_allergies",
			"type": "POST"
		},

		"columnDefs": [
		{ 
			"targets": [ -1 ],
			"orderable": false,
		},
		],
	});

	active_diseases = $('#active_diseases').DataTable({ 
		"processing": true,
		"serverSide": true,
		"order": [],

		"ajax":{
			"url": "http://localhost/hhrr-master/index.php/employee/list_active_diseases",
			"type": "POST"
		},

		"columnDefs": [
		{ 
			"targets": [ -1 ],
			"orderable": false,
		},
		],
	});

	inactive_diseases = $('#inactive_diseases').DataTable({ 
		"processing": true,
		"serverSide": true,
		"order": [],

		"ajax":{
			"url": "http://localhost/hhrr-master/index.php/employee/list_inactive_diseases",
			"type": "POST"
		},

		"columnDefs": [
		{ 
			"targets": [ -1 ],
			"orderable": false,
		},
		],
	});

	active_emails = $('#active_emails').DataTable({ 
		"processing": true,
		"serverSide": true,
		"order": [],

		"ajax":{
			"url": "http://localhost/hhrr-master/index.php/employee/list_active_emails",
			"type": "POST"
		},

		"columnDefs": [
		{ 
			"targets": [ -1 ],
			"orderable": false,
		},
		],
	});

	inactive_emails = $('#inactive_emails').DataTable({ 
		"processing": true,
		"serverSide": true,
		"order": [],

		"ajax":{
			"url": "http://localhost/hhrr-master/index.php/employee/list_inactive_emails",
			"type": "POST"
		},

		"columnDefs": [
		{ 
			"targets": [ -1 ],
			"orderable": false,
		},
		],
	});
});

function get_regions(){
	var country_id = $('[name="country"]').val();
  	var html_select;

	if(!country_id){
  		html_select =='<option value="0">Choose an option</option>'
        $('#regions_select').html(html_select);
    }
	$.ajax({
		url : "http://localhost/hhrr-master/index.php/employee/get_regions/" + country_id,
		type: "GET",
		dataType: "JSON",
		success: function (data){
  		 	var html_select = '';
  		 		html_select +='<option value="0">Choose an option</option>'
	            for (var i=0; i<data.length; ++i){
	              html_select +='<option value="'+data[i].region_id+'">'+data[i].region_name+'</option>';
	            }
              $('#regions_select').html(html_select);
		},
		error: function (jqXHR, textStatus, errorThrown){
			swal({
				title: "Error",
				text: "An error has occurred!",
				type: "error"
			});
		}
	});
}

function get_cities(){
	var region_id = $('[name="region"]').val();
  	var html_select;

	if(!region_id){
  		html_select =='<option value="0">Choose an option</option>'
        $('#cities_select').html(html_select);
    }
	$.ajax({
		url : "http://localhost/hhrr-master/index.php/employee/get_cities/" + region_id,
		type: "GET",
		dataType: "JSON",
		success: function (data){
  		 		html_select +='<option value="0">Choose an option</option>'
	            for (var i=0; i<data.length; ++i){
	              html_select +='<option value="'+data[i].city_id+'">'+data[i].city_name+'</option>';
	            }
              $('#cities_select').html(html_select);
		},
		error: function (jqXHR, textStatus, errorThrown){
			swal({
				title: "Error",
				text: "An error has occurred!",
				type: "error"
			});
		}
	});
}

function create_employee(){
	$('#employee_form')[0].reset();
	$('.form-group').removeClass('has-error');
    $('.form-group').removeClass('has-warning');
    $('.form-group').removeClass('has-success');
	$('.input-feedback').empty();
	$('#employee_modal_title').text('Create an employee');
	$('#employee_id').removeAttr('readonly');
	$('#employee_btn').attr('onclick','save_employee("create")');
	$('#employee_btn').text('Create');
}

function get_employee(id){
	get_regions();
	get_cities();

	$('#employee_form')[0].reset();
	$('.form-group').removeClass('has-error');
    $('.form-group').removeClass('has-warning');
    $('.form-group').removeClass('has-success');
	$('.input-feedback').empty();
	$('#employee_modal_title').text('Update an employee');
	$('#employee_id').attr('readonly','true');
	$('#employee_btn').attr('onclick','save_employee("update")');
	$('#employee_btn').text('Save changes');

	$.ajax({
		url : "http://localhost/hhrr-master/index.php/employee/get_employee/" + id,
		type: "GET",
		dataType: "JSON",
		success: function (data){
			$('[name="id"]').val(data.employee_id);
			$('[name="firstname"]').val(data.employee_firstname);
			$('[name="secondname"]').val(data.employee_secondname);
			$('[name="firstlastname"]').val(data.employee_firstlastname);
			$('[name="secondlastname"]').val(data.employee_secondlastname);
			$('[name="birth"]').val(data.employee_birth);
			$('[name="country"]').val(data.employee_country);
			$('[name="region"]').val(data.employee_region);
			$('[name="city"]').val(data.employee_city);
			$('[name="address"]').val(data.employee_address);
			$('[name="cellphone"]').val(data.employee_cellphone);
			$('[name="phone"]').val(data.employee_phone);
			$('[name="whatsapp"]').val(data.employee_whatsapp);
			$('[name="facebook"]').val(data.employee_facebook);
			$('[name="gplus"]').val(data.employee_gplus);
			$('[name="linkedin"]').val(data.employee_linkedin);
			$('[name="file"]').val(data.employee_file);
			$('[name="rol"]').val(data.employee_rol);
			$('[name="area"]').val(data.employee_area);
			$('[name="position"]').val(data.employee_position);
			$('[name="boss"]').val(data.employee_boss);
			$('[name="salary"]').val(data.employee_salary);
			$('[name="username"]').val(data.employee_username);
			$('[name="password"]').val(data.employee_password);
			$('[name="repeat_password"]').val(data.employee_password);
			$('[name="secques"]').val(data.employee_secques);
			$('[name="secans"]').val(data.employee_secans);
		},
		error: function (jqXHR, textStatus, errorThrown){
			swal({
				title: "Error",
				text: "An error has occurred!",
				type: "error"
			});
		}
	});
}

function validate_id(){
	$('[name="id"]').parent().removeClass('has-error');
    $('[name="id"]').parent().removeClass('has-warning');
    $('[name="id"]').parent().removeClass('has-success');
    $('[name="id"]').next().empty();

    var id = $("#employee_id").val();

	$.ajax({
		url : "http://localhost/hhrr-master/index.php/employee/validate_id",
		type: 'GET',
		data: {'id':id},
		dataType: 'JSON',
		success: function (data){
			switch(data.type){
                case 'Warning':
                    $('[name="id"]').parent().addClass('has-warning');
                    $('[name="id"]').next().text(data.message);  
                break;

                case 'Notice':
                    $('[name="id"]').parent().addClass('has-success');
                    $('[name="id"]').next().text(data.message);  
                break;

                case 'Error':
                    $('[name="id"]').parent().addClass('has-error');
                    $('[name="id"]').next().text(data.message);  
                break;
            }

			if(data.button == 1){
				$('#employee_btn').attr('disabled',true);
			}
			else{
				$('#employee_btn').attr('disabled',false);
			} 
		},
		error: function (jqXHR, textStatus, errorThrown){
			swal({
				title: "Error",
				text: "An error has occurred!",
				type: "error"
			});
		}
	});
}

function validate_username(){
	$('[name="username"]').parent().removeClass('has-error');
    $('[name="username"]').parent().removeClass('has-warning');
    $('[name="username"]').parent().removeClass('has-success');
    $('[name="username"]').next().empty();

    var username = $("#employee_username").val();

	$.ajax({
		url : "http://localhost/hhrr-master/index.php/employee/validate_username",
		type: 'GET',
		data: {'username':username},
		dataType: 'JSON',
		success: function (data){
			switch(data.type){
                case 'Warning':
                    $('[name="username"]').parent().addClass('has-warning');
                    $('[name="username"]').next().text(data.message);  
                break;

                case 'Notice':
                    $('[name="username"]').parent().addClass('has-success');
                    $('[name="username"]').next().text(data.message);  
                break;

                case 'Error':
                    $('[name="username"]').parent().addClass('has-error');
                    $('[name="username"]').next().text(data.message);  
                break;
            }

			if(data.button == 1){
				$('#employee_btn').attr('disabled',true);
			}
			else{
				$('#employee_btn').attr('disabled',false);
			} 
		},
		error: function (jqXHR, textStatus, errorThrown){
			swal({
				title: "Error",
				text: "An error has occurred!",
				type: "error"
			});
		}
	});
}

function compare_password(){
	$('[name="password"]').parent().removeClass('has-error');
    $('[name="password"]').parent().removeClass('has-warning');
    $('[name="password"]').parent().removeClass('has-success');
    $('[name="password"]').next().empty();

    var password = $('[name="password"]').val();
    var repeat_password = $('[name="repeat_password"]').val();

	$.ajax({
		url : "http://localhost/hhrr-master/index.php/employee/compare_password",
		type: 'GET',
		data: {'password':password, 'repeat_password':repeat_password},
		dataType: 'JSON',
		success: function (data){
			switch(data.type){
                case 'Warning':
                    $('[name="repeat_password"]').parent().addClass('has-warning');
                    $('[name="repeat_password"]').next().text(data.message);  
                break;

                case 'Notice':
                    $('[name="repeat_password"]').parent().addClass('has-success');
                    $('[name="repeat_password"]').next().text(data.message);  
                break;
            }

			if(data.button == 1){
				$('#employee_btn').attr('disabled',true);
			}
			else{
				$('#employee_btn').attr('disabled',false);
			} 
		},
		error: function (jqXHR, textStatus, errorThrown){
			swal({
				title: "Error",
				text: "An error has occurred!",
				type: "error"
			});
		}
	});
}

function save_employee(save_method){
	$('.form-group').removeClass('has-error');
    $('.form-group').removeClass('has-warning');
    $('.form-group').removeClass('has-success');
    $('.input-feedback').empty(); 

	var url;

	if(save_method == 'create'){
		url = "http://localhost/hhrr-master/index.php/employee/create_employee";
	}
	else{
		url = "http://localhost/hhrr-master/index.php/employee/update_employee";
	}

	$.ajax({
		url : url,
		type: "POST",
		data: $('#employee_form').serialize(),
		dataType: "JSON",
		success: function (data){
			if(data.status){
				if(save_method == 'create'){
					message = "Employee created!";
					$('#employee_form')[0].reset();
				}
				else{
					message = "Employee updated!";
				}

				swal({
					title: "Success",
					text: message,
					type: "success"
				}); 
				reload_employees();
			}
			else{
				if(data.inputerror)
                {
                    for(var i = 0; i < data.inputerror.length; i++){
                        $('[name="'+data.inputerror[i]+'"]').parent().addClass('has-error');
                        $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]);
                    }
                }
			}
		},
		error: function (jqXHR, textStatus, errorThrown){
			swal({
				title: "Error",
				text: "An error has occurred!",
				type: "error"
			});
		}
	});
}

function activate_employee(id){  
	swal({
		title: "Warning",
		text: "Do you want to activate this employee?",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#f8ac59",
		confirmButtonText: "Sí",
		cancelButtonText: "No"
	}).then(function (){
		$.ajax({
			url : "http://localhost/hhrr-master/index.php/employee/activate_employee/" + id,
			type: "POST",
			dataType: "JSON",
			success: function (data){
				reload_employees();
			},
			error: function (jqXHR, textStatus, errorThrown){
				swal({
					title: "Error",
					text: "An error has occurred!",
					type: "error"
				});
			}
		});
		swal("Success", "Employeed activated!", "success");
	}, function (dismiss){
        // dismiss can be 'cancel', 'overlay',
        // 'close', and 'timer'
        if(dismiss === 'cancel'){
          	swal("Notice", "Activation canceled!", "info");
        }
    })
}

function deactivate_employee(id){  
	swal({
		title: "Warning",
		text: "Do you want to deactivate this employee?",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#f8ac59",
		confirmButtonText: "Yes",
		cancelButtonText: "No"
	}).then(function (){
		$.ajax({
			url : "http://localhost/hhrr-master/index.php/employee/deactivate_employee/" + id,
			type: "POST",
			dataType: "JSON",
			success: function (data){
				reload_employees();
			},
			error: function (jqXHR, textStatus, errorThrown){
				swal({
					title: "Error",
					text: "An error has occurred!",
					type: "error"
				});
			}
		});
		swal("Success", "Employeed deactivated!", "success");
	}, function (dismiss){
        // dismiss can be 'cancel', 'overlay',
        // 'close', and 'timer'
        if(dismiss === 'cancel'){
          	swal("Notice", "Deactivation canceled!", "info");
        }
    })
}

function reload_employees(){
	active_employees.ajax.reload(null,false);
	inactive_employees.ajax.reload(null,false);
}

function create_empski(){
	$('#empski_form')[0].reset();
	$('.form-group').removeClass('has-error');
    $('.form-group').removeClass('has-warning');
    $('.form-group').removeClass('has-success');
	$('.input-feedback').empty();
	$('#empski_modal_title').text('Create an employee skill');
	$('#empski_id').removeAttr('readonly');
	$('#empski_btn').attr('onclick','save_empski("create")');
	$('#empski_btn').text('Create');
}

function get_empski(id){
	$('#empski_form')[0].reset();
	$('.form-group').removeClass('has-error');
    $('.form-group').removeClass('has-warning');
    $('.form-group').removeClass('has-success');
	$('.input-feedback').empty();
	$('#empski_modal_title').text('Update an empski');
	$('#empski_btn').attr('onclick','save_empski("update")');
	$('#empski_btn').text('Save changes');

	$.ajax({
		url : "http://localhost/hhrr-master/index.php/employee/get_empski/" + id,
		type: "GET",
		dataType: "JSON",
		success: function (data){
			$('[name="id"]').val(data.empski_id);
			$('[name="skill"]').val(data.empski_skill);
			$('[name="level"]').val(data.empski_level);
			$('[name="grade"]').val(data.empski_grade);
			$('[name="time"]').val(data.empski_time);
		},
		error: function (jqXHR, textStatus, errorThrown){
			swal({
				title: "Error",
				text: "An error has occurred!",
				type: "error"
			});
		}
	});
}

function save_empski(save_method){
	$('.form-group').removeClass('has-error');
    $('.form-group').removeClass('has-warning');
    $('.form-group').removeClass('has-success');
    $('.input-feedback').empty(); 

	var url;

	if(save_method == 'create'){
		url = "http://localhost/hhrr-master/index.php/employee/create_empski/" + 'empski';
	}
	else{
		url = "http://localhost/hhrr-master/index.php/employee/update_empski/" + 'empski';
	}

	$.ajax({
		url : url,
		type: "POST",
		data: $('#empski_form').serialize(),
		dataType: "JSON",
		success: function (data){
			if(data.status){
				if(save_method == 'create'){
					message = "Employee skill created!";
					$('#empski_form')[0].reset();
				}
				else{
					message = "Employee skill updated!";
				}

				swal({
					title: "Success",
					text: message,
					type: "success"
				}); 
				reload_empski();
			}
			else{
				if(data.inputerror)
                {
                    for(var i = 0; i < data.inputerror.length; i++){
                        $('[name="'+data.inputerror[i]+'"]').parent().addClass('has-error');
                        $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]);
                    }
                }
			}
		},
		error: function (jqXHR, textStatus, errorThrown){
			swal({
				title: "Error",
				text: "An error has occurred!",
				type: "error"
			});
		}
	});
}

function activate_empski(id){  
	swal({
		title: "Warning",
		text: "Do you want to activate this skill?",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#f8ac59",
		confirmButtonText: "Sí",
		cancelButtonText: "No"
	}).then(function (){
		$.ajax({
			url : "http://localhost/hhrr-master/index.php/employee/activate_empski/" + id,
			type: "POST",
			dataType: "JSON",
			success: function (data){
				reload_empski();
			},
			error: function (jqXHR, textStatus, errorThrown){
				swal({
					title: "Error",
					text: "An error has occurred!",
					type: "error"
				});
			}
		});
		swal("Success", "Skill activated!", "success");
	}, function (dismiss){
        // dismiss can be 'cancel', 'overlay',
        // 'close', and 'timer'
        if(dismiss === 'cancel'){
          	swal("Notice", "Activation canceled!", "info");
        }
    })
}

function deactivate_empski(id){  
	swal({
		title: "Warning",
		text: "Do you want to deactivate this skill?",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#f8ac59",
		confirmButtonText: "Yes",
		cancelButtonText: "No"
	}).then(function (){
		$.ajax({
			url : "http://localhost/hhrr-master/index.php/employee/deactivate_empski/" + id,
			type: "POST",
			dataType: "JSON",
			success: function (data){
				reload_empski();
			},
			error: function (jqXHR, textStatus, errorThrown){
				swal({
					title: "Error",
					text: "An error has occurred!",
					type: "error"
				});
			}
		});
		swal("Success", "Employeed deactivated!", "success");
	}, function (dismiss){
        // dismiss can be 'cancel', 'overlay',
        // 'close', and 'timer'
        if(dismiss === 'cancel'){
          	swal("Notice", "Deactivation canceled!", "info");
        }
    })
}

function reload_empski(){
	active_skills.ajax.reload(null,false);
	inactive_skills.ajax.reload(null,false);
}

function create_empwork(){
	$('#empwork_form')[0].reset();
	$('.form-group').removeClass('has-error');
    $('.form-group').removeClass('has-warning');
    $('.form-group').removeClass('has-success');
	$('.input-feedback').empty();
	$('#empwork_modal_title').text('Create an employee work');
	$('#empwork_id').removeAttr('readonly');
	$('#empwork_btn').attr('onclick','save_empwork("create")');
	$('#empwork_btn').text('Create');
}

function get_empwork(id){
	$('#empwork_form')[0].reset();
	$('.form-group').removeClass('has-error');
    $('.form-group').removeClass('has-warning');
    $('.form-group').removeClass('has-success');
	$('.input-feedback').empty();
	$('#empwork_modal_title').text('Update an employee work');
	$('#empwork_btn').attr('onclick','save_empwork("update")');
	$('#empwork_btn').text('Save changes');

	$.ajax({
		url : "http://localhost/hhrr-master/index.php/employee/get_empwork/" + id,
		type: "GET",
		dataType: "JSON",
		success: function (data){
			$('[name="id"]').val(data.empwork_id);
			$('[name="company"]').val(data.empwork_company);
			$('[name="country"]').val(data.empwork_country);
			$('[name="position"]').val(data.empwork_position);
			$('[name="time"]').val(data.empwork_time);
		},
		error: function (jqXHR, textStatus, errorThrown){
			swal({
				title: "Error",
				text: "An error has occurred!",
				type: "error"
			});
		}
	});
}

function save_empwork(save_method){
	$('.form-group').removeClass('has-error');
    $('.form-group').removeClass('has-warning');
    $('.form-group').removeClass('has-success');
    $('.input-feedback').empty(); 

	var url;

	if(save_method == 'create'){
		url = "http://localhost/hhrr-master/index.php/employee/create_empwork/" + 'empwork';
	}
	else{
		url = "http://localhost/hhrr-master/index.php/employee/update_empwork/" + 'empwork';
	}

	$.ajax({
		url : url,
		type: "POST",
		data: $('#empwork_form').serialize(),
		dataType: "JSON",
		success: function (data){
			if(data.status){
				if(save_method == 'create'){
					message = "Employee work created!";
					$('#empwork_form')[0].reset();
				}
				else{
					message = "Employee work updated!";
				}

				swal({
					title: "Success",
					text: message,
					type: "success"
				}); 
				reload_empwork();
			}
			else{
				if(data.inputerror)
                {
                    for(var i = 0; i < data.inputerror.length; i++){
                        $('[name="'+data.inputerror[i]+'"]').parent().addClass('has-error');
                        $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]);
                    }
                }
			}
		},
		error: function (jqXHR, textStatus, errorThrown){
			swal({
				title: "Error",
				text: "An error has occurred!",
				type: "error"
			});
		}
	});
}

function activate_empwork(id){  
	swal({
		title: "Warning",
		text: "Do you want to activate this work?",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#f8ac59",
		confirmButtonText: "Sí",
		cancelButtonText: "No"
	}).then(function (){
		$.ajax({
			url : "http://localhost/hhrr-master/index.php/employee/activate_empwork/" + id,
			type: "POST",
			dataType: "JSON",
			success: function (data){
				reload_empwork();
			},
			error: function (jqXHR, textStatus, errorThrown){
				swal({
					title: "Error",
					text: "An error has occurred!",
					type: "error"
				});
			}
		});
		swal("Success", "Employee work activated!", "success");
	}, function (dismiss){
        // dismiss can be 'cancel', 'overlay',
        // 'close', and 'timer'
        if(dismiss === 'cancel'){
          	swal("Notice", "Activation canceled!", "info");
        }
    })
}

function deactivate_empwork(id){  
	swal({
		title: "Warning",
		text: "Do you want to deactivate this work?",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#f8ac59",
		confirmButtonText: "Yes",
		cancelButtonText: "No"
	}).then(function (){
		$.ajax({
			url : "http://localhost/hhrr-master/index.php/employee/deactivate_empwork/" + id,
			type: "POST",
			dataType: "JSON",
			success: function (data){
				reload_empwork();
			},
			error: function (jqXHR, textStatus, errorThrown){
				swal({
					title: "Error",
					text: "An error has occurred!",
					type: "error"
				});
			}
		});
		swal("Success", "Employee work deactivated!", "success");
	}, function (dismiss){
        // dismiss can be 'cancel', 'overlay',
        // 'close', and 'timer'
        if(dismiss === 'cancel'){
          	swal("Notice", "Deactivation canceled!", "info");
        }
    })
}

function reload_empwork(){
	active_works.ajax.reload(null,false);
	inactive_works.ajax.reload(null,false);
}

function create_empalle(){
	$('#empalle_form')[0].reset();
	$('.form-group').removeClass('has-error');
    $('.form-group').removeClass('has-warning');
    $('.form-group').removeClass('has-success');
	$('.input-feedback').empty();
	$('#empalle_modal_title').text('Create an employee allergie');
	$('#empalle_id').removeAttr('readonly');
	$('#empalle_btn').attr('onclick','save_empalle("create")');
	$('#empalle_btn').text('Create');
}

function get_empalle(id){
	$('#empalle_form')[0].reset();
	$('.form-group').removeClass('has-error');
    $('.form-group').removeClass('has-warning');
    $('.form-group').removeClass('has-success');
	$('.input-feedback').empty();
	$('#empalle_modal_title').text('Update an employee allergie');
	$('#empalle_btn').attr('onclick','save_empalle("update")');
	$('#empalle_btn').text('Save changes');

	$.ajax({
		url : "http://localhost/hhrr-master/index.php/employee/get_empalle/" + id,
		type: "GET",
		dataType: "JSON",
		success: function (data){
			$('[name="id"]').val(data.empalle_id);
			$('[name="allergie"]').val(data.empalle_allergie);
		},
		error: function (jqXHR, textStatus, errorThrown){
			swal({
				title: "Error",
				text: "An error has occurred!",
				type: "error"
			});
		}
	});
}

function save_empalle(save_method){
	$('.form-group').removeClass('has-error');
    $('.form-group').removeClass('has-warning');
    $('.form-group').removeClass('has-success');
    $('.input-feedback').empty(); 

	var url;

	if(save_method == 'create'){
		url = "http://localhost/hhrr-master/index.php/employee/create_empalle/" + 'empalle';
	}
	else{
		url = "http://localhost/hhrr-master/index.php/employee/update_empalle/" + 'empalle';
	}

	$.ajax({
		url : url,
		type: "POST",
		data: $('#empalle_form').serialize(),
		dataType: "JSON",
		success: function (data){
			if(data.status){
				if(save_method == 'create'){
					message = "Employee allergie created!";
					$('#empalle_form')[0].reset();
				}
				else{
					message = "Employee allergie updated!";
				}

				swal({
					title: "Success",
					text: message,
					type: "success"
				}); 
				reload_empalle();
			}
			else{
				if(data.inputerror)
                {
                    for(var i = 0; i < data.inputerror.length; i++){
                        $('[name="'+data.inputerror[i]+'"]').parent().addClass('has-error');
                        $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]);
                    }
                }
			}
		},
		error: function (jqXHR, textStatus, errorThrown){
			swal({
				title: "Error",
				text: "An error has occurred!",
				type: "error"
			});
		}
	});
}

function activate_empalle(id){  
	swal({
		title: "Warning",
		text: "Do you want to activate this allergie?",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#f8ac59",
		confirmButtonText: "Sí",
		cancelButtonText: "No"
	}).then(function (){
		$.ajax({
			url : "http://localhost/hhrr-master/index.php/employee/activate_empalle/" + id,
			type: "POST",
			dataType: "JSON",
			success: function (data){
				reload_empalle();
			},
			error: function (jqXHR, textStatus, errorThrown){
				swal({
					title: "Error",
					text: "An error has occurred!",
					type: "error"
				});
			}
		});
		swal("Success", "Employee allergie activated!", "success");
	}, function (dismiss){
        // dismiss can be 'cancel', 'overlay',
        // 'close', and 'timer'
        if(dismiss === 'cancel'){
          	swal("Notice", "Activation canceled!", "info");
        }
    })
}

function deactivate_empalle(id){  
	swal({
		title: "Warning",
		text: "Do you want to deactivate this allergie?",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#f8ac59",
		confirmButtonText: "Yes",
		cancelButtonText: "No"
	}).then(function (){
		$.ajax({
			url : "http://localhost/hhrr-master/index.php/employee/deactivate_empalle/" + id,
			type: "POST",
			dataType: "JSON",
			success: function (data){
				reload_empalle();
			},
			error: function (jqXHR, textStatus, errorThrown){
				swal({
					title: "Error",
					text: "An error has occurred!",
					type: "error"
				});
			}
		});
		swal("Success", "Employee allergie deactivated!", "success");
	}, function (dismiss){
        // dismiss can be 'cancel', 'overlay',
        // 'close', and 'timer'
        if(dismiss === 'cancel'){
          	swal("Notice", "Deactivation canceled!", "info");
        }
    })
}

function reload_empalle(){
	active_allergies.ajax.reload(null,false);
	inactive_allergies.ajax.reload(null,false);
}

function create_empdis(){
	$('#empdis_form')[0].reset();
	$('.form-group').removeClass('has-error');
    $('.form-group').removeClass('has-warning');
    $('.form-group').removeClass('has-success');
	$('.input-feedback').empty();
	$('#empdis_modal_title').text('Create an employee disease');
	$('#empdis_id').removeAttr('readonly');
	$('#empdis_btn').attr('onclick','save_empdis("create")');
	$('#empdis_btn').text('Create');
}

function get_empdis(id){
	$('#empdis_form')[0].reset();
	$('.form-group').removeClass('has-error');
    $('.form-group').removeClass('has-warning');
    $('.form-group').removeClass('has-success');
	$('.input-feedback').empty();
	$('#empdis_modal_title').text('Update an employee disease');
	$('#empdis_btn').attr('onclick','save_empdis("update")');
	$('#empdis_btn').text('Save changes');

	$.ajax({
		url : "http://localhost/hhrr-master/index.php/employee/get_empdis/" + id,
		type: "GET",
		dataType: "JSON",
		success: function (data){
			$('[name="id"]').val(data.empdis_id);
			$('[name="disease"]').val(data.empdis_disease);
			$('[name="diagnostic"]').val(data.empdis_diagnostic);
			$('[name="date"]').val(data.empdis_date);
			$('[name="control"]').val(data.empdis_control);
		},
		error: function (jqXHR, textStatus, errorThrown){
			swal({
				title: "Error",
				text: "An error has occurred!",
				type: "error"
			});
		}
	});
}

function save_empdis(save_method){
	$('.form-group').removeClass('has-error');
    $('.form-group').removeClass('has-warning');
    $('.form-group').removeClass('has-success');
    $('.input-feedback').empty(); 

	var url;

	if(save_method == 'create'){
		url = "http://localhost/hhrr-master/index.php/employee/create_empdis/" + 'empdis';
	}
	else{
		url = "http://localhost/hhrr-master/index.php/employee/update_empdis/" + 'empdis';
	}

	$.ajax({
		url : url,
		type: "POST",
		data: $('#empdis_form').serialize(),
		dataType: "JSON",
		success: function (data){
			if(data.status){
				if(save_method == 'create'){
					message = "Employee disease created!";
					$('#empdis_form')[0].reset();
				}
				else{
					message = "Employee disease updated!";
				}

				swal({
					title: "Success",
					text: message,
					type: "success"
				}); 
				reload_empdis();
			}
			else{
				if(data.inputerror)
                {
                    for(var i = 0; i < data.inputerror.length; i++){
                        $('[name="'+data.inputerror[i]+'"]').parent().addClass('has-error');
                        $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]);
                    }
                }
			}
		},
		error: function (jqXHR, textStatus, errorThrown){
			swal({
				title: "Error",
				text: "An error has occurred!",
				type: "error"
			});
		}
	});
}

function activate_empdis(id){  
	swal({
		title: "Warning",
		text: "Do you want to activate this disease?",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#f8ac59",
		confirmButtonText: "Sí",
		cancelButtonText: "No"
	}).then(function (){
		$.ajax({
			url : "http://localhost/hhrr-master/index.php/employee/activate_empdis/" + id,
			type: "POST",
			dataType: "JSON",
			success: function (data){
				reload_empdis();
			},
			error: function (jqXHR, textStatus, errorThrown){
				swal({
					title: "Error",
					text: "An error has occurred!",
					type: "error"
				});
			}
		});
		swal("Success", "Employee disease activated!", "success");
	}, function (dismiss){
        // dismiss can be 'cancel', 'overlay',
        // 'close', and 'timer'
        if(dismiss === 'cancel'){
          	swal("Notice", "Activation canceled!", "info");
        }
    })
}

function deactivate_empdis(id){  
	swal({
		title: "Warning",
		text: "Do you want to deactivate this disease?",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#f8ac59",
		confirmButtonText: "Yes",
		cancelButtonText: "No"
	}).then(function (){
		$.ajax({
			url : "http://localhost/hhrr-master/index.php/employee/deactivate_empdis/" + id,
			type: "POST",
			dataType: "JSON",
			success: function (data){
				reload_empdis();
			},
			error: function (jqXHR, textStatus, errorThrown){
				swal({
					title: "Error",
					text: "An error has occurred!",
					type: "error"
				});
			}
		});
		swal("Success", "Employee disease deactivated!", "success");
	}, function (dismiss){
        // dismiss can be 'cancel', 'overlay',
        // 'close', and 'timer'
        if(dismiss === 'cancel'){
          	swal("Notice", "Deactivation canceled!", "info");
        }
    })
}

function reload_empdis(){
	active_diseases.ajax.reload(null,false);
	inactive_diseases.ajax.reload(null,false);
}

function create_empema(){
	$('#empema_form')[0].reset();
	$('.form-group').removeClass('has-error');
    $('.form-group').removeClass('has-warning');
    $('.form-group').removeClass('has-success');
	$('.input-feedback').empty();
	$('#empema_modal_title').text('Create an employee email');
	$('#empema_id').removeAttr('readonly');
	$('#empema_btn').attr('onclick','save_empema("create")');
	$('#empema_btn').text('Create');
}

function get_empema(id){
	$('#empema_form')[0].reset();
	$('.form-group').removeClass('has-error');
    $('.form-group').removeClass('has-warning');
    $('.form-group').removeClass('has-success');
	$('.input-feedback').empty();
	$('#empema_modal_title').text('Update an employee email');
	$('#empema_btn').attr('onclick','save_empema("update")');
	$('#empema_btn').text('Save changes');

	$.ajax({
		url : "http://localhost/hhrr-master/index.php/employee/get_empema/" + id,
		type: "GET",
		dataType: "JSON",
		success: function (data){
			$('[name="id"]').val(data.empema_id);
			$('[name="email"]').val(data.empema_email);
		},
		error: function (jqXHR, textStatus, errorThrown){
			swal({
				title: "Error",
				text: "An error has occurred!",
				type: "error"
			});
		}
	});
}

function save_empema(save_method){
	$('.form-group').removeClass('has-error');
    $('.form-group').removeClass('has-warning');
    $('.form-group').removeClass('has-success');
    $('.input-feedback').empty(); 

	var url;

	if(save_method == 'create'){
		url = "http://localhost/hhrr-master/index.php/employee/create_empema/" + 'empema';
	}
	else{
		url = "http://localhost/hhrr-master/index.php/employee/update_empema/" + 'empema';
	}

	$.ajax({
		url : url,
		type: "POST",
		data: $('#empema_form').serialize(),
		dataType: "JSON",
		success: function (data){
			if(data.status){
				if(save_method == 'create'){
					message = "Employee email created!";
					$('#empema_form')[0].reset();
				}
				else{
					message = "Employee email updated!";
				}

				swal({
					title: "Success",
					text: message,
					type: "success"
				}); 
				reload_empema();
			}
			else{
				if(data.inputerror)
                {
                    for(var i = 0; i < data.inputerror.length; i++){
                        $('[name="'+data.inputerror[i]+'"]').parent().addClass('has-error');
                        $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]);
                    }
                }
			}
		},
		error: function (jqXHR, textStatus, errorThrown){
			swal({
				title: "Error",
				text: "An error has occurred!",
				type: "error"
			});
		}
	});
}

function activate_empema(id){  
	swal({
		title: "Warning",
		text: "Do you want to activate this email?",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#f8ac59",
		confirmButtonText: "Sí",
		cancelButtonText: "No"
	}).then(function (){
		$.ajax({
			url : "http://localhost/hhrr-master/index.php/employee/activate_empema/" + id,
			type: "POST",
			dataType: "JSON",
			success: function (data){
				reload_empema();
			},
			error: function (jqXHR, textStatus, errorThrown){
				swal({
					title: "Error",
					text: "An error has occurred!",
					type: "error"
				});
			}
		});
		swal("Success", "Employee email activated!", "success");
	}, function (dismiss){
        // dismiss can be 'cancel', 'overlay',
        // 'close', and 'timer'
        if(dismiss === 'cancel'){
          	swal("Notice", "Activation canceled!", "info");
        }
    })
}

function deactivate_empema(id){  
	swal({
		title: "Warning",
		text: "Do you want to deactivate this email?",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#f8ac59",
		confirmButtonText: "Yes",
		cancelButtonText: "No"
	}).then(function (){
		$.ajax({
			url : "http://localhost/hhrr-master/index.php/employee/deactivate_empema/" + id,
			type: "POST",
			dataType: "JSON",
			success: function (data){
				reload_empema();
			},
			error: function (jqXHR, textStatus, errorThrown){
				swal({
					title: "Error",
					text: "An error has occurred!",
					type: "error"
				});
			}
		});
		swal("Success", "Employee email deactivated!", "success");
	}, function (dismiss){
        // dismiss can be 'cancel', 'overlay',
        // 'close', and 'timer'
        if(dismiss === 'cancel'){
          	swal("Notice", "Deactivation canceled!", "info");
        }
    })
}

function reload_empema(){
	active_emails.ajax.reload(null,false);
	inactive_emails.ajax.reload(null,false);
}