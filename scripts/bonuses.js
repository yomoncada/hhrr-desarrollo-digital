var save_method;
var pending_bonuses;
var processed_bonuses;

$(document).ready(function (){
	pending_bonuses = $('#pending_bonuses').DataTable({ 
		"processing": true,
		"serverSide": true,
		"order": [],

		"ajax":{
			"url": "http://localhost/hhrr-master/index.php/bonus/list_pending_bonuses",
			"type": "POST"
		},

		"columnDefs": [
		{ 
			"targets": [ -1 ],
			"orderable": false,
		},
		],
	});

	processed_bonuses = $('#processed_bonuses').DataTable({ 
		"processing": true,
		"serverSide": true,
		"order": [],

		"ajax":{
			"url": "http://localhost/hhrr-master/index.php/bonus/list_processed_bonuses",
			"type": "POST"
		},

		"columnDefs": [
		{ 
			"targets": [ -1 ],
			"orderable": false,
		},
		],
	});
});

function create_bonus(){
	$('#bonus_form')[0].reset();
	$('.form-group').removeClass('has-error');
    $('.form-group').removeClass('has-warning');
    $('.form-group').removeClass('has-success');
	$('.input-feedback').empty();
	$('#bonus_modal_title').text('Create a bonus');
	$('#bonus_btn').css('display','block');
	$('#bonus_btn').attr('onclick','save_bonus("create")');
	$('#bonus_btn').text('Create');
	$('[name="id"]').removeAttr('disabled',true);
	$('[name="employee"]').removeAttr('disabled',true);
	$('[name="value"]').removeAttr('disabled',true);
	$('[name="date"]').removeAttr('disabled',true);
}

function get_bonus(id){
	$('#bonus_form')[0].reset();
	$('.form-group').removeClass('has-error');
    $('.form-group').removeClass('has-warning');
    $('.form-group').removeClass('has-success');
	$('.input-feedback').empty();
	$('#bonus_modal_title').text('Process a bonus');
	$('#bonus_btn').css('display','block');
	$('#bonus_btn').attr('onclick','save_bonus("update")');
	$('#bonus_btn').text('Process');
	$('[name="id"]').removeAttr('disabled',true);
	$('[name="employee"]').removeAttr('disabled',true);
	$('[name="value"]').removeAttr('disabled',true);
	$('[name="date"]').removeAttr('disabled',true);

	$.ajax({
		url : "http://localhost/hhrr-master/index.php/bonus/get_bonus/" + id,
		type: "GET",
		dataType: "JSON",
		success: function (data){
			$('[name="id"]').val(data.bonus_id);
			$('[name="employee"]').val(data.bonus_employee);
			$('[name="value"]').val(data.bonus_value);
			$('[name="date"]').val(data.bonus_date);
		},
		error: function (jqXHR, textStatus, errorThrown){
			swal({
				title: "Error",
				text: "An error has occurred!",
				type: "error"
			});
		}
	});
}

function get_bonus_unmodifiable(id){
	get_bonus(id);
	$('#bonus_modal_title').text('Read a bonus');
	$('#bonus_btn').css('display','none');

	$.ajax({
		url : "http://localhost/hhrr-master/index.php/bonus/get_bonus/" + id,
		type: "GET",
		dataType: "JSON",
		success: function (data){
			$('[name="id"]').attr('disabled',true);
			$('[name="employee"]').attr('disabled',true);
			$('[name="value"]').attr('disabled',true);
			$('[name="date"]').attr('disabled',true);
		},
		error: function (jqXHR, textStatus, errorThrown){
			swal({
				title: "Error",
				text: "An error has occurred!",
				type: "error"
			});
		}
	});
}

function save_bonus(save_method){
	$('.form-group').removeClass('has-error');
    $('.form-group').removeClass('has-warning');
    $('.form-group').removeClass('has-success');
    $('.input-feedback').empty(); 

	var url;

	if(save_method == 'create'){
		url = "http://localhost/hhrr-master/index.php/bonus/create_bonus";
	}
	else{
		url = "http://localhost/hhrr-master/index.php/bonus/update_bonus";
	}

	$.ajax({
		url : url,
		type: "POST",
		data: $('#bonus_form').serialize(),
		dataType: "JSON",
		success: function (data){
			if(data.status){
				if(save_method == 'create'){
					message = "Bonus created!";
					$('#bonus_form')[0].reset();
				}
				else{
					message = "Bonus processed!";
				}

				swal({
					title: "Success",
					text: message,
					type: "success"
				}); 
				reload_bonuses();
			}
			else{
				if(data.inputerror)
                {
                    for(var i = 0; i < data.inputerror.length; i++){
                        $('[name="'+data.inputerror[i]+'"]').parent().addClass('has-error');
                        $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]);
                    }
                }
			}
		},
		error: function (jqXHR, textStatus, errorThrown){
			swal({
				title: "Error",
				text: "An error has occurred!",
				type: "error"
			});
		}
	});
}

function reload_bonuses(){
	pending_bonuses.ajax.reload(null,false);
	processed_bonuses.ajax.reload(null,false);
}