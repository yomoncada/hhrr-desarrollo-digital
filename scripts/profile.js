var employee_absences;
var employee_ascents;
var employee_bonuses;
var employee_increases;

$(document).ready(function (){

	get_profile();

    employee_absences = $('#employee_absences').DataTable({ 
        "processing": true,
        "serverSide": true,
        "order": [],

        "ajax":{
            "url": "http://localhost/hhrr-master/index.php/profile/list_employee_absences",
            "type": "POST"
        },

        "columnDefs": [
        { 
            "targets": [ -1 ],
            "orderable": true,
        },
        ],
    });

    employee_ascents = $('#employee_ascents').DataTable({ 
        "processing": true,
        "serverSide": true,
        "order": [],

        "ajax":{
            "url": "http://localhost/hhrr-master/index.php/profile/list_employee_ascents",
            "type": "POST"
        },

        "columnDefs": [
        { 
            "targets": [ -1 ],
            "orderable": true,
        },
        ],
    });

    employee_bonuses = $('#employee_bonuses').DataTable({ 
        "processing": true,
        "serverSide": true,
        "order": [],

        "ajax":{
            "url": "http://localhost/hhrr-master/index.php/profile/list_employee_bonuses",
            "type": "POST"
        },

        "columnDefs": [
        { 
            "targets": [ -1 ],
            "orderable": true,
        },
        ],
    });

    employee_increases = $('#employee_increases').DataTable({ 
        "processing": true,
        "serverSide": true,
        "order": [],

        "ajax":{
            "url": "http://localhost/hhrr-master/index.php/profile/list_employee_increases",
            "type": "POST"
        },

        "columnDefs": [
        { 
            "targets": [ -1 ],
            "orderable": true,
        },
        ],
    });
});

function get_regions(){
    var country_id = $('[name="country"]').val();
    var html_select;

    if(!country_id){
        html_select =='<option value="0">Choose an option</option>'
        $('#regions_select').html(html_select);
    }
    $.ajax({
        url : "http://localhost/hhrr-master/index.php/employee/get_regions/" + country_id,
        type: "GET",
        dataType: "JSON",
        success: function (data){
            var html_select = '';
                html_select +='<option value="0">Choose an option</option>'
                for (var i=0; i<data.length; ++i){
                  html_select +='<option value="'+data[i].region_id+'">'+data[i].region_name+'</option>';
                }
              $('#regions_select').html(html_select);
        },
        error: function (jqXHR, textStatus, errorThrown){
            swal({
                title: "Error",
                text: "An error has occurred!",
                type: "error"
            });
        }
    });
}

function get_cities(){
    var region_id = $('[name="region"]').val();
    var html_select;

    if(!region_id){
        html_select =='<option value="0">Choose an option</option>'
        $('#cities_select').html(html_select);
    }
    $.ajax({
        url : "http://localhost/hhrr-master/index.php/employee/get_cities/" + region_id,
        type: "GET",
        dataType: "JSON",
        success: function (data){
                html_select +='<option value="0">Choose an option</option>'
                for (var i=0; i<data.length; ++i){
                  html_select +='<option value="'+data[i].city_id+'">'+data[i].city_name+'</option>';
                }
              $('#cities_select').html(html_select);
        },
        error: function (jqXHR, textStatus, errorThrown){
            swal({
                title: "Error",
                text: "An error has occurred!",
                type: "error"
            });
        }
    });
}

function get_profile(){
	$.ajax({
		url : "http://localhost/hhrr-master/index.php/profile/get_profile",
		type: "GET",
		dataType: "JSON",
		success: function (data){
            $('#profile_fullname').text(data.employee_firstname + ' ' + data.employee_firstlastname);
			$('#profile_username').text(data.employee_username);
            $('#profile_birth').text(data.employee_birth);
            $('#profile_area').text(data.area_name);
            $('#profile_position').text(data.position_name);
		},
		error: function (jqXHR, textStatus, errorThrown){
			swal({
				title: "Error",
				text: "An error has occurred!",
				type: "error"
			});
		}
	});
}

function get_settings(id,type){
    $('#settings_form')[0].reset();
    $('.form-group').removeClass('has-error');
    $('.form-group').removeClass('has-warning');
    $('.form-group').removeClass('has-success');
    $('.input-feedback').empty();
    if(type == 'info')
    {
        $('#settings_modal_title').text('Change my info');
        $('#settings_btn').text('Save changes');
        $('#settings_btn').attr('onclick','save_settings("profile_info")');
        $('.security-input').css('display','none');
        $('.info-input').css('display','block');
        $('#modal').addClass('modal-lg');
    }
    else
    {
        $('#settings_modal_title').text('Change my security');
        $('#settings_btn').text('Save changes');
        $('#settings_btn').attr('onclick','save_settings("profile_security")');
        $('.info-input').css('display','none');
        $('.security-input').css('display','block');
        $('#modal').removeClass('modal-lg');
    }

    $.ajax({
        url : "http://localhost/hhrr-master/index.php/employee/get_employee/" + id,
        type: "GET",
        dataType: "JSON",
        success: function (data){
            $('[name="id"]').val(data.employee_id);
            $('[name="firstname"]').val(data.employee_firstname);
            $('[name="secondname"]').val(data.employee_secondname);
            $('[name="firstlastname"]').val(data.employee_firstlastname);
            $('[name="secondlastname"]').val(data.employee_secondlastname);
            $('[name="birth"]').val(data.employee_birth);
            $('[name="country"]').val(data.employee_country);
            $('[name="region"]').val(data.employee_region);
            $('[name="city"]').val(data.employee_city);
            $('[name="address"]').val(data.employee_address);
            $('[name="cellphone"]').val(data.employee_cellphone);
            $('[name="phone"]').val(data.employee_phone);
            $('[name="whatsapp"]').val(data.employee_whatsapp);
            $('[name="facebook"]').val(data.employee_facebook);
            $('[name="gplus"]').val(data.employee_gplus);
            $('[name="linkedin"]').val(data.employee_linkedin);
        },
        error: function (jqXHR, textStatus, errorThrown){
            swal({
                title: "Error",
                text: "An error has occurred!",
                type: "error"
            });
        }
    });
}

function validate_actual_password(){
    $('[name="actual_password"]').parent().removeClass('has-error');
    $('[name="actual_password"]').parent().removeClass('has-warning');
    $('[name="actual_password"]').parent().removeClass('has-success');
    $('[name="actual_password"]').next().empty();

    var actual_password = $('[name="actual_password"]').val();

    $.ajax({
        url : "http://localhost/hhrr-master/index.php/employee/validate_actual_password",
        type: 'GET',
        data: {'actual_password':actual_password},
        dataType: 'JSON',
        success: function (data){
            switch(data.type){
                case 'Warning':
                    $('[name="actual_password"]').parent().addClass('has-warning');
                    $('[name="actual_password"]').next().text(data.message);  
                break;

                case 'Notice':
                    $('[name="actual_password"]').parent().addClass('has-success');
                    $('[name="actual_password"]').next().text(data.message);  
                break;

                case 'Error':
                    $('[name="actual_password"]').parent().addClass('has-error');
                    $('[name="actual_password"]').next().text(data.message);  
                break;
            }

            if(data.button == 1){
                $('#settings_btn').attr('disabled',true);
            }
            else{
                $('#settings_btn').attr('disabled',false);
            } 
        },
        error: function (jqXHR, textStatus, errorThrown){
            swal({
                title: "Error",
                text: "An error has occurred!",
                type: "error"
            });
        }
    });
}

function compare_password(){
    $('[name="password"]').parent().removeClass('has-error');
    $('[name="password"]').parent().removeClass('has-warning');
    $('[name="password"]').parent().removeClass('has-success');
    $('[name="password"]').next().empty();

    var password = $('[name="password"]').val();
    var repeat_password = $('[name="repeat_password"]').val();

    $.ajax({
        url : "http://localhost/hhrr-master/index.php/employee/compare_password",
        type: 'GET',
        data: {'password':password, 'repeat_password':repeat_password},
        dataType: 'JSON',
        success: function (data){
            switch(data.type){
                case 'Warning':
                    $('[name="password"]').parent().addClass('has-warning');
                    $('[name="password"]').next().text(data.message);
                    $('[name="repeat_password"]').parent().addClass('has-warning');
                    $('[name="repeat_password"]').next().text(data.message);    
                break;

                case 'Notice':
                    $('[name="password"]').parent().addClass('has-success');
                    $('[name="password"]').next().text(data.message);
                    $('[name="repeat_password"]').parent().addClass('has-success');
                    $('[name="repeat_password"]').next().text(data.message);  
                break;
            }

            if(data.button == 1){
                $('#employee_btn').attr('disabled',true);
            }
            else{
                $('#employee_btn').attr('disabled',false);
            } 
        },
        error: function (jqXHR, textStatus, errorThrown){
            swal({
                title: "Error",
                text: "An error has occurred!",
                type: "error"
            });
        }
    });
}

function save_settings(save_method){
    $('.form-group').removeClass('has-error');
    $('.form-group').removeClass('has-warning');
    $('.form-group').removeClass('has-success');
    $('.input-feedback').empty(); 

    $.ajax({
        url : "http://localhost/hhrr-master/index.php/employee/update_employee/" + save_method,
        type: "POST",
        data: $('#settings_form').serialize(),
        dataType: "JSON",
        success: function (data){
            if(data.status){
                swal({
                    title: "Success",
                    text: "Settings saved!",
                    type: "success"
                }); 
                get_profile();
            }
            else{
                if(data.inputerror)
                {
                    for(var i = 0; i < data.inputerror.length; i++){
                        $('[name="'+data.inputerror[i]+'"]').parent().addClass('has-error');
                        $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]);
                    }
                }
            }
        },
        error: function (jqXHR, textStatus, errorThrown){
            swal({
                title: "Error",
                text: "An error has occurred!",
                type: "error"
            });
        }
    });
}