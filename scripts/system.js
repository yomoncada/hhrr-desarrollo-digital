$(document).ready(function (){
    $('#recovery_card').css('display','none');
});

function recovery_form(){
    $.ajax({
        success: function (data){
            $('#recovery_form')[0].reset();
            $('[name="username"]').val($('#username').val());
            $('#recovery_card').css('display','block');
            $('#login_card').css('display','none');
            $('.form-group').removeClass('has-error');
            $('.help-block').empty();
        }
    });
}

function login_form(){
    $.ajax({
        success: function (data){
            $('#login_form')[0].reset();
            $('#recovery_card').css('display','none');
            $('#login_card').css('display','block');
            $('.form-group').removeClass('has-error');
            $('.help-block').empty();
        }
    });
}

function validate_login(){
    $('.form-group').removeClass('has-error');
    $('.input-feedback').empty(); 

    $.ajax({
        url : "http://localhost/hhrr-master/index.php/system/validate_login",
        type: "POST",
        data: $('#login_form').serialize(),
        dataType: "JSON",
        success: function (data){
            if(data.status == true){
                swal({
                  position: 'center',
                  type: 'success',
                  title: 'Welcome!',
                  showConfirmButton: false,
                  timer: 1500
                })
                location.href = "http://localhost/hhrr-master/index.php/home";
            }
            else
            {
                if(data.inputerror)
                {
                    for(var i = 0; i < data.inputerror.length; i++){
                        $('[name="'+data.inputerror[i]+'"]').parent().addClass('has-error');
                        $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]);
                    }
                }
                else if(data.swalx == 3)
                {
                    swal({
                        title: "Error",
                        text: "Username or password entered is invalid!",
                        type: "error"
                    });
                }
            }
        },
        error: function (jqXHR, textStatus, errorThrown){
            swal({
                title: "Error",
                text: "An error has occurred!",
                type: "error"
            });
        }
    });
}

function validate_recovery(){
    $('.form-group').removeClass('has-error');
    $('.input-feedback').empty(); 

    $.ajax({
        url : "http://localhost/hhrr-master/index.php/system/validate_recovery",
        type: "POST",
        data: $('#recovery_form').serialize(),
        dataType: "JSON",
        success: function (data){
            if(data.status == true){
                swal({
                  position: 'center',
                  type: 'success',
                  html: 'Use following temporal password to login:<br><h3>' + data.temporal_password + '</h3></br>',
                  showConfirmButton: false,
                  timer: 5000
                })
                $('#recovery_form')[0].reset();
            }
            else
            {
                if(data.inputerror)
                {
                    for(var i = 0; i < data.inputerror.length; i++){
                        $('[name="'+data.inputerror[i]+'"]').parent().addClass('has-error');
                        $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]);
                    }
                }
                else if(data.swalx == 3)
                {
                    swal({
                        title: "Error",
                        text: "Username, secret question or secret answer entered is invalid!",
                        type: "error"
                    });
                }
            }
        },
        error: function (jqXHR, textStatus, errorThrown){
            swal({
                title: "Error",
                text: "An error has occurred!",
                type: "error"
            });
        }
    });
}