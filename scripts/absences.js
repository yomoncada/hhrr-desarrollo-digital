var save_method;
var pending_absences;
var processed_absences;

$(document).ready(function (){
	pending_absences = $('#pending_absences').DataTable({ 
		"processing": true,
		"serverSide": true,
		"order": [],

		"ajax":{
			"url": "http://localhost/hhrr-master/index.php/absence/list_pending_absences",
			"type": "POST"
		},

		"columnDefs": [
		{ 
			"targets": [ -1 ],
			"orderable": false,
		},
		],
	});

	processed_absences = $('#processed_absences').DataTable({ 
		"processing": true,
		"serverSide": true,
		"order": [],

		"ajax":{
			"url": "http://localhost/hhrr-master/index.php/absence/list_processed_absences",
			"type": "POST"
		},

		"columnDefs": [
		{ 
			"targets": [ -1 ],
			"orderable": false,
		},
		],
	});
});

function create_absence(){
	$('#absence_form')[0].reset();
	$('.form-group').removeClass('has-error');
    $('.form-group').removeClass('has-warning');
    $('.form-group').removeClass('has-success');
	$('.input-feedback').empty();
	$('#absence_modal_title').text('Create an absence');
	$('#absence_btn').css('display','block');
	$('#absence_btn').attr('onclick','save_absence("create")');
	$('#absence_btn').text('Create');
	$('[name="id"]').removeAttr('disabled',true);
	$('[name="employee"]').removeAttr('disabled',true);
	$('[name="days"]').removeAttr('disabled',true);
	$('[name="startdate"]').removeAttr('disabled',true);
	$('[name="enddate"]').removeAttr('disabled',true);
	$('[name="reason"]').removeAttr('disabled',true);
	$('[name="file"]').removeAttr('disabled',true);
	$('[name="type"]').removeAttr('disabled',true);
}

function get_absence(id){
	$('#absence_form')[0].reset();
	$('.form-group').removeClass('has-error');
    $('.form-group').removeClass('has-warning');
    $('.form-group').removeClass('has-success');
	$('.input-feedback').empty();
	$('#absence_modal_title').text('Process an absence');
	$('#absence_btn').css('display','block');
	$('#absence_btn').attr('onclick','save_absence("update")');
	$('#absence_btn').text('Process');
	$('[name="id"]').removeAttr('disabled',true);
	$('[name="employee"]').removeAttr('disabled',true);
	$('[name="days"]').removeAttr('disabled',true);
	$('[name="startdate"]').removeAttr('disabled',true);
	$('[name="enddate"]').removeAttr('disabled',true);
	$('[name="reason"]').removeAttr('disabled',true);
	$('[name="file"]').removeAttr('disabled',true);
	$('[name="type"]').removeAttr('disabled',true);

	$.ajax({
		url : "http://localhost/hhrr-master/index.php/absence/get_absence/" + id,
		type: "GET",
		dataType: "JSON",
		success: function (data){
			$('[name="id"]').val(data.absence_id);
			$('[name="employee"]').val(data.absence_employee);
			$('[name="days"]').val(data.absence_days);
			$('[name="startdate"]').val(data.absence_startdate);
			$('[name="enddate"]').val(data.absence_enddate);
			$('[name="reason"]').val(data.absence_reason);
			$('[name="file"]').val(data.absence_file);
			$('[name="type"]').val(data.absence_type);
		},
		error: function (jqXHR, textStatus, errorThrown){
			swal({
				title: "Error",
				text: "An error has occurred!",
				type: "error"
			});
		}
	});
}

function get_absence_unmodifiable(id){
	get_absence(id);
	$('#absence_modal_title').text('Read an absence');
	$('#absence_btn').css('display','none');

	$.ajax({
		url : "http://localhost/hhrr-master/index.php/absence/get_absence/" + id,
		type: "GET",
		dataType: "JSON",
		success: function (data){
			$('[name="id"]').attr('disabled',true);
			$('[name="employee"]').attr('disabled',true);
			$('[name="days"]').attr('disabled',true);
			$('[name="startdate"]').attr('disabled',true);
			$('[name="enddate"]').attr('disabled',true);
			$('[name="reason"]').attr('disabled',true);
			$('[name="file"]').attr('disabled',true);
			$('[name="type"]').attr('disabled',true);
		},
		error: function (jqXHR, textStatus, errorThrown){
			swal({
				title: "Error",
				text: "An error has occurred!",
				type: "error"
			});
		}
	});
}

function save_absence(save_method){
	$('.form-group').removeClass('has-error');
    $('.form-group').removeClass('has-warning');
    $('.form-group').removeClass('has-success');
    $('.input-feedback').empty(); 

	var url;

	if(save_method == 'create'){
		url = "http://localhost/hhrr-master/index.php/absence/create_absence";
	}
	else{
		url = "http://localhost/hhrr-master/index.php/absence/update_absence";
	}

	$.ajax({
		url : url,
		type: "POST",
		data: $('#absence_form').serialize(),
		dataType: "JSON",
		success: function (data){
			if(data.status){
				if(save_method == 'create'){
					message = "Absence created!";
					$('#absence_form')[0].reset();
				}
				else{
					message = "Absence updated!";
				}

				swal({
					title: "Success",
					text: message,
					type: "success"
				}); 
				reload_absences();
			}
			else{
				if(data.inputerror)
                {
                    for(var i = 0; i < data.inputerror.length; i++){
                        $('[name="'+data.inputerror[i]+'"]').parent().addClass('has-error');
                        $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]);
                    }
                }
			}
		},
		error: function (jqXHR, textStatus, errorThrown){
			swal({
				title: "Error",
				text: "An error has occurred!",
				type: "error"
			});
		}
	});
}

function reload_absences(){
	pending_absences.ajax.reload(null,false);
	processed_absences.ajax.reload(null,false);
}