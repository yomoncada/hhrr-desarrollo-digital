var area_employees;

$(document).ready(function (){

    area_employees = $('#area_employees').DataTable({ 
        "processing": true,
        "serverSide": true,
        "order": [],

        "ajax":{
            "url": "http://localhost/hhrr-master/index.php/home/list_area_employees",
            "type": "POST"
        },

        "columnDefs": [
        { 
            "targets": [ -1 ],
            "orderable": true,
        },
        ],
    });
});