var save_method;
var pending_ascents;
var processed_ascents;

$(document).ready(function (){
	pending_ascents = $('#pending_ascents').DataTable({ 
		"processing": true,
		"serverSide": true,
		"order": [],

		"ajax":{
			"url": "http://localhost/hhrr-master/index.php/ascent/list_pending_ascents",
			"type": "POST"
		},

		"columnDefs": [
		{ 
			"targets": [ -1 ],
			"orderable": false,
		},
		],
	});

	processed_ascents = $('#processed_ascents').DataTable({ 
		"processing": true,
		"serverSide": true,
		"order": [],

		"ajax":{
			"url": "http://localhost/hhrr-master/index.php/ascent/list_processed_ascents",
			"type": "POST"
		},

		"columnDefs": [
		{ 
			"targets": [ -1 ],
			"orderable": false,
		},
		],
	});
});

function create_ascent(){
	$('#ascent_form')[0].reset();
	$('.form-group').removeClass('has-error');
    $('.form-group').removeClass('has-warning');
    $('.form-group').removeClass('has-success');
	$('.input-feedback').empty();
	$('#ascent_modal_title').text('Create an ascent');
	$('#ascent_btn').css('display','block');
	$('#ascent_btn').attr('onclick','save_ascent("create")');
	$('#ascent_btn').text('Create');
	$('[name="id"]').attr('disabled',true);
	$('[name="employee"]').removeAttr('disabled',true);
	$('[name="area"]').removeAttr('disabled',true);
	$('[name="position"]').removeAttr('disabled',true);
	$('[name="salary"]').removeAttr('disabled',true);
}

function get_ascent(id){
	$('#ascent_form')[0].reset();
	$('.form-group').removeClass('has-error');
    $('.form-group').removeClass('has-warning');
    $('.form-group').removeClass('has-success');
	$('.input-feedback').empty();
	$('#ascent_btn').css('display','block');
	$('#ascent_modal_title').text('Process an ascent');
	$('#ascent_btn').attr('onclick','save_ascent("update")');
	$('#ascent_btn').text('Process');
	$('[name="employee"]').removeAttr('disabled',true);
	$('[name="area"]').removeAttr('disabled',true);
	$('[name="position"]').removeAttr('disabled',true);
	$('[name="salary"]').removeAttr('disabled',true);
	$('[name="date"]').removeAttr('disabled',true);

	$.ajax({
		url : "http://localhost/hhrr-master/index.php/ascent/get_ascent/" + id,
		type: "GET",
		dataType: "JSON",
		success: function (data){
			$('[name="id"]').val(data.ascent_id);
			$('[name="employee"]').val(data.ascent_employee);
			$('[name="area"]').val(data.ascent_area);
			$('[name="position"]').val(data.ascent_position);
			$('[name="salary"]').val(data.ascent_salary);
			$('[name="date"]').val(data.ascent_date);
		},
		error: function (jqXHR, textStatus, errorThrown){
			swal({
				title: "Error",
				text: "An error has occurred!",
				type: "error"
			});
		}
	});
}

function get_ascent_unmodifiable(id){
	get_ascent(id);
	$('#ascent_modal_title').text('Read an ascent');
	$('#ascent_btn').css('display','none');

	$.ajax({
		url : "http://localhost/hhrr-master/index.php/ascent/get_ascent/" + id,
		type: "GET",
		dataType: "JSON",
		success: function (data){
			$('[name="id"]').attr('disabled',true);
			$('[name="employee"]').attr('disabled',true);
			$('[name="area"]').attr('disabled',true);
			$('[name="position"]').attr('disabled',true);
			$('[name="salary"]').attr('disabled',true);
			$('[name="date"]').attr('disabled',true);
		},
		error: function (jqXHR, textStatus, errorThrown){
			swal({
				title: "Error",
				text: "An error has occurred!",
				type: "error"
			});
		}
	});
}

function save_ascent(save_method){
	$('.form-group').removeClass('has-error');
    $('.form-group').removeClass('has-warning');
    $('.form-group').removeClass('has-success');
    $('.input-feedback').empty(); 

	var url;

	if(save_method == 'create'){
		url = "http://localhost/hhrr-master/index.php/ascent/create_ascent";
	}
	else{
		url = "http://localhost/hhrr-master/index.php/ascent/update_ascent";
	}

	$.ajax({
		url : url,
		type: "POST",
		data: $('#ascent_form').serialize(),
		dataType: "JSON",
		success: function (data){
			if(data.status){
				if(save_method == 'create'){
					message = "Ascent created!";
					$('#ascent_form')[0].reset();
				}
				else{
					message = "Ascent processed!";
				}

				swal({
					title: "Success",
					text: message,
					type: "success"
				}); 
				reload_ascents();
			}
			else{
				if(data.inputerror)
                {
                    for(var i = 0; i < data.inputerror.length; i++){
                        $('[name="'+data.inputerror[i]+'"]').parent().addClass('has-error');
                        $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]);
                    }
                }
			}
		},
		error: function (jqXHR, textStatus, errorThrown){
			swal({
				title: "Error",
				text: "An error has occurred!",
				type: "error"
			});
		}
	});
}

function reload_ascents(){
	pending_ascents.ajax.reload(null,false);
	processed_ascents.ajax.reload(null,false);
}