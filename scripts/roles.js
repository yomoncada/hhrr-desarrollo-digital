var save_method;
var active_roles;
var inactive_roles;

$(document).ready(function (){
	active_roles = $('#active_roles').DataTable({ 
		"processing": true,
		"serverSide": true,
		"order": [],

		"ajax":{
			"url": "http://localhost/hhrr-master/index.php/rol/list_active_roles",
			"type": "POST"
		},

		"columnDefs": [
		{ 
			"targets": [ -1 ],
			"orderable": false,
		},
		],
	});

	inactive_roles = $('#inactive_roles').DataTable({ 
		"processing": true,
		"serverSide": true,
		"order": [],

		"ajax":{
			"url": "http://localhost/hhrr-master/index.php/rol/list_inactive_roles",
			"type": "POST"
		},

		"columnDefs": [
		{ 
			"targets": [ -1 ],
			"orderable": false,
		},
		],
	});
});

function create_rol(){
	$('#rol_form')[0].reset();
	$('.form-group').removeClass('has-error');
    $('.form-group').removeClass('has-warning');
    $('.form-group').removeClass('has-success');
	$('.input-feedback').empty();
	$('#rol_modal_title').text('Create an rol');
	$('#rol_btn').attr('onclick','save_rol("create")');
	$('#rol_btn').text('Create');
}

function get_rol(id){
	$('#rol_form')[0].reset();
	$('.form-group').removeClass('has-error');
    $('.form-group').removeClass('has-warning');
    $('.form-group').removeClass('has-success');
	$('.input-feedback').empty();
	$('#rol_modal_title').text('Update an rol');
	$('#rol_btn').attr('onclick','save_rol("update")');
	$('#rol_btn').text('Save changes');

	$.ajax({
		url : "http://localhost/hhrr-master/index.php/rol/get_rol/" + id,
		type: "GET",
		dataType: "JSON",
		success: function (data){
			$('[name="id"]').val(data.rol_id);
			$('[name="name"]').val(data.rol_name);
			$('[name="description"]').val(data.rol_description);
		},
		error: function (jqXHR, textStatus, errorThrown){
			swal({
				title: "Error",
				text: "An error has occurred!",
				type: "error"
			});
		}
	});
}

function validate_name(){
	$('[name="name"]').parent().removeClass('has-error');
    $('[name="name"]').parent().removeClass('has-warning');
    $('[name="name"]').parent().removeClass('has-success');
    $('[name="name"]').next().empty();

    var name = $("#rol_name").val();

	$.ajax({
		url : "http://localhost/hhrr-master/index.php/rol/validate_name",
		type: 'GET',
		data: {'name':name},
		dataType: 'JSON',
		success: function (data){
			switch(data.type){
                case 'Warning':
                    $('[name="name"]').parent().addClass('has-warning');
                    $('[name="name"]').next().text(data.message);  
                break;

                case 'Notice':
                    $('[name="name"]').parent().addClass('has-success');
                    $('[name="name"]').next().text(data.message);  
                break;

                case 'Error':
                    $('[name="name"]').parent().addClass('has-error');
                    $('[name="name"]').next().text(data.message);  
                break;
            }

			if(data.button == 1){
				$('#rol_btn').attr('disabled',true);
			}
			else{
				$('#rol_btn').attr('disabled',false);
			} 
		},
		error: function (jqXHR, textStatus, errorThrown){
			swal({
				title: "Error",
				text: "An error has occurred!",
				type: "error"
			});
		}
	});
}

function save_rol(save_method){
	$('.form-group').removeClass('has-error');
    $('.form-group').removeClass('has-warning');
    $('.form-group').removeClass('has-success');
    $('.input-feedback').empty(); 

	var url;

	if(save_method == 'create'){
		url = "http://localhost/hhrr-master/index.php/rol/create_rol";
	}
	else{
		url = "http://localhost/hhrr-master/index.php/rol/update_rol";
	}

	$.ajax({
		url : url,
		type: "POST",
		data: $('#rol_form').serialize(),
		dataType: "JSON",
		success: function (data){
			if(data.status){
				if(save_method == 'create'){
					message = "Rol created!";
					$('#rol_form')[0].reset();
				}
				else{
					message = "Rol updated!";
				}

				swal({
					title: "Success",
					text: message,
					type: "success"
				}); 
				reload_roles();
			}
			else{
				if(data.inputerror)
                {
                    for(var i = 0; i < data.inputerror.length; i++){
                        $('[name="'+data.inputerror[i]+'"]').parent().addClass('has-error');
                        $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]);
                    }
                }
			}
		},
		error: function (jqXHR, textStatus, errorThrown){
			swal({
				title: "Error",
				text: "An error has occurred!",
				type: "error"
			});
		}
	});
}

function activate_rol(id){  
	swal({
		title: "Warning",
		text: "Do you want to activate this rol?",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#f8ac59",
		confirmButtonText: "Sí",
		cancelButtonText: "No"
	}).then(function (){
		$.ajax({
			url : "http://localhost/hhrr-master/index.php/rol/activate_rol/" + id,
			type: "POST",
			dataType: "JSON",
			success: function (data){
				reload_roles();
			},
			error: function (jqXHR, textStatus, errorThrown){
				swal({
					title: "Error",
					text: "An error has occurred!",
					type: "error"
				});
			}
		});
		swal("Success", "Rol activated!", "success");
	}, function (dismiss){
        // dismiss can be 'cancel', 'overlay',
        // 'close', and 'timer'
        if(dismiss === 'cancel'){
          	swal("Notice", "Activation canceled!", "info");
        }
    })
}

function deactivate_rol(id){  
	swal({
		title: "Warning",
		text: "Do you want to deactivate this rol?",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#f8ac59",
		confirmButtonText: "Yes",
		cancelButtonText: "No"
	}).then(function (){
		$.ajax({
			url : "http://localhost/hhrr-master/index.php/rol/deactivate_rol/" + id,
			type: "POST",
			dataType: "JSON",
			success: function (data){
				reload_roles();
			},
			error: function (jqXHR, textStatus, errorThrown){
				swal({
					title: "Error",
					text: "An error has occurred!",
					type: "error"
				});
			}
		});
		swal("Success", "Rol deactivated!", "success");
	}, function (dismiss){
        // dismiss can be 'cancel', 'overlay',
        // 'close', and 'timer'
        if(dismiss === 'cancel'){
          	swal("Notice", "Deactivation canceled!", "info");
        }
    })
}

function reload_roles(){
	active_roles.ajax.reload(null,false);
	inactive_roles.ajax.reload(null,false);
}