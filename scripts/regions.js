var save_method;
var active_regions;
var inactive_regions;

$(document).ready(function (){
	active_regions = $('#active_regions').DataTable({ 
		"processing": true,
		"serverSide": true,
		"order": [],

		"ajax":{
			"url": "http://localhost/hhrr-master/index.php/region/list_active_regions",
			"type": "POST"
		},

		"columnDefs": [
		{ 
			"targets": [ -1 ],
			"orderable": false,
		},
		],
	});

	inactive_regions = $('#inactive_regions').DataTable({ 
		"processing": true,
		"serverSide": true,
		"order": [],

		"ajax":{
			"url": "http://localhost/hhrr-master/index.php/region/list_inactive_regions",
			"type": "POST"
		},

		"columnDefs": [
		{ 
			"targets": [ -1 ],
			"orderable": false,
		},
		],
	});
});

function create_region(){
	$('#region_form')[0].reset();
	$('.form-group').removeClass('has-error');
    $('.form-group').removeClass('has-warning');
    $('.form-group').removeClass('has-success');
	$('.input-feedback').empty();
	$('#region_modal_title').text('Create an region');
	$('#region_btn').attr('onclick','save_region("create")');
	$('#region_btn').text('Create');
}

function get_region(id){
	$('#region_form')[0].reset();
	$('.form-group').removeClass('has-error');
    $('.form-group').removeClass('has-warning');
    $('.form-group').removeClass('has-success');
	$('.input-feedback').empty();
	$('#region_modal_title').text('Update an region');
	$('#region_btn').attr('onclick','save_region("update")');
	$('#region_btn').text('Save changes');

	$.ajax({
		url : "http://localhost/hhrr-master/index.php/region/get_region/" + id,
		type: "GET",
		dataType: "JSON",
		success: function (data){
			$('[name="id"]').val(data.region_id);
			$('[name="name"]').val(data.region_name);
			$('[name="country"]').val(data.region_country);
		},
		error: function (jqXHR, textStatus, errorThrown){
			swal({
				title: "Error",
				text: "An error has occurred!",
				type: "error"
			});
		}
	});
}

function validate_name(){
	$('[name="name"]').parent().removeClass('has-error');
    $('[name="name"]').parent().removeClass('has-warning');
    $('[name="name"]').parent().removeClass('has-success');
    $('[name="name"]').next().empty();

    var name = $("#region_name").val();

	$.ajax({
		url : "http://localhost/hhrr-master/index.php/region/validate_name",
		type: 'GET',
		data: {'name':name},
		dataType: 'JSON',
		success: function (data){
			switch(data.type){
                case 'Warning':
                    $('[name="name"]').parent().addClass('has-warning');
                    $('[name="name"]').next().text(data.message);  
                break;

                case 'Notice':
                    $('[name="name"]').parent().addClass('has-success');
                    $('[name="name"]').next().text(data.message);  
                break;

                case 'Error':
                    $('[name="name"]').parent().addClass('has-error');
                    $('[name="name"]').next().text(data.message);  
                break;
            }

			if(data.button == 1){
				$('#region_btn').attr('disabled',true);
			}
			else{
				$('#region_btn').attr('disabled',false);
			} 
		},
		error: function (jqXHR, textStatus, errorThrown){
			swal({
				title: "Error",
				text: "An error has occurred!",
				type: "error"
			});
		}
	});
}

function save_region(save_method){
	$('.form-group').removeClass('has-error');
    $('.form-group').removeClass('has-warning');
    $('.form-group').removeClass('has-success');
    $('.input-feedback').empty(); 

	var url;

	if(save_method == 'create'){
		url = "http://localhost/hhrr-master/index.php/region/create_region";
	}
	else{
		url = "http://localhost/hhrr-master/index.php/region/update_region";
	}

	$.ajax({
		url : url,
		type: "POST",
		data: $('#region_form').serialize(),
		dataType: "JSON",
		success: function (data){
			if(data.status){
				if(save_method == 'create'){
					message = "Region created!";
					$('#region_form')[0].reset();
				}
				else{
					message = "Region updated!";
				}

				swal({
					title: "Success",
					text: message,
					type: "success"
				}); 
				reload_regions();
			}
			else{
				if(data.inputerror)
                {
                    for(var i = 0; i < data.inputerror.length; i++){
                        $('[name="'+data.inputerror[i]+'"]').parent().addClass('has-error');
                        $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]);
                    }
                }
			}
		},
		error: function (jqXHR, textStatus, errorThrown){
			swal({
				title: "Error",
				text: "An error has occurred!",
				type: "error"
			});
		}
	});
}

function activate_region(id){  
	swal({
		title: "Warning",
		text: "Do you want to activate this region?",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#f8ac59",
		confirmButtonText: "Sí",
		cancelButtonText: "No"
	}).then(function (){
		$.ajax({
			url : "http://localhost/hhrr-master/index.php/region/activate_region/" + id,
			type: "POST",
			dataType: "JSON",
			success: function (data){
				reload_regions();
			},
			error: function (jqXHR, textStatus, errorThrown){
				swal({
					title: "Error",
					text: "An error has occurred!",
					type: "error"
				});
			}
		});
		swal("Success", "Region activated!", "success");
	}, function (dismiss){
        // dismiss can be 'cancel', 'overlay',
        // 'close', and 'timer'
        if(dismiss === 'cancel'){
          	swal("Notice", "Activation canceled!", "info");
        }
    })
}

function deactivate_region(id){  
	swal({
		title: "Warning",
		text: "Do you want to deactivate this region?",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#f8ac59",
		confirmButtonText: "Yes",
		cancelButtonText: "No"
	}).then(function (){
		$.ajax({
			url : "http://localhost/hhrr-master/index.php/region/deactivate_region/" + id,
			type: "POST",
			dataType: "JSON",
			success: function (data){
				reload_regions();
			},
			error: function (jqXHR, textStatus, errorThrown){
				swal({
					title: "Error",
					text: "An error has occurred!",
					type: "error"
				});
			}
		});
		swal("Success", "Region deactivated!", "success");
	}, function (dismiss){
        // dismiss can be 'cancel', 'overlay',
        // 'close', and 'timer'
        if(dismiss === 'cancel'){
          	swal("Notice", "Deactivation canceled!", "info");
        }
    })
}

function reload_regions(){
	active_regions.ajax.reload(null,false);
	inactive_regions.ajax.reload(null,false);
}